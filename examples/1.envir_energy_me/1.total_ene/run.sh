
NCPU=8
MESCAL='/home/gdavino/myPrograms/mescal/mescal_mpi'


mpirun -np $NCPU $MESCAL neu.inp > neu.out
mpirun -np $NCPU $MESCAL cat.inp > cat.out
mpirun -np $NCPU $MESCAL ani.inp > ani.out


U0=$(grep g_ENE neu.out | awk '{print $4}')
Up=$(grep g_ENE cat.out | awk '{print $4}')
D_tot_h=$(echo $Up - $U0 | bc)



U0=$(grep g_ENE neu.out | awk '{print $4}')
Um=$(grep g_ENE ani.out | awk '{print $4}')
D_tot_e=$(echo $U0 - $Um | bc)


echo
echo 'Environmental energies:'
echo '   hole     : ' $D_tot_h eV
echo '   electron : ' $D_tot_e eV
echo



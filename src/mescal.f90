!    This file is part of MESCal.
!    Copyright Gabriele D'Avino (2013)
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.

program mescal
  use post_sc
  use microelectrostatic
#ifdef MPI  
  use module_mpi
#endif

  implicit none 
  logical       :: inp_exist,found
  character(50) :: input_fname,foo
  character(50) :: key,job_type,fname_out
  integer       :: strlen

#ifdef MPI
  call initial_mpi()
  if (proc_id==0) then
#endif

call start_banner
call cpu_time(time_begin)


call getarg(1,input_fname)
inquire(file=input_fname,exist=inp_exist)

if (inp_exist) then
   write(*,*) 'Reading input from: ',input_fname
   write(*,*) 
   call read_input(input_fname)
else
   write(*,*) 'ERROR: input file not found!'
   call stop_exec 
endif

strlen=len(trim(input_fname))
root_fname=adjustl(input_fname)
root_fname=root_fname(1:strlen-4)


! Determine the type of calulation to perform:
key="JOBTYPE"
call parse_str(key,job_type,found) 
if (found) then 
!   write(*,"(1x,a20,a2,a4)") key,': ',job_type
!   write(*,*)
else
   write(*,*) 'ERROR: Mandatory ',adjustl(trim(key)),' input not found! ' 
   call stop_exec 
endif


! GD 06/10/21 this block was here, moved just below
! It should be the same but keep it mind in case of future problems!
!! tell mescal how to read coordinates
!key="PDX"
!call parse_log(key,use_pdx,found) 
!if (found) then 
!   write(*,"(1x,a20,a2,l1)") key,': ',use_pdx
!   write(*,*) 'INFO: atomic coordinates read in pdx format'
!   write(*,*)
!endif

#ifdef MPI
  endif
  !brodcasting job_type
  call mpi_bcast(job_type ,50,  MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)
  call show_mpi()

  if (proc_id==0) then
     write(*,"(1x,a20,a2,a4)") key,': ',job_type
     write(*,*)

     ! tell mescal how to read coordinates
     key="PDX"
     call parse_log(key,use_pdx,found) 
     if (found) then 
        write(*,"(1x,a20,a2,l1)") key,': ',use_pdx
        write(*,*) 'INFO: atomic coordinates read in pdx format'
        write(*,*)
     endif
   
  endif
#endif



if ( (job_type=='CR') .or. (job_type=='ME') ) then
   ! go for self consistent ME or CR calculation
   call driver_MECR 

elseif (job_type=='AAP') then
   call opt_calc_aap  

elseif (job_type=='VF_PBC') then
   call opt_VF_pc_cutoff

elseif (job_type=='SITENE') then
   call opt_sitene

else
   write(*,*) '  ERROR: JOBTYPE ',adjustl(trim(job_type)),' is not contemplated!'
   call stop_exec

endif 

#ifdef MPI
  if (proc_id==0) then
#endif

! POST-SC TASKS 

key="VF_POINTS"
call parse_str(key,foo,found) 
if (found) then 
   call opt_write_VF_at_r
endif




key="OUT_CHG_DIP"
call parse_str(key,foo,found) 
if (found) then 
   call opt_write_chg_dip
endif

key="OUT_CHG_DIP_MM"
call parse_str(key,foo,found) 
if (found) then 
   call opt_write_chg_dip_mm
endif



call stop_banner

#ifdef MPI
  endif
  call finalize_mpi()
#endif



end program mescal






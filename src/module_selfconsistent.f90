!    This file is part of MESCal.
!    Copyright Gabriele D'Avino (2013)
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.
!
module selfconsistent
  ! Subroutines for the self consistent determination of treatment induced
  ! dipoles and charges via iterative solution of linear equations
  use io
  use types
  use electrostatics 
#ifdef MPI
  use module_mpi
#endif  

  implicit none
  save
  integer       :: n_iter
  logical       :: is_converged,do_ene_part
  real(rk)      :: energy_0,deltaE,dq_max,dq_avg,dmu_max,dmu_avg
  real(rk),allocatable,dimension(:) :: energy,DE_all
  character(30) :: o1fmt="(i5,10(f17.9))"
  character(120):: iter_header
contains


subroutine selfconsistent_solution
  ! perform self-consistent treatment of electronic polarization for both  ME and CR
  implicit none
  integer       :: i,j
  character(50) :: fname_itrec,fname_failed,fout_dbg
  real(rk)      :: ene,slope0,slope1,slope2,mm
  logical       :: is_rst,write_rst
  character(50) :: rst_file,key
  character(150):: fn_rst
  logical       :: found


#ifdef MPI
  call mpi_bcast(max_it        ,1,  MPI_INTEGER,0,MPI_COMM_WORLD,ierr) 
  call mpi_bcast(Emax        ,1,  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(tolE        ,1,  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
#endif

! memory allocation for variables keeping track of iterations 
if ( allocated(energy)) deallocate (energy)
if ( allocated(DE_all) ) deallocate (DE_all)
allocate(energy(max_it))
allocate(DE_all(max_it))

#ifdef MPI
  if (proc_id==0) then
#endif

write(*,*) '    *** Self consistent calculation of molecular polarization *** '
write(*,*)
write(*,*)

! Initialize to zero induced charges, dipoles, fields and potentials
! The only exception are external fields/potentials assigned in the specific routines  
call init_0




!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                    START OR RESTART ????
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


key="RESTART"
call parse_str(key,rst_file,found)
is_rst=.false.
if (found) then 
   is_rst=.true.
   write(*,"(1x,a20,a2,a40)") key,': ',rst_file
else
   write(*,*) 'INFO: No RESTART key found, starting from scratch! '
endif
write(*,*)

#ifdef MPI
  endif
  
  call mpi_bcast(is_rst            ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(is_me             ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(is_cr             ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(is_periodic_2D_Rc ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(is_periodic_3D_Rc ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)  
  call mpi_bcast(use_damp_dyn      ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)  
  call mpi_bcast(is_mirr           ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)

  ! GD 24/7/20 
  call mpi_bcast(screen_inter_dipdip   ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(screen_intra_dipdip   ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(a_screen              ,1,  MPI_DOUBLE ,0,MPI_COMM_WORLD,ierr)

#endif

if (is_rst) then

#ifdef MPI
if(proc_id==0) then
#endif

   call read_restart(rst_file)
   
#ifdef MPI
endif   
call bcast_sys(is_rst) ! broadcast system
#endif

else ! --> not a restart
   
      ! do the 0-th iteration consisting of the followins steps:
      !  1. Evaluate potential and field due to permanent charges 
      !  2. Sum field and potential of permanent souces: charges + external
      !  3: Perform the first energy evaluation
      
      ! STEP 1: Evaluate  potential and field due to permanent charges 
      ! We distinguish the following cases:
      ! -permanent charges at atoms of input pdb only
      ! -field and potential including periodic replica within cutoff
      ! -field and potential of permanent charges from external file

#ifdef MPI    
  call bcast_sys(is_rst) ! broadcast system
#endif


  key="CHECK_CONTACTS"
  call parse_real(key,Rcontact,found) 
  if (found) then 
     write(*,"(1x,a20,a2,f6.3,a10)") key,': ',Rcontact
     write(*,*) 
     call check_intermol_contacts
     call stop_exec
  endif

  

  ! GD 07/02/21
  call calc_F_pc_v2
  call calc_V_pc_v2


  ! GD 07/05/21 
  ! Dipole corrections in PBC calculations (if required)
  call dipole_correction
  
    
  ! STEP 2: Sum field and potential of permanent sources: charges + external
  ! V_0 and F_0 will be then used in ALL energy evaluations
  call calc_V0_F0
       

  

  ! STEP 3: Perform the first energy evaluation
  call calc_energy(ene)
      
  energy_0=ene*iu2ev
  energy_elstat=energy_0

#ifdef MPI 
  if(proc_id==0) then
#endif    
     write(*,"(a,f16.4,a)") '   Electrostatic Energy (eV) ', energy_elstat !,'   g_ELSTAT '
     write(*,*)
     call cpu_time(time_end)
     call print_elaps_time
     write(*,*) 
#ifdef MPI 
  endif
#endif 
endif


#ifdef MPI 
  if(proc_id==0) then
#endif 
! Write restarts at every step?
key="OUT_RST"
write_rst=.false.
!call parse_str_long(key,fn_rst,found) GD 28/4/20 incompatible string length 
fn_rst=''
call parse_str(key,fn_rst,found)  
if (found) then
   write_rst=.true.
   write(*,*) key,': ',fn_rst
   write(*,*) 'INFO: restart files will be written at every iteration!'
   write(*,*) 
endif
#ifdef MPI 
  endif
#endif 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                   ITERATIVE SOLUTION                     !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Within each iteration:
!  1.Save result of the previous iteration
!  2.Compute F/V due to induced charges/dipoles
!  3.Compute total F/V
!  4.Check if damping should be applied 
!  5.Updated induced charges/dipoles
!  6.Compute energy
!  7.Compute difference to previous iteration
!  8.Evaluate exit condition

energy=0.0_rk
DE_all=0.0_rk
is_converged=.false.
relax_dummy=.true.

#ifdef MPI 
  if(proc_id==0) then
#endif 
write(*,"(a)") 
write(*,"(a)") '   -- Starting iterations  --'
write(*,"(a)") 
write(*,"(a)") 'Convergence indicators:'
write(*,"(a)") '   dp_mean=<|mu(i)-mu(i-1)|>     (e*Angstrom)'
write(*,"(a)") '   dp_max= Max[|mu(i)-mu(i-1)|]  (e*Angstrom)'
if (is_cr) write(*,"(a)") '   dq_mean= <|q(i)-q(i-1)|>   (e)'
if (is_cr) write(*,"(a)") '   dq_max= Max[|q(i)-q(i-1)|] (e)'
write(*,*) 


iter_header=' Step   Damp      Energy           E(i)-E(i-1)   dp_mean      dp_max'
if (is_cr) iter_header=' Step   Damp      Energy           E(i)-E(i-1)   dq_mean      dq_max       dp_mean      dp_max'
write(*,"(a)") adjustl(iter_header)
write(*,"(a95)")' --------------------------------------------------------------------------------------------'
#ifdef MPI 
  endif
#endif 

do n_iter=1,max_it

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                    i-th   ITERATION                      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


   ! STEP 1: Save result of the previous iteration
   call save_2_im1


   ! STEP 2: Compute F/V due to induced charges and dipoles 
   ! We distinguish the following cases: 
   ! ME and CR, with and withouth periodic replica (within cutoff) 
   !
   ! Remarks: 
   !  - 2D or 3D replica make no difference at this stage
   !  - In ME only fields of induced dipoles has to be re-computed
   !  - IN CR field and potentials of dipoles and charges are needed
   if ( (n_iter.gt.1) .or. (is_rst) ) then 


      ! GD 07/04/21      
      call calc_F_dip_v2

      if (is_cr) then
         call calc_V_dip_v2
         call calc_V_dq_v2
         call calc_F_dq_v2
      endif

   endif

   
! 2clear 
!      if (is_me) then
!
!         if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then
!            call calc_F_dip_rep
!         else
!            call calc_F_dip
!         endif
!         
!         ! GD 20/03/2017:  updating site potential is irrelevant for convengence 
!         ! and energy evaluation in ME calculations!
!         ! The potential at atoms is not computed during iterations but only at the end
!         ! if a restart file is required.
!         
!      elseif (is_cr) then
!
!         call calc_V_dip_v2
!         call calc_V_dq_v2
!         call calc_F_dq_v2
!
!! 2clear 
!!         if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  
!!            call calc_V_dq_rep
!!            call calc_F_dq_rep
!!            call calc_V_dip_rep
!!            call calc_F_dip_rep
!!         else
!!            call calc_V_dq
!!            call calc_F_dq
!!            call calc_V_dip
!!            call calc_F_dip
!!         endif
!
!      endif
!   endif

   ! STEP 3: Compute total F/V

   call calc_V_tot
   call calc_F_tot


   ! STEP 4: Check if damping should be applied 
#ifdef MPI
  if(proc_id==0) then
#endif


! GD 09/04/21 previous version
! adaptive damping that is fixed to damp_1 until deltaE < dE_1
! then, it decreased exponentially until deltaE < dE_2, after which
! the damping is fixed to damp_2
if  (use_damp_dyn) then  

   if (n_iter.le.3) then
      damp=damp_1
   elseif ( abs(deltaE) .gt. dE_1 )  then
      damp=damp_1
   elseif ( abs(deltaE) .gt. dE_2 ) then
      
      mm=( damp_2 - damp_1 )/( log10(dE_1) - log10(dE_2) )
   
      damp = damp_1 + mm * ( log10(dE_1) - log10( abs(deltaE) ) )

   else !  ( abs(deltaE) .le. dE_2 )
      damp=damp_2 
   endif

endif

! GD 09/04/21 previous version  2clear
!if (use_smart_damp) then    ! use damping in case of oscillations 
!
!      if (n_iter.le.4) then
!         damp=abs(damp_read)
!
!      else
!         slope0=energy(n_iter-2)-energy(n_iter-3)
!         slope1=energy(n_iter-1)-energy(n_iter-2)
!!         slope2=energy(n_iter)-energy(n_iter-1)
!
!!         write(*,*) '>>>>>>>>>>> ',N_ITER,slope0,slope1,slope2
!         damp=0.0_rk
!!         if ( (slope1*slope2.lt.0.0_rk).or.(slope0*slope1.lt.0.0_rk) )  then
!         if ( slope0*slope1.lt.0.0_rk )  then
!            damp=abs(damp_read)
!            !write(*,*) '>>>>>>>>>>>here!!!'
!         endif
!
!      endif
!   else
!      damp=abs(damp_read)
!   endif

#ifdef MPI
  endif
  call mpi_bcast(damp,1,  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
#endif


   !  STEP 5: Updated induced charges/dipoles
   if (n_iter.gt.3) relax_dummy=.true.
   if (is_cr) call update_chg
   call update_dip 

#ifdef MPI  
  if(proc_id==0) then
#endif
   if (write_rst) then
      call write_q_mu_V_F(fn_rst)
   endif
#ifdef MPI  
  endif
#endif

   !  STEP 6: Compute energy
   call calc_energy(ene)
   energy(n_iter)=ene*iu2ev


   
   !  STEP 7: Compute difference to previous iteration
   if (is_me) then
      call diff_me
!      write(100,o1fmt) n_iter,energy(n_iter),deltaE,dmu_avg,dmu_max
   elseif (is_cr) then
      call diff_cr
!      write(100,o1fmt) n_iter,energy(n_iter),deltaE,dq_avg,dq_max,dmu_avg,dmu_max
   endif
   DE_all(n_iter)=deltaE

   

   !  STEP 8 : Evaluate exit condition  
   ! Energy exceeds specified threshold
   if (abs(energy(n_iter)).ge.Emax) then
      write(*,*) "ERROR: absolute value of energy is too large (probable divergence)"
      
      fname_failed=trim(root_fname)//"_mu_F_failed.dat"
      call write_dip_field(fname_failed)
      write(*,*) 'Electric fields and induced dipoles wrote to ',trim(fname_failed)
      call stop_exec
   endif

   ! Evaluate convergence criterium (over last 3 steps)
   if (n_iter.ge.3) then 
     if (maxval(abs(DE_all(n_iter-2:n_iter))).lt.tolE) then
      is_converged=.true.
      exit
     endif
   endif

enddo  ! loop on iterative cycles

#ifdef MPI  
  if(proc_id==0) then
#endif
write(*,"(a)")' ---------------------------------------------------------------------------------------------'
write(*,*)
write(*,*)
#ifdef MPI  
  endif
#endif




#ifdef MPI
  if(proc_id==0) then
#endif

if (is_converged) then
   energy_total=energy(n_iter)
   energy_induction=energy_total-energy_elstat
   call success_msg(n_iter,energy_total)
   call write_dipole_crp_pp

else
   call failure_msg(max_it)
endif   

#ifdef MPI
  endif
#endif


! GD 10/03/21  2clear
! previous version  
!if (is_converged) then
!   energy_total=energy(n_iter) 
!   energy_induction=energy_total-energy_elstat
!#ifdef MPI  
!  if(proc_id==0) then
!#endif
!     call success_msg(n_iter,energy_total)
!     call write_dipole_crp_pp
!
!#ifdef MPI  
!  endif
!#endif
!else
!   call failure_msg(max_it)
!endif
!close(100)


! GD  22/03/2017 Write the final "restart" file 

! update site potential (not done in ME after the 0-th iteration) 
if (is_me) then

   call calc_V_dip_v2   
   call calc_V_tot

!   if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then
!      call calc_V_dip_rep  
!   else
!      call calc_V_dip   
!   endif

endif

#ifdef MPI  
  if(proc_id==0) then
#endif
if (.not.write_rst) fn_rst=trim(root_fname)//".rst"
!fn_rst=trim(adjustl(fn_rst))     
call write_q_mu_V_F(fn_rst)

write(*,*) 'Final restart wrote to ',trim(fn_rst)
write(*,*)
#ifdef MPI  
  endif
#endif

end subroutine selfconsistent_solution


subroutine dipole_correction
! GD 20/05/21 
! Dipole corrections in PBC calculations
  implicit none
  integer       :: i_mol
  logical       :: found
  real(rk)      :: dip0(3),Volume 
  character(50) :: key
  real(rk)      :: Fcorr(3),Vmin,Vmax,Vtmp,Verr,slab_normal(3)

  
if (is_periodic_2D_Rc.or.is_periodic_3D_Rc)  then

#ifdef MPI
     if(proc_id==0) then
#endif
     
     key="DIPOLE_CORRECTION"
     call parse_log(key,use_dipcorr,found)
     if (found) then 
        write(*,"(1x,a20,a2,l1)") key,': ',use_dipcorr
     else
        write(*,"(1x,a20,a2,l1,a10)") key,': ',use_dipcorr,' (default)'
     endif
     write(*,*)
     write(*,*) ' INFO: you are runing a calculations with periodic boundary conditions.         '
     write(*,*) '       Be aware that the presence of a spurious net permanent polarization in   '
     write(*,*) '       the cell can determine artifacts.' 
     write(*,*)

    
     ! determine the compensating field 
     call calc_dipole_pc(dip0)
     Volume=DotProd3(cell2rep(1,:),VectorProd3(cell2rep(2,:),cell2rep(3,:)))

     if (is_periodic_3D_Rc) then
        Fcorr= 1e10*dip0*e0/(3*eps0*Volume)
        
     elseif (is_periodic_2D_Rc) then

        call calc_slab_normal(slab_normal)
        Fcorr= Dotprod3(dip0,slab_normal) * slab_normal
        Fcorr= 1e10*Fcorr*e0/(eps0*Volume)
        
     endif
     
     
     ! estimate the error
     Vmax=-1e8
     Vmin=+1e8    
     do i_mol=1,sys_info%nmol_tot ! loop on all molecules

        Vtmp=DotProd3(sys(i_mol)%cm,Fcorr)
        if  (Vtmp.lt.Vmin) Vmin=Vtmp
        if  (Vtmp.gt.Vmax) Vmax=Vtmp
     enddo
     Verr = Vmax - Vmin

     
     write(*,"(a25,3(f12.4),a20)") ' Dipole: ',dip0,' e*Angstrom'
     write(*,"(a25,f12.4,a20)")    ' Volume: ',Volume, ' Angstrom**3'     
     write(*,"(a25,f12.6,a20)")    ' Polarization : ',norm3(dip0)/Volume, ' e/Angstrom**2 '
     write(*,"(a25,3(f12.4),a20)") ' Compensating field : ', Fcorr, ' V*Angstrom '
     write(*,"(a40,f12.4,a2)") ' Potential difference error (estimate) : ',Verr, ' V '
     write(*,*)
     write(*,*) 

     if (.not.use_dipcorr) then
        write(*,*) ' If the error above is too much for your scope, a dipole correction (uniformly polarized' 
        write(*,*) ' compensating background) can be enabled with the input keyword :  DIPOLE_CORRECTION T '
        write(*,*)
        write(*,*)    

     else
        write(*,*) ' INFO: The dipole correction (uniformly polarized compensating background) will be applied'
        write(*,*) 
        write(*,*)
           
     endif

#ifdef MPI
  endif

  call mpi_bcast(use_dipcorr  ,1,  MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(Fcorr        ,3,  MPI_DOUBLE ,0,MPI_COMM_WORLD,ierr)
#endif 

  
  
  if (use_dipcorr) call dipole_corr_FVpc( Fcorr )

     
endif

  
end subroutine dipole_correction


subroutine save_2_im1
  implicit none
  integer   :: i

do i=1,sys_info%nmol_tot ! loop on all molecules 
   sys(i)%dq_im1=sys(i)%dq_i
   sys(i)%mu_im1=sys(i)%mu_i
enddo


end subroutine save_2_im1



subroutine diff_cr
! This subroutine computes the differences between subsequent iteration steps
  implicit none
  integer       :: i_mol,i,i_type
  real(rk)      :: n_q,n_mu,dd
  character(60) :: d_fmt
  

! Energy difference 
if (n_iter.eq.1) then 
   deltaE=energy(1)-energy_0
else
   deltaE=energy(n_iter)-energy(n_iter-1)
endif

! Average and maximum difference between charges
dq_avg=0.0_rk
dq_max=0.0_rk
dmu_avg=0.0_rk
dmu_max=0.0_rk
n_q=0.0_rk
n_mu=0.0_rk
do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i_mol)%moltype_id

   do i=1,n_crp(i_type)  ! loop on crp 
      n_q=n_q+1.0_rk

      dd=abs(sys(i_mol)%dq_i(i) - sys(i_mol)%dq_im1(i))
      dq_avg=dq_avg + dd
      if (dd.gt.dq_max) dq_max=dd
   enddo

   do i=1,n_pp(i_type)  ! loop on pp 
      n_mu=n_mu+1.0_rk
     
      dd=Norm3(sys(i_mol)%mu_i(i,:) - sys(i_mol)%mu_im1(i,:))
      dmu_avg=dmu_avg + dd 
      if (dd.gt.dmu_max) dmu_max=dd
   enddo

enddo


dq_avg =dq_avg/n_q
dmu_avg=dmu_avg/n_mu
dmu_max=dmu_max

#ifdef MPI
if (proc_id==0) then
#endif

d_fmt="(i5,1x,f5.2,1x,2(f16.8,1x),4(f12.8,1x),1x,f6.3)"
write(*,d_fmt) n_iter,damp,energy(n_iter),deltaE,dq_avg,dq_max,dmu_avg,dmu_max

! 2clear
!write(33,*) energy(n_iter),deltaE,damp

#ifdef MPI
endif
#endif

end subroutine diff_cr


subroutine diff_me
! This subroutine computes the differences between subsequent iteration steps
  implicit none
  integer    :: i_mol,i,i_type
  real(rk)    :: n_mu,dd
  character(60) :: d_fmt


! Energy difference 
if (n_iter.eq.1) then 
   deltaE=energy(1)-energy_0
else
   deltaE=energy(n_iter)-energy(n_iter-1)
endif

! Average and maximum difference between charges
dmu_avg=0.0_rk
dmu_max=0.0_rk
n_mu=0.0_rk
do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i_mol)%moltype_id

   do i=1,n_pp(i_type)  ! loop on pp 
      n_mu=n_mu+1.0_rk
     
      dd=Norm3(sys(i_mol)%mu_i(i,:) - sys(i_mol)%mu_im1(i,:))
      dmu_avg=dmu_avg + dd 
      if (dd.gt.dmu_max) dmu_max=dd
   enddo

enddo

dmu_avg=dmu_avg/n_mu
dmu_max=dmu_max

#ifdef MPI
if (proc_id==0) then
#endif

   d_fmt="(i5,1x,f5.2,1x,2(f16.8,1x),2(f12.8,1x))"
write(*,d_fmt) n_iter,damp,energy(n_iter),deltaE,dmu_avg,dmu_max

! 2clear
!write(33,*) energy(n_iter),deltaE,damp

#ifdef MPI
endif
#endif

end subroutine diff_me


subroutine init_0
! Initializes to zero induced charges and dipoles and all 
! field and potential at atoms
  implicit none
  integer   :: i

do i=1,sys_info%nmol_tot 

   sys(i)%dq_i=0.0_rk
   sys(i)%dq_im1=0.0_rk

   sys(i)%mu_i=0.0_rk    
   sys(i)%mu_im1=0.0_rk 

   sys(i)%V_pc=0.0_rk
   sys(i)%V_0=0.0_rk
   sys(i)%V_dq=0.0_rk
   sys(i)%V_id=0.0_rk
   sys(i)%V_tot=0.0_rk

   sys(i)%F_pc=0.0_rk
   sys(i)%F_0=0.0_rk   
   sys(i)%F_dq=0.0_rk
   sys(i)%F_id=0.0_rk
   sys(i)%F_tot=0.0_rk

enddo


end subroutine init_0


end module selfconsistent





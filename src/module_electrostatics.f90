!    This file is part of MESCal.
!    Copyright Gabriele D'Avino (2013)
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.
!
module electrostatics
  ! Subroutines for calculations related to electrostatics
  use io
  use types
#ifdef MPI
  use module_mpi
#endif

contains


subroutine add_Fext_pc(fn_ext)
  ! GD 31/9/20: compute field and potential of a set of point charges read from file
  ! and add that to the external potential
  implicit none
  character(50) :: fn_ext
  integer       :: i,j,k,i_type,N
  real(rk)      :: V,F(3),r(3) !,Pot_chg,Field_chg
  real(rk),allocatable,dimension(:,:)  :: qxyz(:,:)

open(file=fn_ext,unit=22,status="old") 
read(22,*) N
allocate( qxyz(N,4) )
qxyz=0.0_rk

do i=1,N
   read(22,*) qxyz(i,:)
enddo
close(22)

write(*,*) 'INFO: Point charges were read from file!'
write(*,*) 


do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id

   do j=1,n_pp(i_type)  ! loop on atoms
      
      V=0.0_rk
      F=0.0_rk
     
      r=sys(i)%r_at(j,:)

      do k=1,N
         V= V + Pot_chg(   r, qxyz(k,2:4), qxyz(k,1) )
         F= F + Field_chg( r, qxyz(k,2:4), qxyz(k,1) )
      enddo
      
      sys(i)%V_ext(j)=sys(i)%V_ext(j) + V
      sys(i)%F_ext(j,:)=sys(i)%F_ext(j,:) + F

   enddo

enddo


  
end subroutine add_Fext_pc

  
subroutine add_VF_exc_pc
  implicit none
  integer  :: i_mol,i_type,k_crp,j_mol,j_type,jj_mol,l_crp

  
call read_chg_diff

 do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
    i_type=sys(i_mol)%moltype_id


    ! if this is charged molecule add field and potential of its 
    ! excess charge on all the others
    if (abs(sys(i_mol)%chg_mol).gt.0.9) then

 
        do j_mol=1,sys_info%nmol_tot
        if (j_mol.ne.i_mol)  then

           j_type=sys(j_mol)%moltype_id

           ! potential 
           do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
              do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                
                 sys(j_mol)%V_pc(l_crp)=sys(j_mol)%V_pc(l_crp) + &
                      Pot_chg( sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
                      sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
                      sys_info%chg_diff(k_crp,i_type) )
              enddo
           enddo
  
           ! field 
           do k_crp=1,n_crp(i_type)  ! loop on pp of i_mol 
              do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                 sys(j_mol)%F_pc(l_crp,:)=sys(j_mol)%F_pc(l_crp,:) + &
                      Field_chg( sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
                      sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
                      sys_info%chg_diff(k_crp,i_type) )

              enddo
           enddo

        endif
        enddo

     endif
 enddo


end subroutine add_VF_exc_pc


!  backup copy
! subroutine add_VF_exc_pc
!   implicit none
!   integer  :: i_mol,i_type,k_crp,k_pp,j_mol,j_type,jj_mol,l_crp
! 
! call read_chg_diff
! 
! do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!    i_type=sys(i_mol)%moltype_id
! 
!    do jj_mol=1,sys(i_mol)%nnb ! loop on the nb list of i_mol
!       j_mol=sys(i_mol)%nb_list(jj_mol)
!       j_type=sys(j_mol)%moltype_id
! 
!       ! add only the contribution of molecules with permanent charge 
!       if (abs(sys(j_mol)%chg_mol).gt.0.9) then
! 
!          ! potential 
!          do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!             do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!               
!                sys(i_mol)%V_pc(k_crp)=sys(i_mol)%V_pc(k_crp)+ &
!                     Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                     sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                     sys_info%chg_diff(l_crp,j_type) )
!             enddo
!          enddo
! 
!          ! field 
!          do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!             do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!                sys(i_mol)%F_pc(k_pp,:)=sys(i_mol)%F_pc(k_pp,:) + &
!                     Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                     sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                     sys_info%chg_diff(l_crp,j_type) )
!             enddo
!          enddo
! 
!       endif
! 
!    enddo  ! on mol_j
! 
! enddo  ! on mol_i
! 
! end subroutine add_VF_exc_pc


subroutine calc_VF_chg_cutoff
! calulate potentials and field at atoms due to charges  
! calculations include replica and consider cutoff

  implicit none
  integer    :: i,i_mol,j_mol,i_at,j_at,i_type,j_type
  integer    :: i1,i2,i3,n_rep(3) 
  real(rk)   :: v1(3),v2(3),v3(3),Rij,trasl(3)

n_rep=0
do i=1,3
   n_rep(i)=ceiling(3.00*Rc_rep/norm3(cell2rep(1,:)))
enddo

if(is_periodic_2D_Rc) then
   n_rep(u_tf)=0
endif

v1=cell2rep(1,:)
v2=cell2rep(2,:)
v3=cell2rep(3,:)

write(*,*)
do i_mol=1,sys_info%nmol_tot  ! loop on mols in the cell
   i_type=sys(i_mol)%moltype_id

   sys(i_mol)%V_pc=0.0_rk
   sys(i_mol)%F_pc=0.0_rk
   
   write(*,*) 'Computing molecule ',i_mol,'/',sys_info%nmol_tot

   do i_at=1,n_pp(i_type) ! loop on atoms of mol i

      do i1=-n_rep(1),n_rep(1) ! loop on replica
      do i2=-n_rep(2),n_rep(2)
      do i3=-n_rep(3),n_rep(3)
         
         trasl=i1*v1 + i2*v2 + i3*v3
         do j_mol=1,sys_info%nmol_tot ! loop on other mols
            j_type=sys(j_mol)%moltype_id

            Rij=norm3( sys(j_mol)%cm + trasl - sys(i_mol)%cm )
            if ( (Rij.le.Rc_rep) .and. (Rij.gt.0.10_rk) ) then
               ! the 2nd condition is to avoid self interaction!

               do j_at=1,n_pp(j_type) ! loop on atoms of mol j

                  ! call to function Pot_chg(r,r_q,q)
                  sys(i_mol)%V_pc(i_at)=sys(i_mol)%V_pc(i_at) + &
                       Pot_chg( sys(i_mol)%r_at(i_at,:), &
                                sys(j_mol)%r_at(j_at,:) + trasl, &
                                sys(j_mol)%q_i(j_at) )
                  
                  ! call to function Field_chg(r,r_q,q)
                  sys(i_mol)%F_pc(i_at,:)=sys(i_mol)%F_pc(i_at,:) + &
                       Field_chg( sys(i_mol)%r_at(i_at,:), &
                                  sys(j_mol)%r_at(j_at,:) + trasl, &
                                  sys(j_mol)%q_i(j_at) )


               enddo

            endif

         enddo

      enddo
      enddo
      enddo

   enddo

enddo
write(*,*)

end subroutine calc_VF_chg_cutoff


subroutine sitene_elstat_v2(fn_out)
! GD 28/05/21
  implicit none
  integer       :: i,j,i_type
  character(50) :: fn_out
  real(rk)      :: DE,DE0,Vm,q,Vm0

open(file=fn_out,unit=48,status="replace")
write(48,"(a102)") "%1.i_mol 2.mol_type  3-5.xyz com (A)  6.Delta_E (eV)  7.DeltaE0 (eV)  8.avg pot (V)  9.avg pot0 (V) "


do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id

   DE=0.0_rk
   DE0=0.0_rk
   Vm=0.0_rk
   Vm0=0.0_rk
   q=0.0_rk
   do j=1,n_pp(i_type)  ! loop on pp 
      DE = DE  + sys(i)%V_tot(j) * ( sys(i)%dq_i(j) - sys(i)%q_i(j) )
      DE0= DE0 + sys(i)%V_0(j) * ( sys(i)%dq_i(j) - sys(i)%q_i(j) )
      Vm = Vm  + sys(i)%V_tot(j)
      Vm0 = Vm0  + sys(i)%V_0(j)
      q  = q +  sys(i)%dq_i(j) 
   enddo

   Vm= Vm /n_pp(i_type)
   Vm0= Vm0 /n_pp(i_type)  

   if ( abs(q).lt.1e-4 ) q=-1.0_rk ! this is for excitons (particles as electrons)
   DE = DE  * sign( 1.0_rk , -q ) 
   DE0= DE0 * sign( 1.0_rk , -q )
   write(48,"(2(i6),10(f12.6))") i,i_type,sys(i)%cm,iu2ev*DE,iu2ev*DE0,iu2ev*Vm,iu2ev*Vm0

enddo
close (48)

write(*,*) 'Eletrostatic site energies wrote to ',trim(fn_out)
write(*,*)

end subroutine sitene_elstat_v2


subroutine sitene_elstat(fn_out)
  implicit none
  integer       :: i,j,i_type
  character(50) :: fn_out
  real(rk)      :: S

open(file=fn_out,unit=48,status="replace")
write(48,*) "%1.i_mol 2.Electrostatic site energy (eV) 3-5.xyz com (A)"
 
do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id

   S=0.0_rk
   do j=1,n_pp(i_type)  ! loop on pp 
      S= S + sys(i)%V_tot(j) * ( sys(i)%dq_i(j) - sys(i)%q_i(j) )
   enddo

   write(48,"(i6,4(f14.6))") i,iu2ev*S,sys(i)%cm
enddo
close (48)

write(*,*) 'Data wrote to ',trim(fn_out)
write(*,*)

end subroutine sitene_elstat




subroutine total_charge_debug(q_tot,it)
  implicit none
  real(rk)   :: q_tot 
  integer   :: i,j,i_type,it

q_tot=0.0_rk
!q_i=0.0_rk
!qqq=0.0_rk
do i=1,sys_info%nmol_tot ! loop on all molecules 
   i_type=sys(i)%moltype_id
   do j=1,n_crp(sys(i)%moltype_id) ! loop on the crp of each molecule
      q_tot=q_tot + sys(i)%q_i(j)
!      if (i.eq.2) then
!         q_i=q_i + sys(i)%q_i(j)
!         qqq(j)=sys(i)%q_i(j)
!      endif
   enddo
enddo
!write(17,*) 
end subroutine total_charge_debug

subroutine total_charge_chk
  implicit none
  integer   :: i,j,i_type,it
  real(rk)   :: q_tot

q_tot=0.0_rk
do i=1,sys_info%nmol_tot ! loop on all molecules 
   i_type=sys(i)%moltype_id
   do j=1,n_crp(sys(i)%moltype_id) ! loop on the crp of each molecule
      q_tot=q_tot + sys(i)%q_i(j)
   enddo
enddo

if (abs(q_tot-sys_info%total_chg).gt.1e-6) then
   write(*,*)
   write(*,"(a)")        '  WARNING:  total charge not conseved ! ' 
   write(*,"(a,f16.8)")  '      Net charge at 0-th iteration: ',sys_info%total_chg
   write(*,"(a,f16.8)")  '      Net charge at this iteration: ',q_tot
   write(*,*)
endif
end subroutine total_charge_chk


subroutine write_dipole_crp_pp
! this subroutine computes the dipole due to charges and induced 
! dipoles of the single molecules and of the whole system.
! The dipole of each molecule is wrote to file.
! GD 03/03/21  Adding warnings in case of suspiciously large induced dipoles/charges 
  implicit none
  integer   :: i,j,i_type,it
  real(rk)  :: dipole_tot(3),dipole_mol(3)
  character(50) :: ofmt="(i6,1x,3(f16.6,1x),4(f16.6,1x))",fout_dip,wrn_fmt
  real(rk)  ::  factor,dip_thr,dq_thr
 
fout_dip=trim(root_fname)//"_dipole.dat"
open(file=fout_dip,unit=102,status="replace")
write(102,"(a)") "% 1.idx  2.cm_x  3.cm_y  4.cm_z  5.mu_x  6.mu_y  7.mu_z 8 mu_abs "

dip_thr=0.20_rk
dq_thr=0.20_rk

wrn_fmt=""
wrn_fmt="(a65,i6,a3,i6,a8,f12.3)"

dipole_tot=0.0_rk  
do i=1,sys_info%nmol_tot ! loop on all molecules 
   i_type=sys(i)%moltype_id
   dipole_mol=0.0_rk

   do j=1,n_crp(i_type) ! loop on the crp of each molecule
      dipole_mol=dipole_mol + sys(i)%q_i(j)*sys(i)%r_at(i_crp(j,i_type),:)

      if  (abs( sys(i)%dq_i(j) ).gt.dq_thr ) then
         write(*,wrn_fmt) '  WARNING: suspiciously large induced charge at molecule / atom ',i,' / ',j, &
                    ' dq= ',sys(i)%dq_i(j)
      endif
   enddo

   do j=1,n_pp(i_type) ! loop on the pp of each molecule
      dipole_mol=dipole_mol + sys(i)%mu_i(j,:)

      if  (norm3( sys(i)%mu_i(j,:) ).gt.dip_thr ) then
         write(*,wrn_fmt) '  WARNING: suspiciously large induced dipole at molecule / atom ',i,' / ',j, &
                    '  |mu|= ',norm3( sys(i)%mu_i(j,:) )
      endif
      
   enddo

   dipole_tot=dipole_tot + dipole_mol
   sys(i)%mu_mol=dipole_mol
   write(102,ofmt) i,sys(i)%cm,dipole_mol,Norm3(dipole_mol)
enddo 

sys_info%dipole_tot=dipole_tot
write(*,*) 
write(*,"(a,3(f10.3),a)") ' Total dipole of the system (e*Angstrom): ',dipole_tot,' g_DIPTOT'
write(*,*) 
write(*,*) 'Dipole moments and centers of mass of individual' 
write(*,*) 'molecules wrote to file dipole.dat '
write(*,*) 
close(102)


if ( use_Fext_uni .and. (is_periodic_2D_Rc.or.is_periodic_3D_Rc) ) then
   factor=16.0217657/(CellVolume*F0*eps0)
      
   chi0=factor*dipole_tot
   write(*,*) 
   write(*,*) ' Susceptibility to the external field, defined as: '
   write(*,*) ' P=eps0 * zeta * F0   (dimensionless, SI units)' 
   write(*,*) 
   write(*,"(a8,3(f18.7),a)") ' zeta= ',chi0,'    g_ZETA' 
   write(*,*) 

endif


end subroutine write_dipole_crp_pp


subroutine calc_dipole_pp(dipole_pp)
! this subroutine computes the dipole due to induced 
! dipoles of the whole system.
  implicit none
  integer   :: i,j,i_type
  real(rk)   :: dipole_pp(3)

dipole_pp=0.0_rk  
do i=1,sys_info%nmol_tot ! loop on all molecules 
   i_type=sys(i)%moltype_id
   
   do j=1,n_pp(i_type) ! loop on the pp of each molecule
      dipole_pp = dipole_pp + sys(i)%mu_i(j,:)
   enddo

enddo 

end subroutine calc_dipole_pp


subroutine calc_dipole_pc(dipole)
! GD 07/05/21
! this subroutine computes the dipole due to permanent charges 
  implicit none
  integer   :: i,j,i_type
  real(rk)   :: dipole(3)

dipole=0.0_rk  
do i=1,sys_info%nmol_tot ! loop on all molecules 
   i_type=sys(i)%moltype_id
   
   do j=1,n_pp(i_type) ! loop on the pp of each molecule
      dipole=dipole + sys(i)%q_0(j) * sys(i)%r_at(j,:)
   enddo

enddo 

end subroutine calc_dipole_pc


subroutine calc_centroid(Rc) 
! GD 07/05/21
! compute atomic coordinate centroid
  implicit none
  integer               :: i,i_mol,i_type,n_avg
  real(rk),intent(out)  :: Rc(3)

Rc=0.0_rk
n_avg=0
do i_mol=1,sys_info%nmol_tot ! loop on all molecules
   i_type=sys(i_mol)%moltype_id

   do i=1,n_crp(i_type)  ! loop on atoms
      n_avg=n_avg+1

      Rc(1)=Rc(1) + sys(i_mol)%r_at(i_crp(i,i_type),1)
      Rc(2)=Rc(2) + sys(i_mol)%r_at(i_crp(i,i_type),2)
      Rc(3)=Rc(3) + sys(i_mol)%r_at(i_crp(i,i_type),3)
   enddo
enddo
Rc=Rc/n_avg

end subroutine calc_centroid


subroutine  calc_slab_normal(vn)
! GD 07/05/21
  implicit none
  integer               :: i_mol
  real(rk)              :: v1(3),v2(3),vn(3)

if (u_tf.eq.3) then     ! ab plane film
   v1=cell2rep(1,:)
   v2=cell2rep(2,:)
elseif (u_tf.eq.2) then ! ac plane film
   v1=cell2rep(1,:)
   v2=cell2rep(3,:)
elseif (u_tf.eq.1) then ! bc plane film
   v1=cell2rep(2,:)
   v2=cell2rep(3,:)
endif

vn=VectorProd3(v1,v2)
vn=vn/Norm3(vn)
  
end subroutine calc_slab_normal


subroutine dipole_corr_FVpc( Fc )
! GD 20/05/21
! correct potential and field of permanent sources for spurious dipole effects
  implicit none
  integer   :: i,i_mol,i_type,proc_id_source
  real(rk)  :: Fc(3),r0(3)


call calc_centroid(r0)

Fc=ev2iu*Fc

do i_mol=1,sys_info%nmol_tot ! loop on all molecules

#ifdef MPI
   if(modulo(i_mol,num_procs)==proc_id) then
#endif
      
   i_type=sys(i_mol)%moltype_id
   
     do i=1,n_crp(i_type)  ! loop on atoms

        sys(i_mol)%F_pc(i,:)= sys(i_mol)%F_pc(i,:) + Fc 
        sys(i_mol)%V_pc(i)= sys(i_mol)%V_pc(i) - DotProd3( Fc , (sys(i_mol)%r_at(i,:) - r0)  )

     enddo

#ifdef MPI
  endif
#endif
  
  enddo


#ifdef MPI
  ! update V_pc and F_pc for each proc
  do i_mol=1,sys_info%nmol_tot

     i_type=sys(i_mol)%moltype_id
     proc_id_source=modulo(i_mol,num_procs)
     
     call mpi_bcast(sys(i_mol)%V_pc(1:n_crp(i_type))   ,  n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
     call mpi_bcast(sys(i_mol)%F_pc(1:n_crp(i_type),:) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)

  enddo

  
#endif

  
end subroutine dipole_corr_FVpc

      
subroutine calc_energy(ene) 
! calculation of the energy of the system with
! eq 19 of Tsiper, Soos, Phys Rev B 64, 195124 (2001)
  implicit none
  integer              :: i_mol,i,j,i_type
  real(rk),intent(out)  :: ene
  
  !local variable
  real(rk) :: tmp

ene=0.0_rk
do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

#ifdef MPI
  if(modulo(i_mol,num_procs)==proc_id) then
#endif
   i_type=sys(i_mol)%moltype_id

   do i=1,n_crp(i_type)  ! loop on crp 
      ene=ene + sys(i_mol)%q_i(i)*sys(i_mol)%V_0(i)
   enddo

   do i=1,n_pp(i_type)  ! loop on pp 
      ene=ene - DotProd3(sys(i_mol)%mu_i(i,:),sys(i_mol)%F_0(i,:))
   enddo
   
#ifdef MPI
   endif
#endif
enddo

#ifdef MPI
  call mpi_allreduce(ene, tmp, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD, ierr)
  ene=tmp
#endif
ene=ene*0.5_rk  

end subroutine calc_energy



subroutine calc_energy_q_only(ene) 
! same as calc_energy but for omitting dipoles 
  implicit none
  integer              :: i_mol,i,j,i_type
  real(rk),intent(out)  :: ene

ene=0.0_rk
do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i_mol)%moltype_id

   do i=1,n_crp(i_type)  ! loop on crp 
      ene=ene + sys(i_mol)%q_i(i)*sys(i_mol)%V_0(i)
   enddo

enddo
ene=ene*0.5_rk  

end subroutine calc_energy_q_only


subroutine update_chg
  implicit none
  integer    :: i_mol,i,j,i_type,n_it
  real(rk)    :: dq_undamped

do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

#ifdef MPI
  if(modulo(i_mol,num_procs)==proc_id) then
#endif
   i_type=sys(i_mol)%moltype_id

   do i=1,n_crp(i_type)  ! loop on crp 
      dq_undamped=0.0_rk

      do j=1,n_crp(i_type)  ! loop on crp
         dq_undamped=dq_undamped - aap(i_type,i,j)*sys(i_mol)%V_tot(j)
      enddo

      sys(i_mol)%dq_i(i)=damp*sys(i_mol)%dq_im1(i) + (1.0_rk-damp)*dq_undamped

      sys(i_mol)%q_i(i)=sys(i_mol)%q_0(i) + sys(i_mol)%dq_i(i)

   enddo
#ifdef MPI
   endif
#endif
enddo

#ifdef MPI
! update dq_i and q_i for each proc
do i_mol=1,sys_info%nmol_tot

  i_type=sys(i_mol)%moltype_id
  proc_id_source=modulo(i_mol,num_procs)
  call mpi_bcast(sys(i_mol)%dq_i(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
  call mpi_bcast(sys(i_mol)%q_i(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
enddo
#endif
end subroutine update_chg



subroutine update_dip
  implicit none
  integer    :: i_mol,i,i_type,n_it
  real(rk)    :: mu_undamped(3)

do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

#ifdef MPI
  if(modulo(i_mol,num_procs)==proc_id) then
#endif
   i_type=sys(i_mol)%moltype_id

   if (.not.(is_dummy(i_type)).or.(relax_dummy))  then 

   do i=1,n_pp(i_type)  ! loop on pp 
      mu_undamped=0.0_rk

      ! mu_x
      mu_undamped(1)=sys(i_mol)%acp(i,1)*sys(i_mol)%F_tot(i,1) + &
                     sys(i_mol)%acp(i,2)*sys(i_mol)%F_tot(i,2) + &
                     sys(i_mol)%acp(i,3)*sys(i_mol)%F_tot(i,3) 
      ! mu_y
      mu_undamped(2)= sys(i_mol)%acp(i,2)*sys(i_mol)%F_tot(i,1) + &
                      sys(i_mol)%acp(i,4)*sys(i_mol)%F_tot(i,2) + &
                      sys(i_mol)%acp(i,5)*sys(i_mol)%F_tot(i,3) 
      ! mu_z
      mu_undamped(3)= sys(i_mol)%acp(i,3)*sys(i_mol)%F_tot(i,1) + &
                      sys(i_mol)%acp(i,5)*sys(i_mol)%F_tot(i,2) + &
                      sys(i_mol)%acp(i,6)*sys(i_mol)%F_tot(i,3) 

      sys(i_mol)%mu_i(i,:)=damp*sys(i_mol)%mu_im1(i,:) + (1.0_rk-damp)*mu_undamped
   enddo

   endif  
#ifdef MPI
   endif
#endif
enddo

#ifdef MPI
! update mu_i for each proc
do i_mol=1,sys_info%nmol_tot

  i_type=sys(i_mol)%moltype_id
  proc_id_source=modulo(i_mol,num_procs)
  call mpi_bcast(sys(i_mol)%mu_i(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
enddo
#endif

end subroutine update_dip


subroutine calc_F_tot
! calculate total field at atoms by summing up all contributions
  implicit none
  integer    :: i_mol,i,i_type

do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i_mol)%moltype_id
   sys(i_mol)%F_tot=0.0_rk

   do i=1,n_pp(i_type)  ! loop on pp 
      sys(i_mol)%F_tot(i,:)=sys(i_mol)%F_0(i,:) + sys(i_mol)%F_dq(i,:) + sys(i_mol)%F_id(i,:)
   enddo

enddo

end subroutine calc_F_tot


subroutine calc_V_tot
! calculate total potential at atoms by summing up all contributions
  implicit none
  integer    :: i_mol,i,i_type

do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i_mol)%moltype_id
   sys(i_mol)%V_tot=0.0_rk

   do i=1,n_crp(i_type)  ! loop on pp 
      sys(i_mol)%V_tot(i)=sys(i_mol)%V_0(i) + sys(i_mol)%V_dq(i) +sys(i_mol)%V_id(i) 
   enddo

enddo

end subroutine calc_V_tot


subroutine calc_V0_F0
! calculate field due to permanent souces: charges + external
  implicit none
  integer    :: i_mol,i,i_type

do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i_mol)%moltype_id

   sys(i_mol)%V_0=0.0_rk
   sys(i_mol)%F_0=0.0_rk

   do i=1,n_crp(i_type)  ! loop on crp
      sys(i_mol)%V_0(i)=sys(i_mol)%V_pc(i) + sys(i_mol)%V_ext(i)
   enddo

   do i=1,n_pp(i_type)  ! loop on pp 
      sys(i_mol)%F_0(i,:)=sys(i_mol)%F_pc(i,:) + sys(i_mol)%F_ext(i,:)
   enddo

   sys(i_mol)%V_tot=sys(i_mol)%V_0
   sys(i_mol)%F_tot=sys(i_mol)%F_0
enddo


end subroutine calc_V0_F0



subroutine calc_V_pc_v2
  ! Compute potentials at atoms due to permanent charges
  ! GD 07/04/21  NEW VERSION superseding calc_V_pc, calc_V_pc_rep
  !  The new version features:
  !  - A single routine for open-boundary and periodic (replica) calculations
  implicit none
  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
  real(rk)   :: q_ima,r_ima(3),q_rep

  
  do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

#ifdef MPI
     if(modulo(i_mol,num_procs)==proc_id) then
#endif
        i_type=sys(i_mol)%moltype_id
        sys(i_mol)%V_pc=0.0_rk

        do j_mol=1,sys_info%nmol_tot
           j_type=sys(j_mol)%moltype_id

           if (j_mol.ne.i_mol)  then
              do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol
              do l_crp=1,n_crp(j_type)  ! loop on crp of j_mol
                 sys(i_mol)%V_pc(k_crp)=sys(i_mol)%V_pc(k_crp)+ &
                      Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
                      sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
                      sys(j_mol)%q_0(l_crp) )
              enddo
              enddo
           endif


           if (is_mirr) then    ! image charges
              
              do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol
              do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                 q_ima=-sys(j_mol)%q_0(l_crp)
                 r_ima=sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
                 r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)

                 sys(i_mol)%V_pc(k_crp)=sys(i_mol)%V_pc(k_crp)+ &
                      Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), r_ima, q_ima)
              enddo
              enddo
           endif

        enddo  ! on mol_j


        if  (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  ! periodic replica


           do i_rep=1,n_rep
              !           mol idx                   ! atomic idx
              q_rep=sys(idx_rep(i_rep,2))%q_0(idx_rep(i_rep,3))

              do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol
                 sys(i_mol)%V_pc(k_crp)=sys(i_mol)%V_pc(k_crp) + &
                      Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
                      r_rep(i_rep,:),q_rep)
              enddo
           enddo
        endif

        
#ifdef MPI
     endif
#endif
  enddo  ! on mol_i

#ifdef MPI
  ! update V_pc for each proc
  do i_mol=1,sys_info%nmol_tot

     i_type=sys(i_mol)%moltype_id
     proc_id_source=modulo(i_mol,num_procs)
     call mpi_bcast(sys(i_mol)%V_pc(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)

  enddo
#endif


end subroutine calc_V_pc_v2



subroutine calc_F_pc_v2
  ! Compute fields at atoms due to permanent charges
  ! GD 07/04/21  NEW VERSION superseding calc_F_pc, calc_F_pc_rep
  !  The new version features:
  !  - A single routine for open-boundary and periodic (replica) calculations
  implicit none
  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
  real(rk)   :: q_ima,r_ima(3),q_rep

  
  do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

#ifdef MPI
     if(modulo(i_mol,num_procs)==proc_id) then
#endif
        i_type=sys(i_mol)%moltype_id
        sys(i_mol)%F_pc=0.0_rk

        do j_mol=1,sys_info%nmol_tot
           j_type=sys(j_mol)%moltype_id

           if (j_mol.ne.i_mol)  then

              do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
              do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                 sys(i_mol)%F_pc(k_pp,:)=sys(i_mol)%F_pc(k_pp,:) + &
                      Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                      sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
                      sys(j_mol)%q_0(l_crp) )
              enddo
              enddo
           endif

           
        if (is_mirr) then    ! image charges

           do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
           do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
              q_ima=-sys(j_mol)%q_0(l_crp)
              r_ima=sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
              r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)

              sys(i_mol)%F_pc(k_pp,:)=sys(i_mol)%F_pc(k_pp,:) + &
                   Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), r_ima, q_ima)
           enddo
        enddo
     endif
   
  enddo  ! on mol_j


  if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  !  periodic replica

     do i_rep=1,n_rep
        !           mol idx                   ! atomic idx
        q_rep=sys(idx_rep(i_rep,2))%q_0(idx_rep(i_rep,3))

        do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
           sys(i_mol)%F_pc(k_pp,:)=sys(i_mol)%F_pc(k_pp,:) + &
                Field_chg(sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                r_rep(i_rep,:),q_rep)
        enddo
     enddo
  endif

  
#ifdef MPI
     endif
#endif
  enddo  ! on mol_i

#ifdef MPI
  ! update F_pc for each proc
  do i_mol=1,sys_info%nmol_tot

     i_type=sys(i_mol)%moltype_id
     proc_id_source=modulo(i_mol,num_procs)
     call mpi_bcast(sys(i_mol)%F_pc(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)

  enddo
#endif


end subroutine calc_F_pc_v2


! GD 07/04/21  calc_V_pc_v2 superseeds calc_V_pc, calc_V_pc_rep
!subroutine calc_V_pc
!! calulate potential at crp/atoms due to charges  
!! GD 19/02/16: introduction of image charges
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type
!  real(rk)   :: q_ima,r_ima(3)
!
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%V_pc=0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   j_type=sys(j_mol)%moltype_id
!
!   if (j_mol.ne.i_mol)  then   
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            sys(i_mol)%V_pc(k_crp)=sys(i_mol)%V_pc(k_crp)+ &
!                  Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                  sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                  sys(j_mol)%q_0(l_crp) )
!         enddo
!      enddo
!   endif
!
!   ! image charges
!   if (is_mirr) then
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            q_ima=-sys(j_mol)%q_0(l_crp)
!            r_ima=sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
!            r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)
!
!            sys(i_mol)%V_pc(k_crp)=sys(i_mol)%V_pc(k_crp)+ &
!                 Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), r_ima, q_ima)
!         enddo
!      enddo
!   endif
!
!   enddo  ! on mol_j
!#ifdef MPI
!   endif
!#endif
!enddo  ! on mol_i
!
!#ifdef MPI
!! update V_pc for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%V_pc(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_V_pc
!
!
!
!subroutine calc_V_pc_rep
!! calulate potential at crp/atoms due to permanent charges  
!! including the contribution from periodic replica within a cutoff
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
!  real(rk)   :: q_rep
!  
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%V_pc=0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   if (j_mol.ne.i_mol)  then
!
!      j_type=sys(j_mol)%moltype_id
!
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            sys(i_mol)%V_pc(k_crp)=sys(i_mol)%V_pc(k_crp)+ &
!                 Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                 sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                 sys(j_mol)%q_0(l_crp) )
!         enddo
!      enddo
!
!   endif
!   enddo  ! on mol_j
!
!   ! Potential due to replicated charges
!   do i_rep=1,n_rep
!      !           mol idx                   ! atomic idx 
!      q_rep=sys(idx_rep(i_rep,2))%q_0(idx_rep(i_rep,3))
!
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol
!         sys(i_mol)%V_pc(k_crp)=sys(i_mol)%V_pc(k_crp) + & 
!              Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                 r_rep(i_rep,:),q_rep)
!         enddo
!
!         
!      enddo
!#ifdef MPI
!   endif
!#endif
!enddo  ! on mol_i
!
!#ifdef MPI
!! update V_pc for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%V_pc(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_V_pc_rep



! GD 01/10/20  routine to detect close intermolecular contacts
subroutine check_intermol_contacts
 
  implicit none
  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
  integer    :: i1,i2,i3
  real(rk)   :: q_rep,Rtmp(3)
  real(rk)   :: v1(3),v2(3),v3(3),trasl(3)
  character(50) :: ofmt="(5(i6,1x),f12.4)"
  logical       :: contact_found 
!!!!!!!!!!!!!!!!!!

contact_found=.false.
  
if (is_periodic_3D_Rc) then
      v1=cell2rep(1,:)
      v2=cell2rep(2,:)
      v3=cell2rep(3,:)
elseif  (is_periodic_2D_Rc) then
   if (u_tf.eq.3) then     ! ab plane film
      v1=cell2rep(1,:)
      v2=cell2rep(2,:)
      v3=cell2rep(3,:)
   elseif (u_tf.eq.2) then ! ac plane film
      v1=cell2rep(1,:)
      v2=cell2rep(3,:)
      v3=cell2rep(2,:)
   elseif (u_tf.eq.1) then ! bc plane film
      v1=cell2rep(2,:)
      v2=cell2rep(3,:)
      v3=cell2rep(1,:)
   endif
endif



write(*,*) ' List of close intermolecular contacts, if any. '
write(*,*) 
write(*,*) ' Format: molid1 molid2   AxRep1 AxRep2 AxRep3  distance_found'
write(*,*) ' Note that  distance_found is not necessarily the shortest contact!'
write(*,*)

open(file='contacts.dat',unit=77,status="replace") 
write(77,*) '! 1.molid1  2.molid2  3.AxRep1 4.AxRep2 5.AxRep3  6.distance_found'
write(77,*) '! Note that  distance_found is not necessarily the shortest contact!'


do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
   
#ifdef MPI
   if(modulo(i_mol,num_procs)==proc_id) then
#endif
      i_type=sys(i_mol)%moltype_id

      do j_mol=1,sys_info%nmol_tot
      if (j_mol.ne.i_mol)  then
            
         j_type=sys(j_mol)%moltype_id
            
         do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
            do l_crp=1,n_crp(j_type) ! loop on crp of j_mol

               Rtmp=sys(i_mol)%r_at(i_crp(k_crp,i_type),:)- &
                    sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
                  
               if (Norm3(Rtmp).le.Rcontact) then

                  write(*,ofmt) i_mol,j_mol,0,0,0,Norm3(Rtmp)
                  write(77,ofmt) i_mol,j_mol,0,0,0,Norm3(Rtmp)
                  contact_found=.true.
                  goto 10
               endif
                  
            enddo
         enddo
            
10       continue    

      endif
      enddo  ! on mol_j
      
      
      if  (is_periodic_2D_Rc) then
         do i1=-1,1
         do i2=-1,1              
         if ( .not.((i1.eq.0).and.(i2.eq.0)) ) then
             
            trasl=i1*v1+i2*v2
            
            do j_mol=1,sys_info%nmol_tot
            
               j_type=sys(j_mol)%moltype_id
            
               do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
                  do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                  
                     Rtmp=sys(i_mol)%r_at(i_crp(k_crp,i_type),:)- &
                          sys(j_mol)%r_at(i_crp(l_crp,j_type),:)+ trasl
                  
                     if (Norm3(Rtmp).le.Rcontact) then

                        if (u_tf.eq.3) then
                           contact_found=.true.
                           write(*,ofmt) i_mol,j_mol,i1,i2,0,Norm3(Rtmp)
                           write(77,ofmt) i_mol,j_mol,i1,i2,0,Norm3(Rtmp)
                        elseif (u_tf.eq.2) then
                           contact_found=.true.
                           write(*,ofmt) i_mol,j_mol,i1,0,i2,Norm3(Rtmp)
                           write(77,ofmt) i_mol,j_mol,i1,0,i2,Norm3(Rtmp)
                        elseif (u_tf.eq.1) then
                           contact_found=.true.
                           write(*,ofmt) i_mol,j_mol,0,i1,i2,Norm3(Rtmp)
                           write(77,ofmt) i_mol,j_mol,0,i1,i2,Norm3(Rtmp)
                        endif
                        
                        goto 20
                     endif
                     
                  enddo
               enddo
            
20             continue    
    
            enddo  ! on mol_j
            
         endif
         enddo
         enddo


      elseif (is_periodic_3D_Rc) then
         do i1=-1,1
         do i2=-1,1
         do i3=-1,1
         if (.not.((i1.eq.0).and.(i2.eq.0).and.(i3.eq.0))) then
            trasl=i1*v1+i2*v2+i3*v3
            
            do j_mol=1,sys_info%nmol_tot
            
               j_type=sys(j_mol)%moltype_id
            
               do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
                  do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                  
                     Rtmp=sys(i_mol)%r_at(i_crp(k_crp,i_type),:)- &
                          sys(j_mol)%r_at(i_crp(l_crp,j_type),:)+ trasl
                  
                     if (Norm3(Rtmp).le.Rcontact) then
                        contact_found=.true.
                        write(*,ofmt) i_mol,j_mol,i1,i2,i3,Norm3(Rtmp)
                        write(77,ofmt) i_mol,j_mol,i1,i2,i3,Norm3(Rtmp)
                        goto 30
                     endif
                     
                  enddo
               enddo
            
30             continue    
    
            enddo  ! on mol_j
            
         endif
         enddo
         enddo
         enddo
         
      endif
   

#ifdef MPI
   endif
#endif
enddo  ! on mol_i

close(77)

if (.not.contact_found )  then
   write(*,*) ' No intermolecular contacts shorter than than the threshold value were found.'
endif
   


write(*,*) 
write(*,*) ' Contacts list wrote to file: contacts.dat'
write(*,*) 
write(*,*) ' Check of intermolecular close contacts done!'
write(*,*) 


end subroutine check_intermol_contacts



subroutine calc_V_dq_v2
  ! Compute potentials at atoms due to induced charges
  ! GD 07/04/21  NEW VERSION superseding calc_V_dq, calc_V_dq_rep
  !  The new version features:
  !  - A single routine for open-boundary and periodic (replica) calculations
  implicit none
  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
  real(rk)   :: q_ima,r_ima(3),q_rep

  do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

#ifdef MPI
     if(modulo(i_mol,num_procs)==proc_id) then
#endif
        i_type=sys(i_mol)%moltype_id
        sys(i_mol)%V_dq=0.0_rk

        do j_mol=1,sys_info%nmol_tot
           j_type=sys(j_mol)%moltype_id

           if (j_mol.ne.i_mol)  then
              do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol
                 do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                    sys(i_mol)%V_dq(k_crp)=sys(i_mol)%V_dq(k_crp)+ &
                         Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
                         sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
                         sys(j_mol)%dq_i(l_crp) )
                 enddo
              enddo
           endif

           if (is_mirr) then    ! image charges
              do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol
                 do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                    q_ima=-sys(j_mol)%dq_i(l_crp)
                    r_ima=sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
                    r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)

                    sys(i_mol)%V_dq(k_crp)=sys(i_mol)%V_dq(k_crp)+ &
                         Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), r_ima, q_ima)
                 enddo
              enddo
           endif


        enddo  ! on mol_j


        if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  ! periodic replica

           do i_rep=1,n_rep
              !           mol idx                   ! atomic idx
              q_rep=sys(idx_rep(i_rep,2))%dq_i(idx_rep(i_rep,3))

              do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol
                 sys(i_mol)%V_dq(k_crp)=sys(i_mol)%V_dq(k_crp) + &
                      Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
                      r_rep(i_rep,:),q_rep)
              enddo
           enddo
        endif

        
#ifdef MPI
     endif
#endif
  enddo  ! on mol_i

#ifdef MPI
  ! update V_dq for each proc
  do i_mol=1,sys_info%nmol_tot

     i_type=sys(i_mol)%moltype_id
     proc_id_source=modulo(i_mol,num_procs)
     call mpi_bcast(sys(i_mol)%V_dq(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)

  enddo
#endif


end subroutine calc_V_dq_v2



subroutine calc_F_dq_v2
  ! Compute fields at atoms due to induced charges
  ! GD 07/04/21  NEW VERSION superseding calc_F_dq, calc_F_dq_rep
  !  The new version features:
  !  - A single routine for open-boundary and periodic (replica) calculations
  implicit none
  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
  real(rk)   :: q_ima,r_ima(3),q_rep

  do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

#ifdef MPI
     if(modulo(i_mol,num_procs)==proc_id) then
#endif
        i_type=sys(i_mol)%moltype_id
        sys(i_mol)%F_dq=0.0_rk

        do j_mol=1,sys_info%nmol_tot
           j_type=sys(j_mol)%moltype_id

           if (j_mol.ne.i_mol)  then

              do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
                 do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                    sys(i_mol)%F_dq(k_pp,:)=sys(i_mol)%F_dq(k_pp,:) + &
                         Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                         sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
                         sys(j_mol)%dq_i(l_crp) )
                 enddo
              enddo
           endif

           if (is_mirr) then    ! image charges
              do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
                 do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
                    q_ima=-sys(j_mol)%dq_i(l_crp)
                    r_ima=sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
                    r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)

                    sys(i_mol)%F_dq(k_pp,:)=sys(i_mol)%F_dq(k_pp,:) + &
                         Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), r_ima, q_ima)
                 enddo
              enddo
           endif

        enddo  ! on mol_j


        if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  !  periodic replica

           do i_rep=1,n_rep
              !           mol idx                   ! atomic idx
              q_rep=sys(idx_rep(i_rep,2))%dq_i(idx_rep(i_rep,3))

              do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
                 sys(i_mol)%F_dq(k_pp,:)=sys(i_mol)%F_dq(k_pp,:) + &
                      Field_chg(sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                      r_rep(i_rep,:),q_rep)
              enddo
           enddo
        endif

        
#ifdef MPI
     endif
#endif
  enddo  ! on mol_i

#ifdef MPI
  ! update F_dq for each proc
  do i_mol=1,sys_info%nmol_tot

     i_type=sys(i_mol)%moltype_id
     proc_id_source=modulo(i_mol,num_procs)
     call mpi_bcast(sys(i_mol)%F_dq(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)

  enddo
#endif


end subroutine calc_F_dq_v2


! GD 07/04/21  calc_V_dq_v2 superseeds calc_V_dq, calc_V_dq_rep
!subroutine calc_V_dq
!! calulate potential at atoms due to induced charges  
!! GD 19/02/16: introduction of image charges
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type
!  real(rk)   :: q_ima,r_ima(3)
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%V_dq=0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   j_type=sys(j_mol)%moltype_id
!   
!   if (j_mol.ne.i_mol)  then
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            sys(i_mol)%V_dq(k_crp)=sys(i_mol)%V_dq(k_crp)+ &
!                 Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                 sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                 sys(j_mol)%dq_i(l_crp) )
!         enddo
!      enddo
!   endif
!
!   ! image charges
!   if (is_mirr) then
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            q_ima=-sys(j_mol)%dq_i(l_crp)
!            r_ima=sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
!            r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)
!
!            sys(i_mol)%V_dq(k_crp)=sys(i_mol)%V_dq(k_crp)+ &
!                 Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), r_ima, q_ima)
!         enddo
!      enddo
!   endif
!
!
!   enddo  ! on mol_j
!#ifdef MPI
!   endif
!#endif   
!enddo  ! on mol_i
!
!#ifdef MPI
!! update V_dq for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%V_dq(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_V_dq
!
!
!subroutine calc_V_dq_rep
!! calulate potential at atoms due to induced charges  
!! including the contribution from periodic replica within a cutoff
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
!  real(rk)   :: q_rep
!  
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%V_dq=0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   if (j_mol.ne.i_mol)  then
!
!      j_type=sys(j_mol)%moltype_id
!
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            sys(i_mol)%V_dq(k_crp)=sys(i_mol)%V_dq(k_crp)+ &
!                 Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                 sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                 sys(j_mol)%dq_i(l_crp) )
!         enddo
!      enddo
!
!   endif
!   enddo  ! on mol_j
!
!   ! Potential due to replicated charges
!   do i_rep=1,n_rep
!      !           mol idx                   ! atomic idx 
!      q_rep=sys(idx_rep(i_rep,2))%dq_i(idx_rep(i_rep,3))
!
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol
!         sys(i_mol)%V_dq(k_crp)=sys(i_mol)%V_dq(k_crp) + & 
!              Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                 r_rep(i_rep,:),q_rep)
!         enddo
!
!         
!      enddo
!#ifdef MPI
!   endif
!#endif 
!enddo  ! on mol_i
!
!#ifdef MPI
!! update V_dq for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%V_dq(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!end subroutine calc_V_dq_rep


! GD 07/04/21  not used  --> 2clear
!subroutine calc_V_chg_kr
!! calulate potential at crp/atoms due to charges with screening 
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type
!  real(rk)   :: krm1,ssrr
! 
!! K(r)=Ks-(Ks-1)*(1+S*r+.5*S^2*r^2)*exp(-S*r)  
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%V_pc=0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   if (j_mol.ne.i_mol)  then
!
!      j_type=sys(j_mol)%moltype_id
!
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!
!            ssrr=s_kr*norm3(sys(i_mol)%r_at(i_crp(k_crp,i_type),:)-sys(j_mol)%r_at(i_crp(l_crp,j_type),:))
!            krm1= ks_kr - (ks_kr-1.0_rk)*(1.0_rk + ssrr + 0.50_rk*ssrr**2)*exp(-ssrr)
!            krm1=(krm1)**(-1)
!
!            sys(i_mol)%V_pc(k_crp)=sys(i_mol)%V_pc(k_crp)+ &
!                 krm1*Pot_chg( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                 sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                 sys(j_mol)%q_i(l_crp) )
!         enddo
!      enddo
!
!   endif
!   enddo  ! on mol_j
!
!enddo  ! on mol_i
!
!end subroutine calc_V_chg_kr



subroutine calc_V_dip_v2
  ! Compute potential at atoms due to induced dipoles
  ! GD 07/04/21  NEW VERSION superseding calc_F_dip, calc_F_dip_rep
  !  The new version features:
  !  - A single routine for open-boundary and periodic (replica) calculations
  !  - Screening of both dipole fields and potential
  !  - Simplified treatment of screened short-range dipole fields
  implicit none
  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
  real(rk)   :: r_ij,a_i,a_j,au3,exp_mn,poly,f_screen,ax3
  real(rk)   :: mu_ima(3),r_ima(3),mu_src(3),mu_rep(3)

ax3=3.0_rk * a_screen
  
  do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

#ifdef MPI
     if(modulo(i_mol,num_procs)==proc_id) then
#endif
        i_type=sys(i_mol)%moltype_id
        sys(i_mol)%V_id=0.0_rk

        ! INTRAMOLECULAR SCREENED DIPOLE FIELD
        if (screen_intra_dipdip) then
           do k_pp=1,n_pp(i_type)  ! loop on crp of i_mol
           do l_pp=1,n_pp(i_type)
           if (l_pp.ne.k_pp) then
              
              ! screening factors calculation
              r_ij=Norm3(sys(i_mol)%r_at(i_pp(k_pp,i_type),:)- &
                   sys(i_mol)%r_at(i_pp(l_pp,i_type),:) )

              f_screen= 1.0_rk
              if (r_ij.lt.ax3)  f_screen= 1.0_rk - exp( -(r_ij/a_screen )**3 )

              mu_src=f_screen * sys(i_mol)%mu_i(l_pp,:)

              sys(i_mol)%V_id(k_pp) = sys(i_mol)%V_id(k_pp) + &
                   Pot_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                   sys(i_mol)%r_at(i_pp(l_pp,i_type),:), &
                   mu_src )

           endif
           enddo
           enddo
     endif


     ! INTERMOLECULAR DIPOLE FIELDS
     do j_mol=1,sys_info%nmol_tot
        j_type=sys(j_mol)%moltype_id

        if (j_mol.ne.i_mol)  then
           do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
           do l_pp=1,n_pp(j_type)  ! loop on pp of j_mol

              f_screen=1.0_rk
              if (screen_inter_dipdip) then
                 r_ij=Norm3(sys(i_mol)%r_at(i_pp(k_pp,i_type),:) - &
                      sys(j_mol)%r_at(i_pp(l_pp,j_type),:) )

                 if (r_ij.lt.ax3)  f_screen= 1.0_rk - exp( -(r_ij/a_screen )**3 )
              endif
              mu_src=f_screen *sys(j_mol)%mu_i(l_pp,:)

              sys(i_mol)%V_id(k_pp)=sys(i_mol)%V_id(k_pp) + &
                   Pot_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                   sys(j_mol)%r_at(i_pp(l_pp,j_type),:), &
                   mu_src )
           enddo
           enddo
        endif

     enddo   ! on mol_j

     
     if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  ! periodic replica

        do i_rep=1,n_rep
           !                   mol idx                  atom idx
           mu_rep=  sys(idx_rep(i_rep,2))%mu_i(idx_rep(i_rep,3),:)

           do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
              
              f_screen=1.0_rk
              if (screen_inter_dipdip) then
                 r_ij=Norm3( sys(i_mol)%r_at(i_pp(k_pp,i_type),:) - r_rep(i_rep,:) )

                 if (r_ij.lt.ax3)  f_screen= 1.0_rk - exp( -(r_ij/a_screen )**3 )
              endif
              mu_src= f_screen * mu_rep

              sys(i_mol)%V_id(k_pp)=sys(i_mol)%V_id(k_pp) + &
                   Pot_dip(sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                   r_rep(i_rep,:), mu_src)

           enddo
        enddo


     elseif (is_mirr) then ! image dipoles

        do j_mol=1,sys_info%nmol_tot
           j_type=sys(j_mol)%moltype_id

           do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
           do l_pp=1,n_pp(j_type)  ! loop on pp of j_mol
              mu_ima=sys(j_mol)%mu_i(l_pp,:)
              mu_ima(i_mirr)=-mu_ima(i_mirr)
              r_ima=sys(j_mol)%r_at(i_pp(l_pp,j_type),:)
              r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)

              sys(i_mol)%V_id(k_pp)=sys(i_mol)%V_id(k_pp) + &
                   Pot_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), r_ima, mu_ima)
           enddo
           enddo

        enddo

     endif



#ifdef MPI
  endif
#endif

enddo  ! on mol_i

#ifdef MPI
! update V_id for each proc
do i_mol=1,sys_info%nmol_tot

   i_type=sys(i_mol)%moltype_id
   proc_id_source=modulo(i_mol,num_procs)
   call mpi_bcast(sys(i_mol)%V_id(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)

enddo
#endif

end subroutine calc_V_dip_v2





! GD 07/04/21  calc_V_dip_v2 superseeds calc_V_dip, calc_V_dip_rep
!subroutine calc_V_dip
!! calulate potential at atoms due to induced dipoles
!! GD 19/02/16: introduction of image dipoles
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type
!  real(rk)   :: mu_ima(3),r_ima(3)
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%V_id=0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   j_type=sys(j_mol)%moltype_id
!
!   if (j_mol.ne.i_mol)  then
!
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_pp=1,n_pp(j_type)
!            sys(i_mol)%V_id(k_crp)=sys(i_mol)%V_id(k_crp)+ &
!                 Pot_dip( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                          sys(j_mol)%r_at(i_pp(l_pp,j_type),:),   &
!                          sys(j_mol)%mu_i(l_pp,:)     )
!         enddo
!      enddo
!   endif
!
!   ! image dipoles
!   if (is_mirr) then
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_pp=1,n_pp(j_type)
!            mu_ima=sys(j_mol)%mu_i(l_pp,:)
!            mu_ima(i_mirr)=-mu_ima(i_mirr)
!            r_ima=sys(j_mol)%r_at(i_pp(l_pp,j_type),:)
!            r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)
!
!            sys(i_mol)%V_id(k_crp)=sys(i_mol)%V_id(k_crp)+ &
!                 Pot_dip( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), r_ima, mu_ima)
!         enddo
!      enddo
!   endif
!
!
!   enddo  ! on mol_j
!#ifdef MPI
!   endif
!#endif   
!enddo  ! on mol_i
!
!#ifdef MPI
!! update V_id for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%V_id(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_V_dip
!
!
!subroutine calc_V_dip_rep
!! calulate potential at atoms due to induced dipoles
!! including the contribution from periodic replica within a cutoff
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
!  real(rk)   :: mu_rep(3)
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%V_id=0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   if (j_mol.ne.i_mol)  then
!
!      j_type=sys(j_mol)%moltype_id
!
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_pp=1,n_pp(j_type)
!            sys(i_mol)%V_id(k_crp)=sys(i_mol)%V_id(k_crp)+ &
!                 Pot_dip( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                          sys(j_mol)%r_at(i_pp(l_pp,j_type),:),   &
!                          sys(j_mol)%mu_i(l_pp,:)     )
!         enddo
!      enddo
!
!   endif
!   enddo  ! on mol_j
!
!   ! Potential due to replicated dipoles
!   do i_rep=1,n_rep
!       !           mol idx                   ! atomic idx 
!      mu_rep=sys(idx_rep(i_rep,2))%mu_i(idx_rep(i_rep,3),:) 
!
!      do  k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         sys(i_mol)%V_id(k_crp)=sys(i_mol)%V_id(k_crp)+ &
!              Pot_dip( sys(i_mol)%r_at(i_crp(k_crp,i_type),:), &
!                       r_rep(i_rep,:),mu_rep)
!      enddo
!   enddo
!
!#ifdef MPI
!   endif
!#endif   
!enddo  ! on mol_i
!
!#ifdef MPI
!! update V_id for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%V_id(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_V_dip_rep



! GD 07/04/21  calc_F_pc_v2 superseeds calc_F_pc, calc_F_pc_rep
!subroutine calc_F_pc
!! calulate field at atoms due to permanent charges
!! GD 19/02/16: introduction of image charges
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type
!  real(rk)   :: q_ima,r_ima(3)
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%F_pc=0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   j_type=sys(j_mol)%moltype_id
!
!   if (j_mol.ne.i_mol)  then
!
!      do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            sys(i_mol)%F_pc(k_pp,:)=sys(i_mol)%F_pc(k_pp,:) + &
!                 Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                            sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                            sys(j_mol)%q_0(l_crp) )
!         enddo
!      enddo
!   endif
!
!   ! image charges
!   if (is_mirr) then 
!      do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            q_ima=-sys(j_mol)%q_0(l_crp)
!            r_ima=sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
!            r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)
!
!            sys(i_mol)%F_pc(k_pp,:)=sys(i_mol)%F_pc(k_pp,:) + &
!                 Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), r_ima, q_ima)
!         enddo
!      enddo
!   endif
!
!
!   enddo  ! on mol_j
!#ifdef MPI
!   endif
!#endif
!enddo  ! on mol_i
!
!#ifdef MPI
!! update F_pc for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%F_pc(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_F_pc
!
!
!subroutine calc_F_pc_rep
!! calulate field at atoms due to permanent charges
!! including the contribution from periodic replica within a cutoff
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
!  real(rk)   :: q_rep
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%F_pc =0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   if (j_mol.ne.i_mol)  then
!
!      j_type=sys(j_mol)%moltype_id
!
!      do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            sys(i_mol)%F_pc(k_pp,:)=sys(i_mol)%F_pc(k_pp,:) + &
!                 Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                            sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                            sys(j_mol)%q_0(l_crp) )
!         enddo
!      enddo
!
!   endif
!   enddo  ! on mol_j
!
!   ! Field due to replicated charges
!   do i_rep=1,n_rep
!      !           mol idx                   ! atomic idx 
!      q_rep=sys(idx_rep(i_rep,2))%q_0(idx_rep(i_rep,3))
!      
!      do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol  
!         sys(i_mol)%F_pc(k_pp,:)=sys(i_mol)%F_pc(k_pp,:) + &
!              Field_chg(sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!              r_rep(i_rep,:),q_rep)
!      enddo
!   enddo
!#ifdef MPI
!   endif
!#endif
!enddo  ! on mol_i
!
!#ifdef MPI
!! update F_pc_rep for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%F_pc(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_F_pc_rep





!! GD 07/04/21  calc_F_dq_v2 superseeds calc_F_dq, calc_F_dq_rep
!subroutine calc_F_dq
!! calulate field at atoms due to induced charges
!! GD 19/02/16: introduction of image charges
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type
!  real(rk)   :: q_ima,r_ima(3)
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%F_dq=0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   j_type=sys(j_mol)%moltype_id
!
!   if (j_mol.ne.i_mol)  then
!
!      do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            sys(i_mol)%F_dq(k_pp,:)=sys(i_mol)%F_dq(k_pp,:) + &
!                 Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                            sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                            sys(j_mol)%dq_i(l_crp) )
!         enddo
!      enddo
!   endif
!
!   ! image charges
!   if (is_mirr) then
!      do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            q_ima=-sys(j_mol)%dq_i(l_crp)
!            r_ima=sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
!            r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)
!
!            sys(i_mol)%F_dq(k_pp,:)=sys(i_mol)%F_dq(k_pp,:) + &
!                 Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), r_ima, q_ima)
!         enddo
!      enddo
!   endif
!
!   enddo  ! on mol_j
!#ifdef MPI
!   endif
!#endif   
!enddo  ! on mol_i
!
!! image charges
!if (is_mirr) then
!   do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol
!      do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!         q_ima=-sys(j_mol)%dq_i(l_crp)
!         r_ima=sys(j_mol)%r_at(i_crp(l_crp,j_type),:)
!         r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)
!
!         sys(i_mol)%F_dq(k_pp,:)=sys(i_mol)%F_dq(k_pp,:) + &
!              Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), r_ima, q_ima)
!      enddo
!   enddo
!endif
!
!enddo  ! on mol_j
!#ifdef MPI
!endif
!#endif
!enddo  ! on mol_i
!
!#ifdef MPI
!! update F_dq for each proc
!do i_mol=1,sys_info%nmol_tot
!
!   i_type=sys(i_mol)%moltype_id
!   proc_id_source=modulo(i_mol,num_procs)
!   call mpi_bcast(sys(i_mol)%F_dq(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!#ifdef MPI
!! update F_dq for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%F_dq(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_F_dq
!
!
!subroutine calc_F_dq_rep
!! calulate field at atoms due to permanent charges
!! including the contribution from periodic replica within a cutoff
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
!  real(rk)   :: q_rep
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%F_dq =0.0_rk
!
!   do j_mol=1,sys_info%nmol_tot
!   if (j_mol.ne.i_mol)  then
!
!      j_type=sys(j_mol)%moltype_id
!
!      do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!            sys(i_mol)%F_dq(k_pp,:)=sys(i_mol)%F_dq(k_pp,:) + &
!                 Field_chg( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                            sys(j_mol)%r_at(i_crp(l_crp,j_type),:), &
!                            sys(j_mol)%dq_i(l_crp) )
!         enddo
!      enddo
!
!   endif
!   enddo  ! on mol_j
!
!     ! Field due to replicated charges
!      do i_rep=1,n_rep
!         !           mol idx                   ! atomic idx 
!         q_rep=sys(idx_rep(i_rep,2))%dq_i(idx_rep(i_rep,3))
!
!         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol  
!            sys(i_mol)%F_dq(k_pp,:)=sys(i_mol)%F_dq(k_pp,:) + &
!                  Field_chg(sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                            r_rep(i_rep,:),q_rep)
!         enddo
!      enddo
!
!#ifdef MPI
!   endif
!#endif   
!enddo  ! on mol_i
!
!#ifdef MPI
!! update F_dq for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%F_dq(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_F_dq_rep



subroutine calc_F_dip_v2
! Compute fields at atoms due to induced dipoles
! GD 07/04/21  NEW VERSION superseding calc_F_dip, calc_F_dip_rep
!  The new version features:
!  - A single routine for open-boundary and periodic (replica) calculations
!  - Screening of both dipole fields and potential
!  - Simplified treatment of screened short-range dipole fields
  implicit none
  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
  real(rk)   :: r_ij,a_i,a_j,au3,exp_mn,poly,f_screen,ax3
  real(rk)   :: mu_ima(3),r_ima(3),mu_src(3),mu_rep(3)

ax3=3.0_rk * a_screen
  
do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

#ifdef MPI
  if(modulo(i_mol,num_procs)==proc_id) then
#endif
   i_type=sys(i_mol)%moltype_id
   sys(i_mol)%F_id=0.0_rk

   ! INTRAMOLECULAR SCREENED DIPOLE FIELD 
   if (screen_intra_dipdip) then
      do k_pp=1,n_pp(i_type)  ! loop on crp of i_mol 
      do l_pp=1,n_pp(i_type)
         if (l_pp.ne.k_pp) then
            
            ! screening factors calculation
            r_ij=Norm3(sys(i_mol)%r_at(i_pp(k_pp,i_type),:)- &
                 sys(i_mol)%r_at(i_pp(l_pp,i_type),:) )

            f_screen= 1.0_rk
            if (r_ij.lt.ax3)  f_screen= 1.0_rk - exp( -(r_ij/a_screen )**3 )
            
            mu_src=f_screen * sys(i_mol)%mu_i(l_pp,:)  
         
            sys(i_mol)%F_id(k_pp,:) = sys(i_mol)%F_id(k_pp,:) + &
                 Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                 sys(i_mol)%r_at(i_pp(l_pp,i_type),:), &
                 mu_src )
         
         endif
      enddo
      enddo
   endif


   ! INTERMOLECULAR DIPOLE FIELDS
   do j_mol=1,sys_info%nmol_tot
      j_type=sys(j_mol)%moltype_id

      if (j_mol.ne.i_mol)  then
         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
         do l_pp=1,n_pp(j_type)  ! loop on pp of j_mol 
            
            
            f_screen=1.0_rk
            if (screen_inter_dipdip) then
               r_ij=Norm3(sys(i_mol)%r_at(i_pp(k_pp,i_type),:)- &
                    sys(j_mol)%r_at(i_pp(l_pp,j_type),:) )

               if (r_ij.lt.ax3)  f_screen= 1.0_rk - exp( -(r_ij/a_screen )**3 )
            endif           
            mu_src=f_screen *sys(j_mol)%mu_i(l_pp,:)
               
            sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
                 Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                            sys(j_mol)%r_at(i_pp(l_pp,j_type),:), &
                            mu_src )
         enddo
         enddo
      endif

   enddo   ! on mol_j

   
   if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  !  periodic replica
   
      do i_rep=1,n_rep
  !                      mol idx                  atom idx 
            mu_rep=  sys(idx_rep(i_rep,2))%mu_i(idx_rep(i_rep,3),:)
         
         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol

            f_screen=1.0_rk
            if (screen_inter_dipdip) then

               r_ij=Norm3( sys(i_mol)%r_at(i_pp(k_pp,i_type),:) - r_rep(i_rep,:) )

               if (r_ij.lt.ax3)  f_screen= 1.0_rk - exp( -(r_ij/a_screen )**3 )
               
            endif
            mu_src= f_screen * mu_rep
   
            sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
                 Field_dip(sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
                 r_rep(i_rep,:),mu_src)
            
         enddo
      enddo

  
   elseif (is_mirr) then ! image dipoles

      do j_mol=1,sys_info%nmol_tot
         j_type=sys(j_mol)%moltype_id

         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
         do l_pp=1,n_pp(j_type)  ! loop on pp of j_mol 
            mu_ima=sys(j_mol)%mu_i(l_pp,:)
            mu_ima(i_mirr)=-mu_ima(i_mirr)
            r_ima=sys(j_mol)%r_at(i_pp(l_pp,j_type),:)
            r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)              

            sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
                 Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), r_ima, mu_ima)
         enddo
         enddo

      enddo

   endif
  
   

#ifdef MPI
   endif
#endif   

enddo  ! on mol_i

#ifdef MPI
! update F_id for each proc
do i_mol=1,sys_info%nmol_tot

  i_type=sys(i_mol)%moltype_id
  proc_id_source=modulo(i_mol,num_procs)
  call mpi_bcast(sys(i_mol)%F_id(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)

enddo
#endif

end subroutine calc_F_dip_v2



! GD 07/04/21  calc_F_dip_v2 superseeds calc_F_dip, calc_F_dip_rep
!subroutine calc_F_dip
!! calulate field at atoms due to induced dipoles
!! INTRA and INTERmolecular screening of the dipole field.
!! GD 19/02/16: introduction of image dipoles
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type
!  real(rk)   :: r_ij,a_i,a_j,au3,exp_mn,poly,fe_screen,ft_screen,nu3
!  real(rk)   :: mu_ima(3),r_ima(3)
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%F_id=0.0_rk
!
!   ! INTRAMOLECULAR REN-PONDER SCREENED DIPOLE FIELD (ONLY IF REQUIRED)
!   if (screen_intra_dipdip) then
!      do k_pp=1,n_pp(i_type)  ! loop on crp of i_mol 
!      do l_pp=1,n_pp(i_type)
!      if (l_pp.ne.k_pp) then
!
!         ! screening factors calculation
!         r_ij=Norm3(sys(i_mol)%r_at(i_pp(k_pp,i_type),:)- &
!                    sys(i_mol)%r_at(i_pp(l_pp,i_type),:) )
!         
!         au3=a_screen*(r_ij*sys(i_mol)%screen_k(k_pp)*sys(i_mol)%screen_k(l_pp) )**3
!         !write(*,*) 'bau ',au3,a_screen
!
!         if (au3.gt.15.0_rk) then  ! here screening is irrelevant: f_t=f_e~1
!            sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                       Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                                  sys(i_mol)%r_at(i_pp(l_pp,i_type),:), &
!                                  sys(i_mol)%mu_i(l_pp,:)  )
!         else  ! apply screening
!            exp_mn=exp(-au3)
!            fe_screen= 1.0_rk - exp_mn
!            ft_screen= 1.0_rk - (1.0_rk + au3)*exp_mn
!        
!            sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                 Field_dip_screen( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                                   sys(i_mol)%r_at(i_pp(l_pp,i_type),:), &
!                                   sys(i_mol)%mu_i(l_pp,:),fe_screen,ft_screen )
!         end if
!
!      endif
!      enddo
!      enddo
!   endif
!
!   !!! INTERMOLECULAR DIPOLE FIELDS
!   if (screen_inter_dipdip) then   ! SCREENED INTERMOLECULAR DIPOLE FIELDS
!
!      do j_mol=1,sys_info%nmol_tot
!         if (j_mol.ne.i_mol)  then
!
!         j_type=sys(j_mol)%moltype_id
!
!         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!            do l_pp=1,n_pp(j_type) ! loop on pp of j_mol 
!
!               ! screening factors calculation
!               r_ij=Norm3(sys(i_mol)%r_at(i_pp(k_pp,i_type),:)- &
!                          sys(j_mol)%r_at(i_pp(l_pp,j_type),:) )
!               au3=a_screen*(r_ij*sys(i_mol)%screen_k(k_pp)*sys(j_mol)%screen_k(l_pp) )**3
!
!
!               if (au3.gt.15.0_rk) then  ! here screening is irrelevant: f_t=f_e~1
!
!                  sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                       Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                                  sys(j_mol)%r_at(i_pp(l_pp,j_type),:), &
!                                  sys(j_mol)%mu_i(l_pp,:)  )
!
!               else  ! apply screening
!                  exp_mn=exp(-au3)
!                  fe_screen= 1.0_rk - exp_mn
!                  ft_screen= 1.0_rk - (1.0_rk + au3)*exp_mn
!
!                  sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                      Field_dip_screen( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                                        sys(j_mol)%r_at(i_pp(l_pp,j_type),:), &
!                                        sys(j_mol)%mu_i(l_pp,:),fe_screen,ft_screen )
!               end if
!
!            enddo
!         enddo
!
!      endif
!      enddo  ! on mol_j
!
!   else ! UNSCREENED INTERMOLECULAR DIPOLE FIELDS
!
!      do j_mol=1,sys_info%nmol_tot
!      j_type=sys(j_mol)%moltype_id
!
!      if (j_mol.ne.i_mol)  then
!         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!            do l_pp=1,n_pp(j_type) ! loop on pp of j_mol 
!
!               sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                    Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                               sys(j_mol)%r_at(i_pp(l_pp,j_type),:), &
!                               sys(j_mol)%mu_i(l_pp,:)  )
!            enddo
!         enddo
!      endif
!
!      ! image dipoles
!      if (is_mirr) then
!         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!            do l_pp=1,n_pp(j_type) ! loop on pp of j_mol 
!               mu_ima=sys(j_mol)%mu_i(l_pp,:)
!               mu_ima(i_mirr)=-mu_ima(i_mirr)
!               r_ima=sys(j_mol)%r_at(i_pp(l_pp,j_type),:)
!               r_ima(i_mirr)=r_ima(i_mirr) - 2.0_rk*(r_ima(i_mirr)-z_mirr)              
!
!               sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                    Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), r_ima, mu_ima)
!            enddo
!         enddo
!
!      endif
!
!
!      enddo  ! on mol_j
!
!   endif ! inter screened/unscreened
!#ifdef MPI
!   endif
!#endif   
!
!enddo  ! on mol_i
!
!#ifdef MPI
!! update F_id for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%F_id(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!
!end subroutine calc_F_dip
!
!
!
!subroutine calc_F_dip_rep
!! calulate field at atoms due to induced dipoles
!! INTRA and INTERmolecular screening of the dipole field.
!! including the contribution from periodic replica within a cutoff
!  implicit none
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type,i_rep
!  real(rk)   :: r_ij,a_i,a_j,au3,exp_mn,poly,fe_screen,ft_screen,nu3,mu_rep(3)
!
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!
!#ifdef MPI
!  if(modulo(i_mol,num_procs)==proc_id) then
!#endif
!   i_type=sys(i_mol)%moltype_id
!   sys(i_mol)%F_id=0.0_rk
!
!   ! INTRAMOLECULAR REN-PONDER SCREENED DIPOLE FIELD (ONLY IF REQUIRED)
!   if (screen_intra_dipdip) then
!      do k_pp=1,n_pp(i_type)  ! loop on crp of i_mol 
!      do l_pp=1,n_pp(i_type)
!      if (l_pp.ne.k_pp) then
!
!         ! screening factors calculation
!         r_ij=Norm3(sys(i_mol)%r_at(i_pp(k_pp,i_type),:)- &
!                    sys(i_mol)%r_at(i_pp(l_pp,i_type),:) )
!         
!         au3=a_screen*(r_ij*sys(i_mol)%screen_k(k_pp)*sys(i_mol)%screen_k(l_pp) )**3
!         !write(*,*) 'bau ',au3,a_screen
!
!         if (au3.gt.15.0_rk) then  ! here screening is irrelevant: f_t=f_e~1
!            sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                       Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                                  sys(i_mol)%r_at(i_pp(l_pp,i_type),:), &
!                                  sys(i_mol)%mu_i(l_pp,:)  )
!         else  ! apply screening
!            exp_mn=exp(-au3)
!            fe_screen= 1.0_rk - exp_mn
!            ft_screen= 1.0_rk - (1.0_rk + au3)*exp_mn
!        
!            sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                 Field_dip_screen( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                                   sys(i_mol)%r_at(i_pp(l_pp,i_type),:), &
!                                   sys(i_mol)%mu_i(l_pp,:),fe_screen,ft_screen )
!         end if
!
!      endif
!      enddo
!      enddo
!   endif
!
!   !!! INTERMOLECULAR DIPOLE FIELDS
!   if (screen_inter_dipdip) then   ! SCREENED INTERMOLECULAR DIPOLE FIELDS
!
!      do j_mol=1,sys_info%nmol_tot
!      if (j_mol.ne.i_mol)  then
!
!         j_type=sys(j_mol)%moltype_id
!
!         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!            do l_pp=1,n_pp(j_type) ! loop on pp of j_mol 
!
!               ! screening factors calculation
!               r_ij=Norm3(sys(i_mol)%r_at(i_pp(k_pp,i_type),:) - &
!                          sys(j_mol)%r_at(i_pp(l_pp,j_type),:) )
!               
!               au3=a_screen*(r_ij*sys(i_mol)%screen_k(k_pp)*sys(j_mol)%screen_k(l_pp) )**3
!
!               if (au3.gt.15.0_rk) then  ! here screening is irrelevant: f_t=f_e=~1
!
!                  sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                       Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                                  sys(j_mol)%r_at(i_pp(l_pp,j_type),:), &
!                                  sys(j_mol)%mu_i(l_pp,:)  )
!
!               else  ! apply screening
!
!                  exp_mn=exp(-au3)
!                  fe_screen= 1.0_rk - exp_mn
!                  ft_screen= 1.0_rk - (1.0_rk + au3)*exp_mn
!
!                  sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                      Field_dip_screen( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                                        sys(j_mol)%r_at(i_pp(l_pp,j_type),:), &
!                                        sys(j_mol)%mu_i(l_pp,:),fe_screen,ft_screen )
!               end if
!
!            enddo
!         enddo
!
!      endif
!      enddo  ! on mol_j
!
!      ! Field due to replicated dipoles
!      ! GD 01/03/2021 adding screened dipolar interactions with replica 
!
!      do i_rep=1,n_rep
!
!         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!         
!            ! screening factors calculation
!            r_ij=Norm3( sys(i_mol)%r_at(i_pp(k_pp,i_type),:) - r_rep(i_rep,:) )      
!         
!            au3=a_screen*( r_ij * sys(i_mol)%screen_k(k_pp) * &
!                   sys( idx_rep(i_rep,2) )%screen_k( idx_rep(i_rep,3) ) )**3
!
!            mu_rep= sys(idx_rep(i_rep,2))%mu_i(idx_rep(i_rep,3),:) 
!
!            if (au3.gt.15.0_rk) then  ! here screening is irrelevant: f_t=f_e=~1
!
!               sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                    Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                    r_rep(i_rep,:), mu_rep )
!
!                    else  ! apply screening
!
!               exp_mn=exp(-au3)
!               fe_screen= 1.0_rk - exp_mn
!               ft_screen= 1.0_rk - (1.0_rk + au3)*exp_mn
!
!               sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                    Field_dip_screen( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                    r_rep(i_rep,:), mu_rep, fe_screen, ft_screen )
!                    
!            endif
!
!         enddo
!
!      enddo
!
!      
!   else ! UNSCREENED INTERMOLECULAR DIPOLE FIELDS
!
!      do j_mol=1,sys_info%nmol_tot
!      if (j_mol.ne.i_mol)  then
!
!         j_type=sys(j_mol)%moltype_id
!
!         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!            do l_pp=1,n_pp(j_type) ! loop on pp of j_mol 
!
!               sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                    Field_dip( sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                               sys(j_mol)%r_at(i_pp(l_pp,j_type),:), &
!                               sys(j_mol)%mu_i(l_pp,:)  )
!            enddo
!         enddo
!
!      endif
!      enddo  ! on mol_j
!
!
!      ! Field due to replicated dipoles
!
!      do i_rep=1,n_rep
!         !           mol idx                   ! atomic idx 
!         mu_rep= sys(idx_rep(i_rep,2))%mu_i(idx_rep(i_rep,3),:) 
!
!         do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol  
!            sys(i_mol)%F_id(k_pp,:)=sys(i_mol)%F_id(k_pp,:) + &
!                    Field_dip(sys(i_mol)%r_at(i_pp(k_pp,i_type),:), &
!                              r_rep(i_rep,:),mu_rep)
!         enddo
!      enddo
!
!   endif ! inter screened/unscreened
!
!#ifdef MPI
!   endif
!#endif   
!
!enddo  ! on mol_i
!
!#ifdef MPI
!! update F_id for each proc
!do i_mol=1,sys_info%nmol_tot
!
!  i_type=sys(i_mol)%moltype_id
!  proc_id_source=modulo(i_mol,num_procs)
!  call mpi_bcast(sys(i_mol)%F_id(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,proc_id_source,MPI_COMM_WORLD,ierr)
!
!enddo
!#endif
!
!end subroutine calc_F_dip_rep


function Pot_chg(r,r_q,q)
  implicit none
  real(rk)   :: Pot_chg,r(3),r_q(3),q

  Pot_chg=q/Norm3(r-r_q)
end function Pot_chg


function Pot_dip(r,r_mu,mu)
  implicit none
  real(rk)   :: Pot_dip,r_mu(3),mu(3),r(3),rr(3)
  rr=r-r_mu
  Pot_dip=DotProd3(mu,rr)/Norm3(rr)**3
end function Pot_dip



function Field_chg(r,r_q,q)
  implicit none
  real(rk)   :: Field_chg(3),r(3),r_q(3),q,rr(3)

  rr=r-r_q
  Field_chg=q*rr/(Norm3(rr))**3

end function Field_chg


function Field_dip(r,r_mu,mu)
  implicit none
  real(rk)   :: Field_dip(3),r_mu(3),mu(3),r(3),r_abs,rr(3)
  
  rr=r-r_mu
  r_abs=Norm3(rr)
  Field_dip=(3.0_rk*DotProd3(mu,rr)*rr - (r_abs**2)*mu)/r_abs**5
end function Field_dip


function Field_dip_screen(r,r_mu,mu,fe,ft)
  implicit none
  real(rk)   :: Field_dip_screen(3),r_mu(3),mu(3),r(3),r_abs,rr(3),fe,ft
  
  rr=r-r_mu
  r_abs=Norm3(rr)
  Field_dip_screen=(3_rk*ft*DotProd3(mu,rr)*rr - fe*(r_abs**2)*mu)/r_abs**5
end function Field_dip_screen

#ifdef MPI
subroutine bcast_sys(is_rst)
  ! broadcast system information for MPI parallelization.
  implicit none
  
  logical, intent(in) :: is_rst !if using restart
  
  integer :: i_mol, i_type

  call mpi_bcast(sys_info%mol_types ,1,  MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(sys_info%nmol_tot ,1,  MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(sys_info%at_per_mol_max ,1,  MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  
  if (proc_id/=0) then 
    allocate(sys(sys_info%nmol_tot))
    allocate(i_crp(sys_info%at_per_mol_max,sys_info%mol_types))
    allocate(n_crp(sys_info%mol_types))
    allocate(i_pp(sys_info%at_per_mol_max,sys_info%mol_types))      
    allocate(n_pp(sys_info%mol_types))
    if(is_cr) then
      allocate(aap(sys_info%mol_types,sys_info%at_per_mol_max,sys_info%at_per_mol_max))  
    endif

    do i_mol=1,sys_info%nmol_tot 

      sys(i_mol)%dq_i=0.0_rk
      sys(i_mol)%dq_im1=0.0_rk

      sys(i_mol)%mu_i=0.0_rk    
      sys(i_mol)%mu_im1=0.0_rk 

      sys(i_mol)%V_pc=0.0_rk
      sys(i_mol)%V_0=0.0_rk
      sys(i_mol)%V_dq=0.0_rk
      sys(i_mol)%V_id=0.0_rk
      sys(i_mol)%V_tot=0.0_rk

      sys(i_mol)%F_pc=0.0_rk
      sys(i_mol)%F_0=0.0_rk   
      sys(i_mol)%F_dq=0.0_rk
      sys(i_mol)%F_id=0.0_rk
      sys(i_mol)%F_tot=0.0_rk
      
    enddo  
  endif  
  
  call mpi_bcast(i_crp, sys_info%at_per_mol_max*sys_info%mol_types , MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(n_crp, sys_info%mol_types , MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(i_pp, sys_info%at_per_mol_max*sys_info%mol_types , MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  call mpi_bcast(n_pp, sys_info%mol_types , MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  if (is_cr) then
    call mpi_bcast(aap, sys_info%at_per_mol_max*sys_info%at_per_mol_max*sys_info%mol_types , MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
  endif
  
  do i_mol=1,sys_info%nmol_tot
     
     call mpi_bcast(sys(i_mol)%moltype_id, 1 , MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
   
     i_type=sys(i_mol)%moltype_id
     
     call mpi_bcast(sys(i_mol)%r_at(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)    
     call mpi_bcast(sys(i_mol)%q_0(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
     call mpi_bcast(sys(i_mol)%q_i(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
     call mpi_bcast(sys(i_mol)%acp(1:n_pp(i_type),1:6) ,6*n_pp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)   

     call mpi_bcast(sys(i_mol)%F_ext(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)    
     call mpi_bcast(sys(i_mol)%V_ext(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)

     ! GD 24/7/20
     call mpi_bcast(sys(i_mol)%screen_k(1:n_crp(i_type)),n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr) 
     
     if(is_rst) then
!      print *, "TEST_JL: With rst"
        call mpi_bcast(sys(i_mol)%dq_i(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)     
        call mpi_bcast(sys(i_mol)%mu_i(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)  
        call mpi_bcast(sys(i_mol)%V_0(1:n_crp(i_type)) ,n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr) 
        call mpi_bcast(sys(i_mol)%F_0(1:n_crp(i_type),1:3) ,3*n_crp(i_type),  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)           
     endif

   
  enddo
  
  if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  
    call mpi_bcast(n_rep ,1,  MPI_INTEGER,0,MPI_COMM_WORLD,ierr)  
    if (proc_id/=0) then   
      allocate( r_rep(n_rep, 3))
      allocate( idx_rep(n_rep, 3))
    endif
    call mpi_bcast(r_rep ,n_rep*3,  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)    
    call mpi_bcast(idx_rep, n_rep*3 , MPI_INTEGER,0,MPI_COMM_WORLD,ierr)    
  endif

  if (is_mirr) then
    call mpi_bcast(z_mirr ,1,  MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)    
    call mpi_bcast(i_mirr, 1 , MPI_INTEGER,0,MPI_COMM_WORLD,ierr)    
  endif 

  
end subroutine bcast_sys
#endif

end module electrostatics

!    This file is part of MESCal.
!    Copyright Gabriele D'Avino (2013)
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.
!
module module_mpi
#ifdef MPI
  use mpi
  
  implicit none  

  integer :: ierr  
  integer :: proc_id, num_procs
  integer :: proc_id_source
  
contains  
  subroutine initial_mpi()
  
    implicit none
    
    call MPI_INIT (ierr)
    call MPI_COMM_RANK (MPI_COMM_WORLD, proc_id, ierr)  
    call MPI_COMM_SIZE (MPI_COMM_WORLD, num_procs, ierr) 


  end subroutine
  
  subroutine show_mpi()
  
    implicit none
    
    if (proc_id==0) then
       write(*,*)
       write(*,*) ' INFO: Parallel execution with MPI '
       write(*,*) '       Number of cores: ',num_procs      
       write(*,*)
       write(*,*)
    endif
  
  end subroutine
  

  subroutine finalize_mpi()
  
    implicit none
    
    call MPI_FINALIZE (ierr) 
  
  end subroutine
#endif

end module module_mpi


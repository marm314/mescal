!    This file is part of MESCal.
!    Copyright Gabriele D'Avino (2013)
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.
!
module io
  ! Subroutines for input-output
  use types
  use math

#ifdef MPI
  use module_mpi
#endif  
  
  implicit none
  save
  type(sys_summary) :: sys_info 
  type(molecule),allocatable,dimension(:) :: sys
  real(rk),allocatable,dimension(:,:,:)   :: aap
  integer,allocatable,dimension(:,:)      :: i_crp,i_pp
  integer,allocatable,dimension(:)        :: n_crp,n_pp
  integer           :: n_pp_tot

  character(50)     :: pdbfile
  character(20)     :: linefmt="(a80)",resnamefmt="(17x,a4)"
  character(20)     :: idfmt  ="(22x,i5)",coorfmt="(30x,3(f8.3))" !,coorfmt_pdx="(30x,3(f13.6))" 
  character(20)     :: atlabfmt="(12x,a4)"

contains

subroutine  option_banner(msg,nlines)
  implicit none
  character(80) :: msg(5)
  integer       :: nlines,i

write(*,*)
write(*,*) ' ****************************************************************************'
write(*,*)
do i=1,nlines
   write(*,"(3x,a)") msg(i)
enddo
write(*,*)
write(*,*) ' ****************************************************************************'
write(*,*)
write(*,*)

end subroutine option_banner


subroutine read_input(input_fname)
! 26/10/15 Reads the input file and stores its content in input_file  
  implicit none
  character(50)  ::  input_fname
  integer        ::  stat
  character(80)  ::  line
  logical        ::  found

open(unit=1,file=input_fname,status='old',iostat=stat)

! write input file to stdout
write(*,*)
write(*,*) '  ------   input file start  ------ '
write(*,*)


nl_inp=0
input_file=""
do while(stat==0)
   read(1,linefmt,iostat=stat) line
!   write(*,*) line,line/="",stat

   if (stat==0) write(*,linefmt) line 
   
   if ( (stat==0) .and. (line/=" ") ) then
      nl_inp=nl_inp+1
      input_file(nl_inp)=line 
   endif
enddo
close(1)

write(*,*)
write(*,*) '  ------   input file end  ------ '
write(*,*)

end subroutine read_input


subroutine parse_str(key,val,found)
! 26/10/15 
  implicit none
  character(50) :: key,tag,val
  logical       :: found
  integer       :: i,j,ioerr
  character(80) :: line
  character(20) :: linefmt="(a80)"


found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag

   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      !read(line,*,iostat=ioerr) tag,val
      
      ! split a string in 2 parts.
      line=TRIM(line)
      j=SCAN(line," ")
      tag=line(1:j-1)
      val=line(j+1:)
      ioerr=SIZEOF(val)
         
      !if (ioerr.eq.0) then
      if (ioerr.gt.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_str


subroutine parse_str_long(key,val,found)
! 26/10/15 
  implicit none
  character(50) :: key,tag
  character(150):: val
  logical       :: found
  integer       :: i,ioerr
  character(80) :: line
  character(20) :: linefmt="(a80)"


found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag

   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      read(line,*,iostat=ioerr) tag,val
         
      if (ioerr.eq.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_str_long


subroutine parse_int(key,val,found)
! 26/10/15 
  implicit none
  character(50) :: key,tag
  logical       :: found
  integer       :: i,ioerr,val
  character(80) :: line
  character(20) :: linefmt="(a80)"


found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag

   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      read(line,*,iostat=ioerr) tag,val
         
      if (ioerr.eq.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_int


subroutine parse_4int_3real(key,val_i,val_r,found)
! 05/11/19
  implicit none
  character(50) :: key,tag
  logical       :: found
  integer       :: i,ioerr,val_i(4)
  real(rk)      :: val_r(3)
  character(80) :: line
  character(20) :: linefmt="(a80)"
  

found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag

   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      read(line,*,iostat=ioerr) tag,val_i,val_r
         
      if (ioerr.eq.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_4int_3real




subroutine parse_int_real(key,val_i,val_r,found)
! 19/03/16
  implicit none
  character(50) :: key,tag
  logical       :: found
  integer       :: i,ioerr,val_i
  real(rk)      :: val_r
  character(80) :: line
  character(20) :: linefmt="(a80)"
  

found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag

   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      read(line,*,iostat=ioerr) tag,val_i,val_r
         
      if (ioerr.eq.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_int_real


subroutine parse_int3(key,val,found)
! 26/10/15 
  implicit none
  character(50) :: key,tag
  logical       :: found
  integer       :: i,ioerr,val(3)
  character(80) :: line
  character(20) :: linefmt="(a80)"


found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag

   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      read(line,*,iostat=ioerr) tag,val
         
      if (ioerr.eq.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_int3



subroutine parse_log(key,val,found)
! 26/10/15 
  implicit none
  character(50) :: key,tag
  logical       :: found,val
  integer       :: i,ioerr
  character(80) :: line
  character(20) :: linefmt="(a80)"


found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag

   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      read(line,*,iostat=ioerr) tag,val
         
      if (ioerr.eq.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_log



subroutine parse_real(key,val,found)
! 26/10/15 
  implicit none
  character(50) :: key,tag
  logical       :: found
  integer       :: i,ioerr
  character(80) :: line
  character(20) :: linefmt="(a80)"
  real(rk)      :: val

found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag


   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      read(line,*,iostat=ioerr) tag,val
      
      if (ioerr.eq.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_real


subroutine parse_real3(key,val,found)
! 27/10/15 
  implicit none
  character(50) :: key,tag
  logical       :: found
  integer       :: i,ioerr
  character(80) :: line
  character(20) :: linefmt="(a80)"
  real(rk)      :: val(3)

found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag


   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      read(line,*,iostat=ioerr) tag,val
      
      if (ioerr.eq.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_real3


subroutine parse_real4(key,val,found)
  implicit none
  character(50) :: key,tag
  logical       :: found
  integer       :: i,ioerr
  character(80) :: line
  character(20) :: linefmt="(a80)"
  real(rk)      :: val(4)

found=.false.

do i=1,nl_inp

   line=input_file(i)
   read(line,*) tag


   if (adjustl(trim(tag))==adjustl(trim(key))) then
         
      read(line,*,iostat=ioerr) tag,val
      
      if (ioerr.eq.0) then
         found=.true.
         close(1)
         return
      else
         write(*,*) 'ERROR: Something went wrong with ',adjustl(trim(key)),' input!'
         stop
      endif

   endif

enddo

end subroutine parse_real4



subroutine read_chg_diff
! read differences between atomic charges of charged and neutral molecules
  implicit none
  integer       :: k,i
  character(60) :: fname_dq
  real(rk)      :: q_mol_chk

! read charges of charged species
sys_info%chg_diff=0.0_rk
do k=1,sys_info%mol_types ! loop on chem spec
  
   q_mol_chk=0.0_rk
   if ( abs(sys_info%chg_type(k)).gt.0.9 ) then
      write(*,"(a,f12.6)") "Specie "//sys_info%mol_names(k)//" has net charge",sys_info%chg_type(k)
      write(*,*) "Insert file with atomic charges differences:"

      read(*,*) fname_dq
      write(*,*) ' >> ',trim(fname_dq),' read'
      write(*,*) ' '
      open(file=fname_dq,unit=22,status="old")

      do i=1,n_crp(k)
         read(22,*) sys_info%chg_diff(i,k)
         q_mol_chk=q_mol_chk + sys_info%chg_diff(i,k)
      enddo

      ! check that the net charge is respected
      if (abs(q_mol_chk-sys_info%chg_type(k)).gt.1e-4 ) then
         write(*,*) ' ERROR: net charge does not match with mei/cri file!!!'
         write(*,*) ' '
         call stop_exec 
        
      elseif (abs(q_mol_chk-sys_info%chg_type(k)).gt.1e-6 ) then
      
         write(*,*) ' WARNING: inaccurate matching with net charge from  mei/cri file!'
         write(*,*) ' '

      endif

      close(22)
      
   endif

enddo


end subroutine read_chg_diff


subroutine write_q0_VF_pc
  implicit none
  character(40) :: fout_name
  integer       :: i,i_type,j


write(*,*) ' Insert name of output file: '
read(*,*)  fout_name
write(*,*) ' >> ',trim(fout_name),' read'
write(*,*) ' '

  open(file=fout_name,unit=48,status="replace")
  write(48,*) "%1.i_mol  2.i_at  3.q_0  4.V(Volt)  5-7.F(Volt/Angs)"
 
  do i=1,sys_info%nmol_tot  ! loop on all molecules
     i_type=sys(i)%moltype_id

     do j=1,n_pp(i_type)  ! loop on pp 

        write(48,"(i6,1x,i6,1x,f10.6,4(1x,e16.8))") i,j,sys(i)%q_0(j), &
              iu2ev*sys(i)%V_pc(j),iu2ev*sys(i)%F_pc(j,:)

     enddo
  enddo
  close (48)

write(*,*) 'Data wrote to ',trim(fout_name)
write(*,*)

end subroutine write_q0_VF_pc


subroutine write_dip_dbg(n,ene)
  implicit none
  character(40) :: fmt
  integer       :: i,i_type,j,n
  real(rk)      :: ene


fmt="(3(i6),3(f10.4),3(f10.4))"
!dip_fmt="(2(1x,i8),3(1x,e16.8))"

  write(100+n,"(f10.4)") ene

  do i=1,sys_info%nmol_tot  ! loop on all molecules
     i_type=sys(i)%moltype_id

     do j=1,n_pp(i_type)  ! loop on pp 

        write(100+n,fmt) i,j,i_type,sys(i)%mu_i(j,:),iu2ev*sys(i)%F_pc(j,:)
     enddo
  enddo

end subroutine write_dip_dbg



subroutine add_Fext_notuni(fn_ext)
! GD 24/15/15: input routine for non uniform fields. Quite rough without many checks!
  implicit none
  character(80) :: fn_ext
  integer       :: i,j,i_type
  real(rk)      :: V,F(3)

open(file=fn_ext,unit=22,status="old") 

do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id

   do j=1,n_pp(i_type)  ! loop on atoms
      read(22,*) V,F 
      
      sys(i)%V_ext(j)=sys(i)%V_ext(j) + ev2iu*V
      sys(i)%F_ext(j,:)=sys(i)%F_ext(j,:) + ev2iu*F

   enddo

enddo


close(22)
  
end subroutine add_Fext_notuni


! 25/12/15 disabled because not in use
!subroutine read_VF_pc
!  implicit none
!  character(60) :: fn_V0F0
!  integer       :: i,j,i_m,j_a,i_type
!  real(rk)      :: V0,F0(3),qq
!
!write(*,*) '  You choose to read potential and field of neutral charges from file.'
!write(*,*) '  Insert filename:'
!read(*,*) fn_V0F0
!write(*,*) ' >> ',trim(fn_V0F0),' read'
!write(*,*) ' '
!open(file=fn_V0F0,unit=22,status="old")
!
!
!do i=1,sys_info%nmol_tot  ! loop on all molecules
!   i_type=sys(i)%moltype_id
!
!   sys(i)%V_pc=0.0d0
!   sys(i)%F_pc=0.0d0
!  
!   if (.not.(adjustl(sys_info%mol_names(i_type)).eq."DMY"))  then 
!
!      do j=1,n_pp(i_type)  ! loop on pp 
!      
!         read(22,*) i_m,j_a,qq,V0,F0
!      
!         if ( (i.eq.i_m).and.(j.eq.j_a) ) then 
!            
!            sys(i)%V_pc(j)=ev2iu*V0
!            sys(i)%F_pc(j,:)=ev2iu*F0
!            
!         else 
!            
!            write(*,*) ' ERROR: in reading potential and field from file!'
!            call stop_exec
!
!         endif
!      enddo   
!   endif
!enddo
!
!write(*,*) '  Potential and field read correctly from file'
!write(*,*) 
!write(*,*) 
!
!end subroutine read_VF_pc



subroutine input_kr
  implicit none

write(*,"(a)") ' -- Input for r-dependent dielectric screening  -- '
write(*,"(a)") ' '
write(*,"(a)") ' Relative dielectric constant has the functional form:'
write(*,"(a)") '  K(r)=Ks-(Ks-1)*(1+S*r+.5*S^2*r^2)*exp(-S*r) '
write(*,"(a)") '  From Nagata, Lennartz, J. Chem. Phys. 129, 034709 (2008)'
write(*,"(a)") '  '
write(*,"(a)") '  Insert the values for Ks and S:'
read(*,*) ks_kr,s_kr 
write(*,*) ' >> ', ks_kr,s_kr,' read'
write(*,*) 
write(*,*) 

end subroutine input_kr


subroutine print_mat3(mat)
  implicit none
  real(rk)       :: mat(3,3)

  write(*,"(3(f12.5))") mat(1,1),mat(1,2),mat(1,3)
  write(*,"(3(f12.5))") mat(2,1),mat(2,2),mat(2,3)
  write(*,"(3(f12.5))") mat(3,1),mat(3,2),mat(3,3)
  write(*,*)

end subroutine print_mat3


!  2clear
! subroutine write_dip2pc(fout_name)
! ! This subroutines writes to file dipoles as couples of charges
!   implicit none
!   integer        :: i,j,i_type
!   character(40)  :: fout_name
!   real(rk)       :: d,d2,q,r_mu(3),mu(3),mu_v(3),mu_abs
! 
! 
! d=0.1_rk
! d2=d*0.5_rk
! write(*,"(a,f8.3)") " INFO: distance between charges of the dipole set to ",d
! 
! 
! open(file=fout_name,unit=49,status="replace")
!  
! do i=1,sys_info%nmol_tot  ! loop on all molecules
!    i_type=sys(i)%moltype_id
!      
!    do j=1,n_pp(i_type)  ! loop on pp 
! 
!       r_mu=sys(i)%r_at(j,:)
!       mu=sys(i)%mu_i(j,:)
!       mu_abs=Norm3(sys(i)%mu_i(j,:) )
!       mu_v=mu/mu_abs
!       q=mu_abs/d
! 
!       write(49,"(4(f15.6))") r_mu+d2*mu_v,q
!       write(49,"(4(f15.6))") r_mu-d2*mu_v,-q
!    enddo
! enddo
! close(49)
! 
! end subroutine write_dip2pc


subroutine write_dip_field(fout_name)
! This subroutines writes to file fields and dipoles at each polariable point
  implicit none
  integer   :: i,j,i_type
  character(40)    :: fout_name

  open(file=fout_name,unit=48,status="replace")
  write(48,*) "%1.i_mol 2.i_pp 3-5.xyz 6-8.F 9-11.mu "
 
  do i=1,sys_info%nmol_tot  ! loop on all molecules
     i_type=sys(i)%moltype_id
     
     do j=1,n_pp(i_type)  ! loop on pp 

        write(48,"(i6,i6,9(e12.3))")   i,j,sys(i)%r_at(j,:),iu2ev*sys(i)%F_tot(j,:),sys(i)%mu_i(j,:) 
     enddo
  enddo
  close (48)

end subroutine write_dip_field


subroutine write_dip_chg(fout_name)
! This subroutines writes atomic charges and dipoles
  implicit none
  integer   :: i,j,i_type
  character(40)    :: fout_name

  open(file=fout_name,unit=48,status="replace")
  write(48,*) "%1.i_mol 2.i_pp 3-5.xyz 6.q 7-9.mu "
 
  do i=1,sys_info%nmol_tot  ! loop on all molecules
     i_type=sys(i)%moltype_id
     
     do j=1,n_pp(i_type)  ! loop on pp 

        write(48,"(i6,i6,9(e12.3))")   i,j,sys(i)%r_at(j,:),sys(i)%q_i(j),sys(i)%mu_i(j,:) 
     enddo
  enddo
  close (48)

end subroutine write_dip_chg


subroutine write_q_mu_V_F(fout) 
! This subroutines writes charges, dipoles potential and field at atomic sites
  implicit none
  integer          :: i,j,i_type
  character(80)    :: fout

!2clear
!write(*,*) ' '
!write(*,*) ' You choose option ', option
!write(*,*) ' Write to file charges, induced dipoles, field at potential at all atoms.'
!write(*,*) ' This file can be used for restart purpose.'
!write(*,*) ' '
!write(*,*) ' '

!fout=trim(adjustl(fout))     
open(file=fout,unit=48,status="replace")
write(48,"(a90)") "%1.i_mol 2.i_at 3-5.xyz 6.q_0 7.dq  8-10.mu 11.V 12-14.F 15.V0 16-18.F0 19.mol_type"
 
do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id

   do j=1,n_pp(i_type)  ! loop on pp 

      write(48,"(i6,i6,16(1x,e16.8),i3)") i,j,sys(i)%r_at(j,:),sys(i)%q_0(j),sys(i)%dq_i(j),&
           sys(i)%mu_i(j,:),iu2ev*sys(i)%V_tot(j),iu2ev*sys(i)%F_tot(j,:),&
           iu2ev*sys(i)%V_0(j),iu2ev*sys(i)%F_0(j,:),sys(i)%moltype_id
   enddo
enddo
close(48)

!write(*,*) 'Data wrote to ',trim(fout)
!write(*,*)

end subroutine write_q_mu_V_F


subroutine read_restart(fname_rst)
!GD  4/8/15: created this version that replaces the previous one   
  integer        :: i,j,i_type,i_chk,j_chk
  character(50)  :: fname_rst
  real(rk)       :: xyz(3),q0,dq,mu(3),pot,field(3),pot0,field0(3)

!2clear
!write(*,*) '  Insert the filename with input induced charges and dipoles:'
!read(*,*) fname_rst
!write(*,*) ' >> ',trim(fname_rst),' read'
!write(*,*) ' '

open(file=fname_rst,unit=48,status="old")
read(48,*) ! skip file header

do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id

   do j=1,n_pp(i_type)  ! loop on pp

      !  write(48,*) "%1.i_mol 2.i_at 3-5.xyz 6.q_0 7.dq  8-10.mu 11.V 12-14.F 15.V0 16-18.F0"
      read(48,*) i_chk,j_chk,xyz,q0,dq,mu,pot,field,pot0,field0
      
      if ( (i_chk.eq.i) .and. (j_chk.eq.j) ) then
      
         sys(i)%dq_i(j)=dq
         sys(i)%mu_i(j,:)=mu
         
         ! field and potntial of 0-th iteration 
         sys(i)%V_0(j)=ev2iu*pot0
         sys(i)%F_0(j,:)=ev2iu*field0

         !write(99,*) sys(i)%dq_i(j),sys(i)%mu_i(j,:)
      else
        
         write(*,*) ' '
         write(*,*) ' ERROR: Restart and pdb file do not match! '
         write(*,*) ' '
         write(*,*) 'pdb: ',i,j
         write(*,*) 'rst: ',i_chk,j_chk

         call stop_exec
      endif

   enddo
enddo
close (48)

write(*,*) '  Restart file read correcly!'
write(*,*) 
write(*,*) 

end subroutine read_restart


subroutine read_restart_sitene(fname_rst)
!GD 28/05/21 
  integer        :: i,j,i_type,i_chk,j_chk
  character(50)  :: fname_rst
  real(rk)       :: xyz(3),q0,dq,mu(3),pot,field(3),pot0,field0(3)


open(file=fname_rst,unit=48,status="old")
read(48,*) ! skip file header

do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id

   do j=1,n_pp(i_type)  ! loop on pp

      !  write(48,*) "%1.i_mol 2.i_at 3-5.xyz 6.q_0 7.dq  8-10.mu 11.V 12-14.F 15.V0 16-18.F0"
      read(48,*) i_chk,j_chk,xyz,q0,dq,mu,pot,field,pot0,field0
      
      if ( (i_chk.eq.i) .and. (j_chk.eq.j) ) then
      
!         sys(i)%dq_i(j)=dq
!         sys(i)%mu_i(j,:)=mu
         
         ! field and potential of 0-th iteration 
         sys(i)%V_0(j)=ev2iu*pot0
         sys(i)%F_0(j,:)=ev2iu*field0

         ! field and potential at the last iteration (self consistency)
         sys(i)%V_tot(j)=ev2iu*pot
         sys(i)%F_tot(j,:)=ev2iu*field

      else
        
         write(*,*) ' '
         write(*,*) ' ERROR: Restart and pdb file do not match! '
         write(*,*) ' '
         write(*,*) 'pdb: ',i,j
         write(*,*) 'rst: ',i_chk,j_chk

         call stop_exec
      endif

   enddo
enddo
close (48)

write(*,*) '  Restart file read correcly!'
write(*,*) 
write(*,*) 

end subroutine read_restart_sitene



subroutine write_chg_red(fout_name)
! This subroutines writes to file fields and dipoles at each polariable point
  implicit none
  integer   :: i,j,i_type
  character(40)    :: fout_name

  open(file=fout_name,unit=48,status="replace")
  write(48,*) "%1.i_mol  2.i_at  3-5.xyz  6.dq  7.q  8.q0"
 
  do i=1,sys_info%nmol_tot  ! loop on all molecules
     i_type=sys(i)%moltype_id
     
     do j=1,n_crp(i_type)  ! loop on atoms

        write(48,"(i6,i6,9(e12.3))") i,j,sys(i)%r_at(j,:),sys(i)%q_i(j)-sys(i)%q_0(j),sys(i)%q_i(j),sys(i)%q_0(j)
     enddo
  enddo
  close (48)

end subroutine write_chg_red


!2clear
!subroutine input_screen
!! This subroutines reads the screeening factor, a_screen, and 
!! computes all the screening constants (k_screen) for each atom.
!! Ren-Ponder expression for screening is used, 
!! see, e.g. Wang et J. Phys. Chem. B 2011, 115, 3091 
!
!  implicit none
!  real(rk)   :: r_ij,a_i,a_j,sqai,ot,mos
!  integer    :: i_mol,j_mol,jj_mol,k_pp,l_pp,i_type,j_type
!
!write(*,"(a)") ' -- Input for screened intramolecular dipole-dipole interaction -- '
!write(*,"(a)") ' '
!write(*,"(a)") ' Note that:'
!write(*,"(a)") '   -Tinker exponential (Ren-Ponder) screening function is adopted, '
!write(*,"(a)") '      see Ren, Ponder, J. Phys. Chem. B  107, 5933 (2003)          '
!write(*,"(a)") '   -The larger is the screening factor, the faster is the screeening decay   ' 
!write(*,"(a)") '   -Insert 0 to neglect intramolecular dipole-dipole interaction'
!write(*,"(a)") '   -If screeening constant<0 also intermolecular dipole-dipole '
!write(*,"(a)") '    interactions will be screened.' 
!write(*,*) 
!write(*,"(a)") ' Insert the value for the screeening constant: ' 
!read(*,*)  a_screen
!write(*,*) ' >> ',a_screen,' read'
!write(*,*) 
!
!if (abs(a_screen).lt.1e-6) then
!   write(*,*) 'INFO: All dipole-dipole interactions will not be screened'
!   is_intra_dipdip=.false.
!   screen_inter_dipdip=.false.
!elseif (a_screen.lt.0.0_rk) then
!   a_screen=-a_screen
!   write(*,*) ' INFO: screening factor will be applied to intra- and intermolecular '
!   write(*,*) '       dipole-dipole interactions '
!   is_intra_dipdip=.true.
!   screen_inter_dipdip=.true.
!else
!   write(*,*) ' INFO: screening factor will be applied to intramolecular '
!   write(*,*) '       dipole-dipole interactions only '
!   is_intra_dipdip=.true.
!   screen_inter_dipdip=.false.
!endif
!write(*,*)
!
!
!! Calulcation of alpha_^{-1/6} for all atoms (used in screening factors)
!if (is_intra_dipdip.or.screen_inter_dipdip) then
!   ot=0.3333333333333_rk !  1/3
!   mos=-0.1666666666667_rk !  -1/6
!
!   do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!      i_type=sys(i_mol)%moltype_id
!
!      do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!      
!         sys(i_mol)%screen_k(k_pp)=( ot*(sys(i_mol)%acp(k_pp,1)+ &
!              sys(i_mol)%acp(k_pp,4)+sys(i_mol)%acp(k_pp,6) ) )**mos
!   enddo
!enddo
!
!endif
!
!end subroutine input_screen


subroutine input_screen
! This subroutines reads the screeening factor, a_screen, and 
! computes all the screening constants (k_screen) for each atom.
! Ren-Ponder expression for screening is used, 
! see, e.g. Wang et J. Phys. Chem. B 2011, 115, 3091 
!
! GD 26/10/15 modified for new flexible input 

  implicit none
  real(rk)   :: r_ij,a_i,a_j,sqai,ot,mos
  integer    :: i_mol,j_mol,jj_mol,k_pp,l_pp,i_type,j_type
  logical    :: found
  character(50) :: key

write(*,*) ' -- Input for distance-dependent screening of dipole fields -- '
write(*,*) ' '

key="SCREEN_DIP_INTRA" 
call parse_log(key,screen_intra_dipdip,found) 
if (found) then 
   write(*,"(1x,a20,a2,l1)") key,': ',screen_intra_dipdip
else
   screen_intra_dipdip=.false.
   write(*,"(1x,a20,a2,l1,a10)") key,': ',screen_intra_dipdip,' (default)'
endif

key="SCREEN_DIP_INTER"
call parse_log(key,screen_inter_dipdip,found) 
if (found) then 
   write(*,"(1x,a20,a2,l1)") key,': ',screen_inter_dipdip
else
   screen_inter_dipdip=.false.
   write(*,"(1x,a20,a2,l1,a10)") key,': ',screen_inter_dipdip,' (default)'
endif

key="SCREENING"
call parse_real(key,a_screen,found) 
if (found) then 
   write(*,"(1x,a20,a2,f6.3,a10)") key,': ',a_screen
else
   a_screen=1000000.0_rk   
   !   write(*,"(1x,a20,a2,f6.3,a10)") key,': ',a_screen,' (default)'
   write(*,"(1x,a20,a20)") key,': disabled  (default)'
endif
write(*,*)
if (a_screen.le.0.0_rk) then
   write(*,*) '  ERROR: the screening constant must be positive!'
   call stop_exec
endif



! GD 07/04/21  this became irrelevant with the new simplified
! treatment of screened short-range dipole fields
! 2clear
! Calulcation of alpha_^{-1/6} for all atoms (used in screening factors)
!if (screen_intra_dipdip.or.screen_inter_dipdip) then
!   ot=0.3333333333333_rk !  1/3
!   mos=-0.1666666666667_rk !  -1/6
!
!   do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!      i_type=sys(i_mol)%moltype_id
!
!      do k_pp=1,n_pp(i_type)  ! loop on pp of i_mol 
!      
!         sys(i_mol)%screen_k(k_pp)=( ot* (sys(i_mol)%acp(k_pp,1)+ &
!              sys(i_mol)%acp(k_pp,4) + sys(i_mol)%acp(k_pp,6) ) )**mos
!   enddo
!enddo
!endif

end subroutine input_screen

!! GD 15/12/15 : useless and not maintained 2clear 
!subroutine check_distances
!! This subroutines checks the charge-charge, charge-dipole and 
!! dipole-dipole distances. If some distance is smaller
!! than a given tolerance a warning is printed!
!  implicit none
!  real(rk)   :: qq_min,qd_min,dd_min,dist
!  integer    :: i_mol,j_mol,jj_mol,k_crp,k_pp,l_crp,l_pp,i_type,j_type
!  integer    :: nqq,nqd,ndd
!
!
!qq_min=2.5_rk
!qd_min=3.8_rk
!dd_min=3.8_rk
!write(*,*) ' Checking charge-charge, charge-dipole and  dipole-dipole distances '
!write(*,*)
!
!open(file="dist_qq.dat",unit=201,status="replace")
!open(file="dist_qd.dat",unit=202,status="replace")
!open(file="dist_dd.dat",unit=203,status="replace")
!
!
!nqq=0
!nqd=0
!ndd=0
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!   i_type=sys(i_mol)%moltype_id
!   
!
!   do jj_mol=1,sys(i_mol)%nnb ! loop on the nb list of i_mol
!      j_mol=sys(i_mol)%nb_list(jj_mol)
!      j_type=sys(j_mol)%moltype_id
!
!
!      ! charge-charge distance
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_crp=1,n_crp(j_type) ! loop on crp of j_mol
!
!                 dist=Norm3(sys(i_mol)%r_at(i_crp(k_crp,i_type),:)- &
!                 sys(j_mol)%r_at(i_crp(l_crp,j_type),:) )
!
!                 if (dist.le.qq_min) then
!                    !write(*,*) " WARNING: small charge-charge distance: "
!                    !write(*,*) "  chg: mol ",i_mol," at ",k_crp
!                    !write(*,*) "  chg: mol ",j_mol," at ",l_crp
!                    nqq=nqq+1
!                    write(201,"(f12.3)") dist                    
!                 endif
!         enddo
!      enddo
!
!      ! charge-dipole distance
!      do k_crp=1,n_crp(i_type)  ! loop on crp of i_mol 
!         do l_pp=1,n_pp(j_type) ! loop on crp of j_mol
!
!                 dist=Norm3(sys(i_mol)%r_at(i_crp(k_crp,i_type),:)- &
!                 sys(j_mol)%r_at(i_pp(l_pp,j_type),:) )
!
!                 if (dist.le.qd_min) then
!!                    write(*,*) " WARNING: small charge-dipole distance: "
!!                    write(*,*) "  chg: mol ",i_mol," at ",k_crp
!!                    write(*,*) "  dip: mol ",j_mol," at ",l_pp
!                    write(202,"(f12.3)") dist                    
!                    nqd=nqd+1
!                 endif
!         enddo
!      enddo
!
!
!      ! dipole-dipole distance
!      do k_pp=1,n_pp(i_type)  ! loop on crp of i_mol 
!         do l_pp=1,n_pp(j_type) ! loop on crp of j_mol
!
!                 dist=Norm3(sys(i_mol)%r_at(i_pp(k_pp,i_type),:)- &
!                 sys(j_mol)%r_at(i_pp(l_pp,j_type),:) )
!
!                 if (dist.le.dd_min) then
!!                    write(*,*) " WARNING: small dipole-dipole distance: "
!!                    write(*,*) "  dip: mol ",i_mol," at ",k_pp
!!                    write(*,*) "  dip: mol ",j_mol," at ",l_pp
!                    write(203,"(f12.3)") dist                    
!                    ndd=ndd+1
!                 endif
!         enddo
!      enddo
!
!   enddo
!enddo
!
!write(*,*)
!write(*,"(a,i8,a,f8.3)") ' Found ',nqq,' charge-charge distances lower than ',qq_min
!write(*,"(a,i8,a,f8.3)") ' Found ',nqd,' charge-dipole distances lower than ',qd_min
!write(*,"(a,i8,a,f8.3)") ' Found ',ndd,' dipole-dipole distances lower than ',dd_min
!write(*,*)
!
!write(*,*) " Small distances wrote to files: dist_qq.dat, dist_dq.dat, dist_dd.dat"
!write(*,*)
!
!close(201)
!close(202)
!close(203)
!
!end subroutine check_distances



! ! GD 15/12/15 routine disabled  2clear
! subroutine cutoff_nblist
! ! Last Modify: 15 Nov 2012
! ! This subroutine defines a neighbors list.  
! ! For hystorical reasons the nb list is based on a cutoff distance 
! !(between centers of mass), but in the current implementation the 
! ! program doesn't read the cutoff which is set to 1e10, 
! ! i.e. practically there is no cutoff.
!   implicit none
!   real(rk)    :: cutoff,r_ij
!   integer    :: i_mol,j_mol
! 
! !write(*,*) ' Insert the value for cutoff distance in Angstom: '
! !read(*,*)  cutoff
! !write(*,*) ' >> ',cutoff,' read'
! !write(*,*) ' '
! cutoff=1.0e10_rk
! 
! do i_mol=1,sys_info%nmol_tot
!    sys(i_mol)%nnb=0
! !2c   sys(i_mol)%nb_list=0 
! 
!    do j_mol=1,sys_info%nmol_tot
!       if (i_mol.ne.j_mol) then
! 
!          r_ij=Norm3(sys(i_mol)%cm-sys(j_mol)%cm)
!          if (r_ij.le.cutoff) then
!             sys(i_mol)%nnb=sys(i_mol)%nnb+1
! !2c            sys(i_mol)%nb_list(sys(i_mol)%nnb)=j_mol
!          endif
!          
!       endif
!    enddo
! !   write(*,*) 'molecule: ', i_mol,'  has', sys(i_mol)%nnb,' neighbors'
! !   write(*,*) 'neighbor id: ', sys(i_mol)%nb_list(1:sys(i_mol)%nnb)
! !   write(*,*) 
! enddo
! !   write(*,"(3x,a28,i12)") 'Total number of atoms: ',sys_info%nat_tot
! !   write(*,"(3x,a28,i12)") 'Total number of molecules: ',sys_info%nmol_tot   
! !   write(*,"(a8,a8,2x,a8,2x,a14)") 'MolId','MolName','# mol.','atoms per mol.'
! !   do i=1,sys_info%mol_types
! !      write(*,"(i8,a8,2x,i8,2x,i14)") i,sys_info%mol_names(i),sys_info%nmol_type(i),sys_info%nat_type(i)
! !write(*,*) !'ok'
! end subroutine cutoff_nblist


subroutine input_replica_2D
! GD 27/10/15 new input
! read the cell parameters and the radius of the disk radius 
! defining periodic replica within cutoff
  implicit none 
  logical       :: found
  character(50) :: key

call parse_pbcbox
CellVolume=DotProd3(cell2rep(1,:),VectorProd3(cell2rep(2,:),cell2rep(3,:)))

key="PBC_CUTOFF"
call parse_real(key,Rc_rep,found) 
if (found) then 
   write(*,"(1x,a20,a2,f10.5)") key,': ',Rc_rep
else
   write(*,*) "ERROR: PBC_CUTOFF input not found!"
   call stop_exec 
endif

key="PBC_SLAB_VEC"
call parse_int(key,u_tf,found) 
if (found) then 
   write(*,"(1x,a20,a2,i3)") key,': ',u_tf
else
   write(*,*) "ERROR: PBC_SLAB_VEC input not found!"
   call stop_exec 
endif

Nrep_tf=0 ! disabled periodic replica above and below

! 2clear
!write(*,"(a)") ' -- Input for cell parameters and cutoff radius -- '
!write(*,"(a)") ' '
!write(*,"(a)") ' '
!write(*,"(a)") ' Insert cell vectors a, b and c (in Angstrom) '
!read(*,*) cell2rep(1,:)
!read(*,*) cell2rep(2,:)
!read(*,*) cell2rep(3,:)
!write(*,"(a)") ' '
!write(*,"(a)") ' Values read:'
!write(*,"(a,3(f12.5))")  '  a: ',cell2rep(1,:)
!write(*,"(a,3(f12.5))")  '  b: ',cell2rep(2,:)
!write(*,"(a,3(f12.5))")  '  c: ',cell2rep(3,:)
!write(*,*) ' '
!write(*,"(a)") ' Insert: '
!write(*,"(a)") "   -Cutoff radius for interaction with periodic replica"
!write(*,"(a)") "   -Direction along you DON'T want to replicate (a=1, b=2, c=3)"
!write(*,"(a)") "   -Number of replica you want above and below your sample"
!read(*,*) Rc_rep,u_tf,Nrep_tf
!write(*,*) ' '
!write(*,"(a)") ' Values read:'
!write(*,"(a,f12.5)")  '  R: ',Rc_rep
!write(*,"(a,i3)")     '  Dir.',u_tf
!write(*,"(a,i3)")     '  Nrep.',Nrep_tf
!write(*,*) 
!write(*,*) 

end subroutine input_replica_2D


subroutine input_replica_3D
! GD 27/10/15 new input
! read the cell parameters and the radius of the sphere 
! defining periodic replica within cutoff
  implicit none 
  logical       :: found
  character(50) :: key


call parse_pbcbox
CellVolume=DotProd3(cell2rep(1,:),VectorProd3(cell2rep(2,:),cell2rep(3,:)))
!write(*,*) CellVolume

key="PBC_CUTOFF"
call parse_real(key,Rc_rep,found) 
if (found) then 
   write(*,"(1x,a20,a2,f10.5)") key,': ',Rc_rep
else
   write(*,*) "ERROR: PBC_CUTOFF input not found!"
   call stop_exec 
endif


! 2clear
!write(*,"(a)") ' -- Input for cell parameters and cutoff radius -- '
!write(*,"(a)") ' '
!write(*,"(a)") ' '
!write(*,"(a)") ' Insert cell vectors a, b and c (in Angstrom) '
!read(*,*) cell2rep(1,:)
!read(*,*) cell2rep(2,:)
!read(*,*) cell2rep(3,:)
!write(*,"(a)") ' '
!write(*,"(a)") ' Values read:'
!write(*,"(a,3(f12.5))")  '  a: ',cell2rep(1,:)
!write(*,"(a,3(f12.5))")  '  b: ',cell2rep(2,:)
!write(*,"(a,3(f12.5))")  '  c: ',cell2rep(3,:)
!write(*,*) ' '
!write(*,"(a)") ' Insert cutoff radius for interaction with periodic replica '
!read(*,*) Rc_sph
!write(*,*) 
!write(*,"(a,3(f10.3))") ' Value read, R=',Rc_sph
!write(*,*) 
!write(*,*) 

end subroutine input_replica_3D



! 2clear
!subroutine input_replica_2D
!! read the cell parameters and the radius of the disk radius 
!! defining periodic replica within cutoff
!  implicit none 
!
!
!
!write(*,"(a)") ' -- Input for cell parameters and cutoff radius -- '
!write(*,"(a)") ' '
!write(*,"(a)") ' '
!
!write(*,"(a)") ' Insert cell vectors a, b and c (in Angstrom) '
!read(*,*) cell2rep(1,:)
!read(*,*) cell2rep(2,:)
!read(*,*) cell2rep(3,:)
!write(*,"(a)") ' '
!write(*,"(a)") ' Values read:'
!write(*,"(a,3(f12.5))")  '  a: ',cell2rep(1,:)
!write(*,"(a,3(f12.5))")  '  b: ',cell2rep(2,:)
!write(*,"(a,3(f12.5))")  '  c: ',cell2rep(3,:)
!write(*,*) ' '
!
!CellVolume=DotProd3(cell2rep(1,:),VectorProd3(cell2rep(2,:),cell2rep(3,:)))
!
!
!write(*,"(a)") ' Insert: '
!write(*,"(a)") "   -Cutoff radius for interaction with periodic replica"
!write(*,"(a)") "   -Direction along you DON'T want to replicate (a=1, b=2, c=3)"
!write(*,"(a)") "   -Number of replica you want above and below your sample"
!read(*,*) Rc_tf,u_tf,Nrep_tf
!write(*,*) ' '
!write(*,"(a)") ' Values read:'
!write(*,"(a,f12.5)")  '  R: ',Rc_tf
!write(*,"(a,i3)")     '  Dir.',u_tf
!write(*,"(a,i3)")     '  Nrep.',Nrep_tf
!write(*,*) 
!write(*,*) 
!
!end subroutine input_replica_2D
!
!
!subroutine input_replica_3D
!! read the cell parameters and the radius of the sphere 
!! defining periodic replica within cutoff
!  implicit none 
!
!
!write(*,"(a)") ' -- Input for cell parameters and cutoff radius -- '
!write(*,"(a)") ' '
!write(*,"(a)") ' '
!
!write(*,"(a)") ' Insert cell vectors a, b and c (in Angstrom) '
!read(*,*) cell2rep(1,:)
!read(*,*) cell2rep(2,:)
!read(*,*) cell2rep(3,:)
!write(*,"(a)") ' '
!write(*,"(a)") ' Values read:'
!write(*,"(a,3(f12.5))")  '  a: ',cell2rep(1,:)
!write(*,"(a,3(f12.5))")  '  b: ',cell2rep(2,:)
!write(*,"(a,3(f12.5))")  '  c: ',cell2rep(3,:)
!write(*,*) ' '
!CellVolume=DotProd3(cell2rep(1,:),VectorProd3(cell2rep(2,:),cell2rep(3,:)))
!
!
!write(*,"(a)") ' Insert cutoff radius for interaction with periodic replica '
!read(*,*) Rc_sph
!write(*,*) 
!write(*,"(a,3(f10.3))") ' Value read, R=',Rc_sph
!write(*,*) 
!write(*,*) 
!
!end subroutine input_replica_3D


subroutine build_replica_2D 
! Create the periodic replica for thin film calculations
  implicit none
  integer      :: i_type,i_mol,i,i1,i2,i3,n_avg,n_cell,n_mol_tf
  integer      :: allocstat
  integer      :: build_step
  real(rk)     :: Rc(3),v1(3),v2(3),v3(3),trasl(3),Lmin
  

call input_replica_2D


if (u_tf.eq.3) then     ! ab plane film
   v1=cell2rep(1,:)
   v2=cell2rep(2,:)
   v3=cell2rep(3,:)
elseif (u_tf.eq.2) then ! ac plane film
   v1=cell2rep(1,:)
   v2=cell2rep(3,:)
   v3=cell2rep(2,:)
elseif (u_tf.eq.1) then ! bc plane film
   v1=cell2rep(2,:)
   v2=cell2rep(3,:)
   v3=cell2rep(1,:)
endif


Nrep_tf=0 ! disabled periodic replica above and below


! GD 26/02/21
! bug fixed: the distance of replica to be included must be computed from the origin!!
!
! compute thin film centroid
! Rc=0.0_rk
!n_avg=0
!do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!   i_type=sys(i_mol)%moltype_id
!
!   do i=1,n_crp(i_type)  ! loop on atoms
!      n_avg=n_avg+1
!
!      Rc(1)=Rc(1) + sys(i_mol)%r_at(i_crp(i,i_type),1)
!      Rc(2)=Rc(2) + sys(i_mol)%r_at(i_crp(i,i_type),2)
!      Rc(3)=Rc(3) + sys(i_mol)%r_at(i_crp(i,i_type),3)
!   enddo
!enddo
!Rc=Rc/n_avg

Lmin=min(norm3(v1),norm3(v2))
n_cell=ceiling(3.00*Rc_rep/Lmin)

! The replicas arrays are dynamically allocated
!
! Since there is no analytical solution to the Gauss circle problem
! we have to count the number of replicas via brute-force:
! build_step = 1 --> Calculate number of replicas (n_rep) and
!                    allocate the arrays
! build_step = 2 --> Fill the replicas arrays

do build_step=1,2

   if ( build_step.eq.1 ) then
      n_mol_tf=0
   else
      r_rep=0.0_rk
      idx_rep=0
   end if

   n_rep = 0

   do i3=-Nrep_tf,Nrep_tf
      do i1=-n_cell,n_cell
         do i2=-n_cell,n_cell
            if (.not.((i1.eq.0).and.(i2.eq.0).and.(i3.eq.0))) then
               

               trasl=i1*v1+i2*v2+i3*v3

               
               if (norm3((trasl)).le.Rc_rep)  then
                  
                  do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

                     i_type=sys(i_mol)%moltype_id

                     if ( build_step.eq.1 ) then ! Only count the number of replicas
                        n_rep = n_rep + n_crp(i_type)
                        n_mol_tf=n_mol_tf+1

                        ! Check that the number of replicas is not bigger that the biggest
                        ! possible integer with the given integer kind
                        
                        if ( n_rep.eq.huge(n_rep) ) then
                           write(*,*) ' ERROR: I cannot handle so many replicas! '
                           call stop_exec 
                        endif

                     else    ! Generate the replicas coordinates

                        do i=1,n_crp(i_type)  ! loop on atoms
                           
                           if (n_crp(i_type).ne.n_pp(i_type)) then
                              write(*,*) ' ERROR: Number of dipoles and atoms per molecule MUST be equal! '
                              call stop_exec
                           endif
                     
                           n_rep=n_rep+1
                           r_rep(n_rep,:)=trasl+sys(i_mol)%r_at(i_crp(i,i_type),:)

                           ! check that replica are correct
                           !if (i.eq.10)  write(17,*) trasl+sys(i_mol)%r_at(i_crp(i,i_type),:)
!                           if ((i.eq.1).and.(i_mol.eq.1)) then 
!                              write(17,*) trasl+sys(i_mol)%r_at(i_crp(i,i_type),:)
!                           endif 

                           idx_rep(n_rep,1)=i_type
                           idx_rep(n_rep,2)=i_mol
                           idx_rep(n_rep,3)=i
                     
                           if ( norm3(sys(i_mol)%r_at(i_crp(i,i_type),:)-sys(i_mol)%r_at(i_pp(i,i_type),:) ).ge.1e-3) then
                              write(*,*) ' ERROR: Dipoles and charges MUST be defined at same positions! '
                              call stop_exec 
                           endif
                           
                        enddo
                     endif
                  enddo
               endif
            endif
         enddo
      enddo
   enddo

   ! At the first iteration, allocate the replicas arrays

   if ( build_step.eq.1 ) then

      allocate ( r_rep(n_rep, 3), stat = allocstat)
      
      if (allocstat.ne.0) then
         write(*,*) ' ERROR: Not enough memory to allocate r_rep(:,3)! '
         call stop_exec
      endif
      
      allocate ( idx_rep(n_rep, 3), stat = allocstat)
      
      if (allocstat.ne.0) then
         write(*,*) ' ERROR: Not enough memory to allocate idx_rep(:,3)! '
         call stop_exec
      endif
   end if

enddo

write(*,*) ' The periodic replica include: '
write(*,"(a,i8)") '  # molecules: ',n_mol_tf
write(*,"(a,i8)") '  # atoms:     ',n_rep
write(*,*) 
write(*,*) 

end subroutine build_replica_2D


subroutine build_replica_3D 
! Create the periodic replica for sphere calculations
  implicit none
  integer      :: i_type,i_mol,i,i1,i2,i3,n_avg,n_cell,n_mol_sph
  integer      :: allocstat
  integer      :: build_step
  real(rk)     :: Rc(3),v1(3),v2(3),v3(3),trasl(3),Lmin


call input_replica_3D
v1=cell2rep(1,:)
v2=cell2rep(2,:)
v3=cell2rep(3,:)

! GD 26/02/21
! bug fixed: the distance of replica to be included must be computed from the origin!!
!
!Rc=0.0_rk
!n_avg=0
!do i_mol=1,sys_info%nmol_tot ! loop on all molecules
!   i_type=sys(i_mol)%moltype_id
!
!   do i=1,n_crp(i_type)  ! loop on atoms
!      n_avg=n_avg+1
!
!      Rc(1)=Rc(1) + sys(i_mol)%r_at(i_crp(i,i_type),1)
!      Rc(2)=Rc(2) + sys(i_mol)%r_at(i_crp(i,i_type),2)
!      Rc(3)=Rc(3) + sys(i_mol)%r_at(i_crp(i,i_type),3)
!   enddo
!enddo
!Rc=Rc/n_avg

Lmin=min(norm3(v3),min(norm3(v1),norm3(v2)))
n_cell=ceiling(3.00*Rc_rep/Lmin)

! The replicas arrays are dynamically allocated
!
! Since there is no analytical solution to the Gauss circle problem
! we have to count the number of replicas via brute-force:
! build_step = 1 --> Calculate number of replicas (n_rep) and
!                    allocate the arrays
! build_step = 2 --> Fill the replicas arrays

do build_step=1,2

   if ( build_step.eq.1 ) then
      n_mol_sph=0
   else
      r_rep=0.0_rk
      idx_rep=0
   end if

   n_rep=0

   do i1=-n_cell,n_cell
      do i2=-n_cell,n_cell
         do i3=-n_cell,n_cell
            if ( .not.((i1.eq.0).and.(i2.eq.0).and.(i3.eq.0)) ) then

               trasl=i1*v1+i2*v2+i3*v3
               
               if (norm3(trasl).le.Rc_rep)  then

                  do i_mol=1,sys_info%nmol_tot  ! loop on all molecules

                     i_type=sys(i_mol)%moltype_id
                     
                     if ( build_step.eq.1 ) then ! Only count the number of replicas
                        n_rep = n_rep + n_crp(i_type)
                        n_mol_sph=n_mol_sph+1

                        ! Check that the number of replicas is not bigger that the biggest
                        ! possible integer with the given integer kind
                        
                        if ( n_rep.eq.huge(n_rep) ) then
                           write(*,*) ' ERROR: I can not represent so many replicas! '
                           call stop_exec 
                        endif

                     else    ! Generate the replicas coordinates

                        do i=1,n_crp(i_type)  ! loop on atoms

                           if (n_crp(i_type).ne.n_pp(i_type)) then
                              write(*,*) ' ERROR: Number of dipoles and atoms per molecule MUST be equal! '
                              call stop_exec
                           endif
            
                           n_rep=n_rep+1
                           r_rep(n_rep,:)=trasl+sys(i_mol)%r_at(i_crp(i,i_type),:)
                           
                           idx_rep(n_rep,1)=i_type
                           idx_rep(n_rep,2)=i_mol
                           idx_rep(n_rep,3)=i
                           
                           if ( norm3(sys(i_mol)%r_at(i_crp(i,i_type),:)-sys(i_mol)%r_at(i_pp(i,i_type),:) ).ge.1e-3) then
                              write(*,*) ' ERROR: Dipoles and charges MUST be defined at same positions! '
                              call stop_exec 
                           endif

                        enddo
                     endif
                  enddo
               endif
            endif
         enddo
      enddo
   enddo

   ! At the first iteration, allocate the repliccas arrays
   
   if ( build_step.eq.1 ) then
      
      allocate ( r_rep(n_rep, 3), stat = allocstat)
      
      if (allocstat.ne.0) then
         write(*,*) ' ERROR: Not enough memory to allocate r_rep(:,3)! '
         call stop_exec
      endif
      
      allocate ( idx_rep(n_rep, 3), stat = allocstat)
      
      if (allocstat.ne.0) then
         write(*,*) ' ERROR: Not enough memory to allocate idx_rep(:,3)! '
         call stop_exec
      endif
   end if

enddo


write(*,*) ' The periodic replica include: '
write(*,"(a,i8)") '  # molecules: ',n_mol_sph
write(*,"(a,i8)") '  # atoms:     ',n_rep
write(*,*)
write(*,*)

end subroutine build_replica_3D


! 2clear
!subroutine input_Fext_uni
!! This subroutine reads from stdin the values of the xyz components of the uniform
!! external field and stores the values of the field and potential in
!! sys(i_mol)%Fext and sys(i_mol)%Vext 
!  implicit none
!  real(rk)     ::  Fx,Fy,Fz,F_ext_uni(3)
!
!
!write(*,"(a)") ' -- Input for the uniform external field -- '
!write(*,"(a)") ' '
!
!write(*,"(a)") ' Insert x, y and z components of the uniform external '
!write(*,"(a)") ' electric field in Volt/Angstrom:'
!read(*,*) Fx,Fy,Fz
!write(*,"(a)") ' '
!write(*,"(a)") ' Values read:'
!write(*,"(a,f12.5)")  '  Fx: ',Fx
!write(*,"(a,f12.5)")  '  Fy: ',Fy
!write(*,"(a,f12.5)")  '  Fz: ',Fz
!write(*,"(a)") ' '
!write(*,"(a)") ' '
!
!
!F0=1e10*sqrt(Fx**2+Fy**2+Fz**2)
!
!F_ext_uni(1)=Fx*ev2iu ! V*A to internal units conversion
!F_ext_uni(2)=Fy*ev2iu
!F_ext_uni(3)=Fz*ev2iu
!
!call set_Fext_uni(F_ext_uni)
!
!end subroutine input_Fext_uni


subroutine set_Fext_zero
! This subroutine sets to zero external field and potential 
  implicit none
  integer      :: i_mol

do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
   sys(i_mol)%F_ext=0.0_rk
   sys(i_mol)%F_ext=0.0_rk
enddo

end subroutine set_Fext_zero


subroutine set_Fext_uni(F_ext_uni)
! This subroutine sets a uniform external field and the corresponding potential
! Value from global variable F_ext_uni
  implicit none
  real(rk)     :: F_ext_uni(3)
  integer      :: i_mol,i_type,i

F0=1e10*sqrt(F_ext_uni(1)**2+F_ext_uni(2)**2+F_ext_uni(3)**2)
F_ext_uni=ev2iu*F_ext_uni ! V/A to internal units conversion

do i_mol=1,sys_info%nmol_tot   ! loop on all molecules
   i_type=sys(i_mol)%moltype_id
   sys(i_mol)%F_ext=0.0_rk
   sys(i_mol)%V_ext=0.0_rk

   ! Set external field on pp
   do i=1,n_pp(i_type)  ! loop on pp 
      sys(i_mol)%F_ext(i,1)=F_ext_uni(1)
      sys(i_mol)%F_ext(i,2)=F_ext_uni(2)
      sys(i_mol)%F_ext(i,3)=F_ext_uni(3)
   enddo

   ! Set external potential on crp 
   do i=1,n_crp(i_type)  ! loop on pp 
      sys(i_mol)%V_ext(i)=-F_ext_uni(1)*sys(i_mol)%r_at(i_crp(i,i_type),1) &
                          -F_ext_uni(2)*sys(i_mol)%r_at(i_crp(i,i_type),2) & 
                          -F_ext_uni(3)*sys(i_mol)%r_at(i_crp(i,i_type),3) 
   enddo

enddo

end subroutine set_Fext_uni



subroutine write_dipoles
! This subroutine writes induced dipole to file
  implicit none
  integer       :: i_mol,i_type,i,stat
  character(50) :: dip_fname,dip_fmt

write(*,*) '  Insert the filename for induced dipoles '
read(*,*) dip_fname
write(*,*) ' >> ',trim(dip_fname),' read'
write(*,*) ' '

open(file=dip_fname,unit=22,iostat=stat,status="replace")
dip_fmt="(2(1x,i8),3(1x,e16.8))"

do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i_mol)%moltype_id

   ! Set external field on pp
   do i=1,n_pp(i_type)  ! loop on pp 

      write(22,dip_fmt) i_mol,i,sys(i_mol)%mu_i(i,:)

   enddo
enddo

close(22)
write(*,*) '  Dipoles wrote to file: ',trim(dip_fname)
write(*,*)
write(*,*)

end subroutine write_dipoles


subroutine input_pp_general(sys_info,n_pp,i_pp)
! This subroutine reads the heading of the *.mei file
! (submol or atomistic and rigid, thio phene, ...)    
  implicit none
  type(sys_summary),intent(in)  :: sys_info 
  integer                       :: i_mol,k,int1,stat,mtype
  integer                       :: nat_sm(moltype_max,at_per_mol_max),i_sm(moltype_max,at_per_mol_max,100)
  integer,allocatable,dimension(:,:)     :: i_pp
  integer,allocatable,dimension(:)       :: n_pp
  integer,allocatable,dimension(:,:,:)   :: orient_idx
  real(rk),allocatable,dimension(:,:)    :: w_pp
  real(rk),allocatable,dimension(:,:,:)  :: w_pp_ani
  real(rk),allocatable,dimension(:,:,:)  :: alpha_t
  real(rk),allocatable,dimension(:,:,:)  :: alpha_F 
  real(rk)        :: alpha_iso(moltype_max),to_skip,alpha_tensor(6) 
  character(50)   :: info_fname
  character(50)   :: first_line
  character(1)    ::  rigid_or(moltype_max) ! this char specifies if the molecule 
  ! is rigid (R) or not:  T for thio phenes, F for the free atomistic representation
  character(1)    ::  at_or_sm(moltype_max)  ! this char specifies if the molecular 
  ! polarizability tensor is distributed over atoms or submolecules
  character(1)    ::  w_type(moltype_max)  ! this char specifies the weigth for the 
  ! distribution of polarizability over atoms


allocate(i_pp(sys_info%at_per_mol_max,sys_info%mol_types))
allocate(w_pp(sys_info%at_per_mol_max,sys_info%mol_types))
allocate(w_pp_ani(sys_info%at_per_mol_max,sys_info%mol_types,3))
allocate(alpha_t(sys_info%mol_types,3,3))
allocate(alpha_F(sys_info%at_per_mol_max,6,sys_info%mol_types))
allocate(n_pp(sys_info%mol_types))
allocate(orient_idx(sys_info%at_per_mol_max,3,sys_info%mol_types))


i_pp=0
n_pp=0
w_pp=0
alpha_t=0.0_rk
rigid_or=""
at_or_sm=""
w_type=""
orient_idx=0 ! atomic indices to give molecular orientation. 
! orient_idx is 3D matrix with i,j,k: i--> pp indices;
! j=1,2,3 atom idx to assign local frame;  k-->chem spec
! Notice that for rigid representation only the first line (i=1) is used!
! All atoms pp have same orientation!

! variables for submolecule representation
!i_sm=0
!nat_sm=0

! atomic polarizabilities for atomistic free represetation
! 3D matix with indices i,j,k: i--> atoms j--> components (upper triangle) k-->chem spec


write(*,*) 'Classical polarizable points: '
alpha_F=0.0 
!!!!!!!!!!!! READ PARAMETERS FOR ALL CHEMICAL SPECIES  !!!!!!!!!!!!!!
do k=1,sys_info%mol_types ! loop on chem spec
   info_fname=sys_info%mei_fn(k)
   open(file=info_fname,unit=20,iostat=stat,status="old")

   ! reading the info for classic polarizability for specie k
   read(20,"(a)") first_line
   first_line=trim(adjustl(first_line))
   read(first_line,*) int1,n_pp(k),at_or_sm(k),rigid_or(k)

   if ( (rigid_or(k).ne."F") .and. (rigid_or(k).ne."I") .and. (rigid_or(k).ne."T") ) then
      read(first_line,*) int1,n_pp(k),at_or_sm(k),rigid_or(k),w_type(k)
   endif

   if (at_or_sm(k).eq.'S' ) then

      write(*,*) ' ' 
      write(*,*) ' ERROR: Distribution of polarizability over submolecules'
      write(*,*) '          has been disabled! ' 
      call stop_exec

   elseif (at_or_sm(k).eq.'I') then ! isotropic polarizable point

      read(20,*) to_skip,to_skip,to_skip,alpha_iso(k) 
      i_pp(1,k)=1
      is_dummy(k)=.true.
      ! GD 14/12/15 
      ! overwrite charge 

   elseif (at_or_sm(k).eq.'T') then ! anisotropic polarizable point

      read(20,*) to_skip,to_skip,to_skip,alpha_tensor
      i_pp(1,k)=1
      is_dummy(k)=.true.
      
   elseif ( (at_or_sm(k).eq.'A' ) .and. (rigid_or(k).eq.'R') ) then
      call input_pp_AR(w_type(k))  ! atomistic rigid

   elseif ( (at_or_sm(k).eq.'A' ) .and. (rigid_or(k).eq.'F') ) then
      call input_pp_AF(w_type(k))  ! atomistic free

   else
      write(*,*) " ERROR: in first line of parameter file for specie "//sys_info%mol_names(k)
      call stop_exec

   endif

   close(20)
enddo


!!!!!!!!!  ASSIGN ROTATED POLARIZABILITY TO ALL MOLECULES  !!!!!!!!

n_pp_tot=0
do i_mol=1,sys_info%nmol_tot   ! loop on all molecules 
   mtype=sys(i_mol)%moltype_id

   
   if  (at_or_sm(mtype).eq.'I') then
      call assign_alpha_iso(alpha_iso(mtype))

   elseif  (at_or_sm(mtype).eq.'T') then
      call assign_alpha_tensor(alpha_tensor)

   elseif ( (at_or_sm(mtype).eq.'A' ) .and. (rigid_or(mtype).eq.'R') .and. (w_type(mtype).ne.'F') ) then
      call assign_alpha_AR  ! atomistic rigid

   elseif ( (at_or_sm(mtype).eq.'A' ) .and. (rigid_or(mtype).eq.'R') .and. (w_type(mtype).eq.'F') ) then
            
      call assign_alpha_ARF  ! atomistic rigid with anisotropic weights 
      
   elseif ( (at_or_sm(mtype).eq.'A' ) .and. (rigid_or(mtype).eq.'F') ) then
      call assign_alpha_AF ! atomistic free 
   endif


! 2clear
!   ! print check  
!   write(*,*) 'MOL ',i_mol
!   write(*,"(3(f8.3))") sum(sys(i_mol)%acp(1:n_pp(mtype),1)), sum(sys(i_mol)%acp(1:n_pp(mtype),4)), sum(sys(i_mol)%acp(1:n_pp(mtype),6))
!   write(*,*)
!
!   do kkk=1,n_pp(mtype)
!      write(*,"(6(f8.3))") sys(i_mol)%acp(kkk,1:6)
!   enddo
!   
!   mat=0.0_rk
!   mat(1,1)=sum(sys(i_mol)%acp(1:n_pp(mtype),1))
!   mat(1,2)=sum(sys(i_mol)%acp(1:n_pp(mtype),2))
!   mat(2,1)=sum(sys(i_mol)%acp(1:n_pp(mtype),2))
!   mat(1,3)=sum(sys(i_mol)%acp(1:n_pp(mtype),3))
!   mat(3,1)=sum(sys(i_mol)%acp(1:n_pp(mtype),3))
!   mat(2,2)=sum(sys(i_mol)%acp(1:n_pp(mtype),4))
!   mat(2,2)=sum(sys(i_mol)%acp(1:n_pp(mtype),4))
!   mat(2,3)=sum(sys(i_mol)%acp(1:n_pp(mtype),5))
!   mat(3,2)=sum(sys(i_mol)%acp(1:n_pp(mtype),5))
!   mat(3,3)=sum(sys(i_mol)%acp(1:n_pp(mtype),6))
!
!   call Diagonalize3(mat,eval,evec)
!
!   write(*,"(3(f8.3))") eval(1:3)
!   write(*,*)
!   write(*,*)


enddo



!!!!!!!!!!!!!!!!!  PP OUTPUT  !!!!!!!!!!!!!!!!!!

do k=1,sys_info%mol_types ! loop on chem spec
   write(*,*) '   '//sys_info%mol_names(k)//' has ',n_pp(k),' polarizable points' 
   write(*,*) '   Polarizability input is ',at_or_sm(k),' ',rigid_or(k), ' ',w_type(k) 
   write(*,*)
enddo


write(*,*)  '   The whole system has ',n_pp_tot,' polarizable points' 
write(*,*)


contains 



  subroutine  input_pp_AR(wt)
    implicit none
    integer   :: i,j,idx,i0,i1,i2,is_pp,numat
    real(rk)  ::  w,charge,axx,axy,axz,ayy,ayz,azz,w_tmp,w1,w2,w3
    character(1) :: wt
  

  j=0
  do i=1,sys_info%nat_type(k)

     if (wt.eq.'W') then
        read(20,*) int1,idx,charge,is_pp,w_tmp

     elseif (wt.eq.'F') then  
        read(20,*) int1,idx,charge,is_pp,numat,w1,w2,w3

     else
        read(20,*) int1,idx,charge,is_pp,numat
     endif

     if (is_pp.eq.1) then
        j=j+1
        i_pp(j,k)=idx

        if (wt.eq.'W') then ! weigths directly read from file
           w_pp(j,k)=w_tmp
        elseif (wt.eq.'P') then ! weigths from atomic polarizability 
           w_pp(j,k)=atnu2atpol(numat)
        elseif (wt.eq.'V') then ! weigths from number of valence electrons
           w_pp(j,k)=atnu2valence(numat)
        elseif (wt.eq.'F') then ! read anisotropic weigths from file 
           w_pp_ani(j,k,1)=w1            
           w_pp_ani(j,k,2)=w2
           w_pp_ani(j,k,3)=w3
        endif

!        write(*,*) 'numat',numat, w_pp(j,k) 
     endif
  enddo

  if (j.ne.n_pp(k)) then
     write(*,*) "ERROR: number of polarizable points not consistent!"
     call stop_exec
  endif
  
   ! read the classical polarizability alpha tilde
  read(20,*) axx,axy,axz,ayy,ayz,azz

   alpha_t(k,1,1)=axx
   alpha_t(k,1,2)=axy
   alpha_t(k,1,3)=axz
   alpha_t(k,2,1)=axy
   alpha_t(k,2,2)=ayy
   alpha_t(k,2,3)=ayz
   alpha_t(k,3,1)=axz
   alpha_t(k,3,2)=ayz
   alpha_t(k,3,3)=azz
    
   ! read indices of atoms defining molecular orientation
   ! indices are directly defined on atomic position (no crp, no pp)


   ! GD 23/01/2018 modified to consider diatomic molecules
!   if (n_pp(k).ge.3) then
      read(20,*) i0,i1,i2
      orient_idx(1,1,k)=i0
      orient_idx(1,2,k)=i1
      orient_idx(1,3,k)=i2
!   endif
!   elseif (n_pp(k).eq.2) then
!   endif


   ! the code within the following "if" is only for sc alpha readjustment
   if ((option.eq.22).and.(k.eq.1)) then 
      iP0_m1=i0
      iP1_m1=i1
      iP2_m1=i2
      alpha_m1=alpha_t(1,:,:)
   endif

 end subroutine input_pp_AR


  subroutine input_pp_AF(wt)
    implicit none
    integer        :: i,j,idx,i0,i1,i2,is_pp,numat
    real(rk)       :: w,charge,axx,axy,axz,ayy,ayz,azz,w_tmp
    character(150) :: line
    character(1)   :: wt

  j=0
  do i=1,sys_info%nat_type(k) 

     read(20,"(a150)") line
     line=trim(adjustl(line))

     read(line,*) int1,idx,charge,is_pp

     if (is_pp.eq.1) then
        j=j+1
        
        read(line,*) int1,idx,charge,is_pp,w_tmp,i0,i1,i2,axx,axy,axz,ayy,ayz,azz
        w_pp(j,k)=0.0

        i_pp(j,k)=idx
        orient_idx(j,1,k)=i0
        orient_idx(j,2,k)=i1
        orient_idx(j,3,k)=i2
        alpha_F(j,1,k)=axx
        alpha_F(j,2,k)=axy
        alpha_F(j,3,k)=axz
        alpha_F(j,4,k)=ayy
        alpha_F(j,5,k)=ayz
        alpha_F(j,6,k)=azz
     endif
  enddo

   if (j.ne.n_pp(k)) then
      write(*,*) "ERROR: number of polarizable points not consistent!"
      call stop_exec
   endif
   
  end subroutine input_pp_AF


  subroutine assign_alpha_iso(a_iso) 
    implicit none
    real(rk)        :: a_iso
    integer         :: j
    
    j=1
    sys(i_mol)%acp(j,1)=a_iso   ! xx
    sys(i_mol)%acp(j,2)=0.0_rk  ! xy
    sys(i_mol)%acp(j,3)=0.0_rk  ! xz
    sys(i_mol)%acp(j,4)=a_iso   ! yy
    sys(i_mol)%acp(j,5)=0.0_rk  ! yz
    sys(i_mol)%acp(j,6)=a_iso   ! zz
     
    sys(i_mol)%mu_i(j,:)=0.0_rk
    sys(i_mol)%mu_i(j,:)=0.0_rk


  end subroutine assign_alpha_iso


  subroutine assign_alpha_tensor(a_tens) 
    implicit none
    real(rk)        :: a_tens(6)
    integer         :: j
    
    j=1
    sys(i_mol)%acp(j,1)=a_tens(1)  ! xx
    sys(i_mol)%acp(j,2)=a_tens(2)  ! xy
    sys(i_mol)%acp(j,3)=a_tens(3)  ! xz
    sys(i_mol)%acp(j,4)=a_tens(4)  ! yy
    sys(i_mol)%acp(j,5)=a_tens(5)  ! yz
    sys(i_mol)%acp(j,6)=a_tens(6)  ! zz
     
    sys(i_mol)%mu_i(j,:)=0.0_rk
    sys(i_mol)%mu_i(j,:)=0.0_rk


  end subroutine assign_alpha_tensor


  subroutine assign_alpha_AF
    implicit none
    real(rk)        :: U(3,3),P0(3),P1(3),P2(3),a0(3,3),a_rot(3,3)
    integer         :: j

  do j=1,n_pp(mtype) ! loop on  pp of each molecule
     n_pp_tot=n_pp_tot+1
     
     a0(1,1)=alpha_F(j,1,mtype)
     a0(1,2)=alpha_F(j,2,mtype)
     a0(2,1)=alpha_F(j,2,mtype)
     a0(1,3)=alpha_F(j,3,mtype)
     a0(3,1)=alpha_F(j,3,mtype)
     a0(2,2)=alpha_F(j,4,mtype)
     a0(2,3)=alpha_F(j,5,mtype)
     a0(3,2)=alpha_F(j,5,mtype)
     a0(3,3)=alpha_F(j,6,mtype)

     P0=sys(i_mol)%r_at(orient_idx(j,1,mtype),:)  
     P1=sys(i_mol)%r_at(orient_idx(j,2,mtype),:)
     P2=sys(i_mol)%r_at(orient_idx(j,3,mtype),:)
     call find_rotU(U,P0,P1,P2)
     a_rot=matmul(matmul(U,a0),transpose(U))

     sys(i_mol)%acp(j,1)=a_rot(1,1)  ! xx
     sys(i_mol)%acp(j,2)=a_rot(1,2)  ! xy
     sys(i_mol)%acp(j,3)=a_rot(1,3)  ! xz
     sys(i_mol)%acp(j,4)=a_rot(2,2)  ! yy
     sys(i_mol)%acp(j,5)=a_rot(2,3)  ! yz
     sys(i_mol)%acp(j,6)=a_rot(3,3)  ! zz
     
     sys(i_mol)%mu_i(j,:)=0.0_rk
     sys(i_mol)%mu_i(j,:)=0.0_rk
  enddo

  end subroutine assign_alpha_AF


  subroutine assign_alpha_AR
    implicit none
    real(rk)        :: U(3,3),P0(3),P1(3),P2(3),a0(3,3),a_rot(3,3),ww,w_tot
    real(rk)        :: v1(3),v2(3)
    integer         :: j


! GD 23/01/2018 modified to consider diatomic molecules

P0=sys(i_mol)%r_at(orient_idx(1,1,mtype),:)
P1=sys(i_mol)%r_at(orient_idx(1,2,mtype),:)

if (n_pp(mtype).ge.3) then
   P2=sys(i_mol)%r_at(orient_idx(1,3,mtype),:)

elseif (n_pp(mtype).eq.2) then
   ! in case of a diatomic molecule we need to find a point P2
   ! in the plane normal to the molecular axis
   v1=P1-P0 + epsilon

   P2(1)=1/v1(1)
   P2(2)=-1/v1(2)
   P2(3)=DotProd3(v1,P0)/v1(3)

   v2=P2-P0
   v2=v2/norm3(v2)

   P2= P0 + v2;

endif

! debug
!write(*,*) i_mol
!write(*,"(3(f12.6))") P0
!write(*,"(3(f12.6))") P1
!write(*,"(3(f12.6))") P2
!write(*,*)
!write(*,*)


U=0.0_rk
call find_rotU(U,P0,P1,P2)



a0=alpha_t(sys(i_mol)%moltype_id,:,:)
a_rot=matmul(matmul(U,a0),transpose(U)) 
  
w_tot=sum(w_pp(1:n_pp(mtype),mtype))


do j=1,n_pp(mtype) ! loop on  pp of each molecule
   n_pp_tot=n_pp_tot+1
   ww=w_pp(j,mtype)/w_tot

   sys(i_mol)%acp(j,1)=a_rot(1,1)*ww  ! xx
   sys(i_mol)%acp(j,2)=a_rot(1,2)*ww  ! xy
   sys(i_mol)%acp(j,3)=a_rot(1,3)*ww  ! xz
   sys(i_mol)%acp(j,4)=a_rot(2,2)*ww  ! yy
   sys(i_mol)%acp(j,5)=a_rot(2,3)*ww  ! yz
   sys(i_mol)%acp(j,6)=a_rot(3,3)*ww  ! zz


   
   sys(i_mol)%mu_i(j,:)=0.0_rk
   sys(i_mol)%mu_i(j,:)=0.0_rk
enddo

!write(*,*) 'gnu!'
!do j=1,n_pp(mtype) ! loop on  pp of each molecule
!   write(*,"(i5,3(f8.3))") j,sys(i_mol)%acp(j,1),sys(i_mol)%acp(j,4),sys(i_mol)%acp(j,6)
!enddo

  end subroutine assign_alpha_AR


  subroutine assign_alpha_ARF 
    implicit none
    real(rk)        :: U(3,3),P0(3),P1(3),P2(3),a0(3,3),a_tmp(3,3),a_rot(3,3),ww,w_tot,tol_od 
    integer         :: j,diad_idx(3),i_ax

  P0=sys(i_mol)%r_at(orient_idx(1,1,mtype),:)
  P1=sys(i_mol)%r_at(orient_idx(1,2,mtype),:)
  P2=sys(i_mol)%r_at(orient_idx(1,3,mtype),:)
  U=0.0_rk
  call find_rotU(U,P0,P1,P2)
  a0=alpha_t(sys(i_mol)%moltype_id,:,:) 

  write(*,*)
  write(*,*) "WARNING: you are assigning atomic polarizabilities with anisotropic weights."
  write(*,*) "         Make sure your input molecular polarizability is diagonal! "
  write(*,*)

  tol_od=0.00010_rk
  if ( (a0(1,2).gt.tol_od).or.(a0(1,3).gt.tol_od).or.(a0(2,3).gt.tol_od) ) then
     write(*,*) "WARNING: your input molecular polarizability does not seem NOT to be diagonal!!!"
     write(*,*)
  endif
  
do i_ax=1,3

   a_tmp=0.0_rk
   a_tmp(i_ax,i_ax)=a0(i_ax,i_ax)

   a_rot=matmul(matmul(U,a_tmp),transpose(U)) 
   w_tot=sum(w_pp_ani(1:n_pp(mtype),mtype,i_ax))
  
   do j=1,n_pp(mtype) ! loop on  pp of each molecule
      n_pp_tot=n_pp_tot+1
      ww=w_pp_ani(j,mtype,i_ax)/w_tot

      sys(i_mol)%acp(j,1)=sys(i_mol)%acp(j,1) + a_rot(1,1)*ww  ! xx
      sys(i_mol)%acp(j,2)=sys(i_mol)%acp(j,2) + a_rot(1,2)*ww  ! xy
      sys(i_mol)%acp(j,3)=sys(i_mol)%acp(j,3) + a_rot(1,3)*ww  ! xz
      sys(i_mol)%acp(j,4)=sys(i_mol)%acp(j,4) + a_rot(2,2)*ww  ! yy
      sys(i_mol)%acp(j,5)=sys(i_mol)%acp(j,5) + a_rot(2,3)*ww  ! yz
      sys(i_mol)%acp(j,6)=sys(i_mol)%acp(j,6) + a_rot(3,3)*ww  ! zz
     
      sys(i_mol)%mu_i(j,:)=0.0_rk
      sys(i_mol)%mu_i(j,:)=0.0_rk
   enddo

enddo
!write(*,*) 'gnu!'
!do j=1,n_pp(mtype) ! loop on  pp of each molecule
!   write(*,"(i5,3(f8.3))") j,sys(i_mol)%acp(j,1),sys(i_mol)%acp(j,4),sys(i_mol)%acp(j,6)
!enddo

  end subroutine assign_alpha_ARF

end subroutine input_pp_general


subroutine parse_pbcbox
! GD 27/10/15: this subroutine reads the PBC_BOX input block
  implicit none
  integer       :: i,j,k,ioerr
  character(80) :: line
  character(50) :: key
  logical       :: found


found=.false.
do i=1,nl_inp
   line=input_file(i)
   read(line,*) key

   if ( adjustl(trim(key))=="PBC_BOX" ) then
      found=.true.
      write(*,*) 'INFO: PBC_BOX input block found!'
      write(*,*) 'Cell vectors:'
      do k=1,3
         line=input_file(i+k)

         read(line,*,iostat=ioerr) cell2rep(k,:)
         write(*,*) cell2rep(k,:)

         if (ioerr.ne.0) then
            write(*,*) "ERROR: Something is wrong in the PBC_BOX block!"
            call stop_exec
         endif
      
      enddo

      line=input_file(i+4)
      read(line,*,iostat=ioerr) key 

      if ( (ioerr.eq.0).and.(adjustl(trim(key))=="END") ) then
         exit
      else 
         write(*,*) "ERROR: The PBC_BOX block should be terminated with END!"
         call stop_exec  
      endif
   endif

enddo

if (.not.found) then
   write(*,*) "ERROR: PBC_BOX input block not found!"
   call stop_exec 
endif

write(*,*)

end subroutine parse_pbcbox


subroutine read_input_fnames
! GD 26/10/15: this subroutine reads the molecular parameter filenames.
  implicit none
  integer  :: i,j,k,found,ioerr
  character(4)  :: mol_name
  character(40) :: mei_name,aap_name
  character(80) :: line
  character(50) :: key
  logical       :: found_input_mol

do k=1,sys_info%mol_types
   sys_info%mei_fn(k)=''
   sys_info%aap_fn(k)=''
enddo

found=0
found_input_mol=.false.
do i=1,nl_inp
   line=input_file(i)
   read(line,*) key
   
   
   if ( adjustl(trim(key))=="INPUT_MOL" ) then
      found_input_mol=.true.
      
      do k=1,sys_info%mol_types
         line=input_file(i+k)
         if ( (is_me) .or. ( adjustl(trim(sys_info%mol_names(k)))=='DMY' ) ) then 
            read(line,*,iostat=ioerr) mol_name,mei_name

         elseif (is_cr) then
            read(line,*,iostat=ioerr) mol_name,mei_name,aap_name
         endif

         if (ioerr.ne.0) then
            write(*,*) "ERROR: Something is wrong in the INPUT_MOL block!"
            call stop_exec  
         endif

         ! assign the read name(s) to global variables
         do j=1,sys_info%mol_types

            if (sys_info%mol_names(j).eq.adjustl(mol_name)) then

               sys_info%mei_fn(j)=adjustl(mei_name)
               if (is_cr) sys_info%aap_fn(j)=adjustl(aap_name)

               found=found+1
               exit

            endif
      
         enddo
      enddo

      line=input_file(i+sys_info%mol_types+1)
      read(line,*,iostat=ioerr) key 

      if ( (ioerr.eq.0).and.(adjustl(trim(key))=="END") ) then
         exit
      else 
         write(*,*) "ERROR: The INPUT_MOL block should be terminated with END!"
         call stop_exec  
      endif
   endif

enddo

if (.not.found_input_mol) then
   write(*,*) "ERROR: Mandatory INPUT_MOL input block not found!"
   call stop_exec 
elseif (found.ne.sys_info%mol_types) then
   write(*,*) "ERROR: INPUT_MOL block and pdb file are inconsistent!"
   call stop_exec
endif


write(*,*) ' --  Input block INPUT_MOL -- '
write(*,*) 
write(*,*) 'Molecular input parameters files: '
do k=1,sys_info%mol_types 
   if (is_me) then
      write(*,"(6x,a4,2(2x,a30))") sys_info%mol_names(k),sys_info%mei_fn(k)
   elseif (is_cr) then
      write(*,"(6x,a4,2(2x,a30))") sys_info%mol_names(k),sys_info%mei_fn(k),sys_info%aap_fn(k)
   endif
enddo
write(*,*)

end subroutine read_input_fnames

!! 2clear
!subroutine read_input_fnames_alt
!! GD 3/12/15: this subroutine is equivalnet to read_input_fnames
!! but loads the mei files of alternative charges
!  implicit none
!  integer  :: i,j,k,found,ioerr
!  character(4)  :: mol_name
!  character(40) :: mei_name
!  character(80) :: line
!  character(50) :: key
!  logical       :: found_input_mol
!
!do k=1,sys_info%mol_types
!   sys_info%mei_fn_alt(k)=''
!enddo
!
!found=0
!found_input_mol=.false.
!do i=1,nl_inp
!   line=input_file(i)
!   read(line,*) key
!
!   if ( adjustl(trim(key))=="INPUT_MOL_ALT" ) then
!      found_input_mol=.true.
!
!      do k=1,sys_info%mol_types
!         line=input_file(i+k)
!         read(line,*,iostat=ioerr) mol_name,mei_name
!         
!         if (ioerr.ne.0) then
!            write(*,*) "ERROR: Something is wrong in the INPUT_MOL_ALT block!"
!            call stop_exec  
!         endif
!
!         ! assign the read name(s) to global variables
!         do j=1,sys_info%mol_types
!
!            if (sys_info%mol_names(j).eq.adjustl(mol_name)) then
!
!               sys_info%mei_fn_alt(j)=adjustl(mei_name)
!
!               found=found+1
!               exit
!
!            endif
!      
!         enddo
!      enddo
!
!      line=input_file(i+sys_info%mol_types+1)
!      read(line,*,iostat=ioerr) key 
!
!      if ( (ioerr.eq.0).and.(adjustl(trim(key))=="END") ) then
!         exit
!      else 
!         write(*,*) "ERROR: The INPUT_MOL_ALT block should be terminated with END!"
!         call stop_exec  
!      endif
!   endif
!
!enddo
!
!if (.not.found_input_mol) then
!   write(*,*) "ERROR: Mandatory INPUT_MOL_ALT input block not found!"
!   call stop_exec 
!elseif (found.ne.sys_info%mol_types) then
!   write(*,*) "ERROR: INPUT_MOL_ALT block and pdb file are inconsistent!"
!   call stop_exec   
!endif
!
!
!write(*,*) ' --  Input block INPUT_MOL_ALT -- '
!write(*,*) 
!write(*,*) 'Molecular input parameters files: '
!do k=1,sys_info%mol_types 
!   write(*,"(6x,a4,2(2x,a30))") sys_info%mol_names(k),sys_info%mei_fn_alt(k)
!enddo
!write(*,*)
!
!end subroutine read_input_fnames_alt



! 2clear
!subroutine read_input_fnames
!! GD 9/9/15: this subroutine reads the molecular parameter filenames.
!! The purpose is to make the parameters input more flexible by 
!! introducing parameter file names for each specie in an arbitrary
!! order, preceded by the molecule (residue) name.
!  implicit none
!  integer  :: k,j,found
!  character(4)  :: mol_name
!  character(40) :: mei_name,aap_name
!
!
!
!write(*,*) ' -- Provide the names of parameters files --'
!write(*,*)
!write(*,*) ' One molecular specie per line preceded by the name.'
!write(*,*) ' Examples:'  
!write(*,*) '            MOL  molecule.mei                  (ME)'
!write(*,*) '            MOL  molecule.cri  molecule.aap    (CR)'
!write(*,*)
!
!
!do k=1,sys_info%mol_types
!   sys_info%mei_fn(k)=''
!   sys_info%aap_fn(k)=''
!enddo
!found=0
!
!
!do k=1,sys_info%mol_types
!
!   if (is_me) then
!      read(*,*) mol_name,mei_name
!   elseif (is_cr) then
!      read(*,*) mol_name,mei_name,aap_name
!   endif
!
!   ! assign the read name(s) to global variables
!   do j=1,sys_info%mol_types
!
!      if (sys_info%mol_names(j).eq.adjustl(mol_name)) then
!
!         sys_info%mei_fn(j)=adjustl(mei_name)
!         if (is_cr) sys_info%aap_fn(j)=adjustl(aap_name)
!
!         found=found+1
!         exit
!
!      endif
!      
!   enddo
!enddo
!
!
!if (found.ne.sys_info%mol_types) then
!   write(*,*) "ERROR: provided input file names and pdb are inconsistent!"
!   call stop_exec   
!endif
!
!
!write(*,*) '  Input parameters files: '
!
!do k=1,sys_info%mol_types 
!   if (is_me) then
!      write(*,"(6x,a4,2(2x,a30))") sys_info%mol_names(k),sys_info%mei_fn(k)
!   elseif (is_cr) then
!      write(*,"(6x,a4,2(2x,a30))") sys_info%mol_names(k),sys_info%mei_fn(k),sys_info%aap_fn(k)
!   endif
!enddo
!write(*,*)
!write(*,*)
!
!end subroutine read_input_fnames

subroutine  input_round 
  implicit none
  character(50)    :: key
  logical          :: found

key="ROUND_CHARGE" 
call parse_log(key,round_chg,found) 
if (found) then 
   write(*,"(1x,a20,a2,l1)") key,': ',round_chg 
else
   round_chg=.true.
   write(*,"(1x,a20,a2,l1,a10)") key,': ',round_chg,' (default)'
endif
write(*,*)

end subroutine input_round


subroutine  input_sccr(sys_info,aap,n_crp,i_crp)
! This subroutine loads the inputs for the SCCR:
!  -AAP is a 3-dimensional array: 1st index is the chemical specie,
!   2nd and 3rd are the couple of atoms. 
!   AAP matrix (for each specie) is symmetric but in the current
!   implementation the full matrix is stored...
!  -n_crp is and array with the number of crp of each chemical specie.
!   In the simplest case of SCCR over all the atoms of a molecules
!   n_crp is the number of atoms.
!  -i_crp is a 2D array relating, for each chemical specie, the atomic index
!   relevant to sccr to the index of the coordinates.
!   EXAMPLE:  the coordinates of atom N of molecule M (specie S) are 
!             stored in sys(M)%r_at(i_crp(N,S),:)
!
!   The subroutine also stores the gas phase charges in sys

implicit none

type(sys_summary)                     :: sys_info 
real(rk),allocatable,dimension(:,:,:)  :: aap
integer,allocatable,dimension(:,:)    :: i_crp
integer,allocatable,dimension(:)      :: n_crp
real(rk),allocatable,dimension(:,:)    :: qq0
integer         ::  i,j,k,ii,jj,stat,is_crp,idx,i_type,q_case
character(50)   ::  aap_fname,icrp_fname,label
logical         ::  file_exists,isok
real(rk)         ::  Pij,Pij_sum,q0,q_tot,q_net,q_tol,q_diff,max_sum 
q_tol=1e-5

allocate(aap(sys_info%mol_types,sys_info%at_per_mol_max,sys_info%at_per_mol_max))
allocate(i_crp(sys_info%at_per_mol_max,sys_info%mol_types))
allocate(qq0(sys_info%at_per_mol_max,sys_info%mol_types))
allocate(n_crp(sys_info%mol_types))

aap=0.0_rk
i_crp=0
n_crp=0

write(*,*) 'Charge redistribution parameters (AAP):'
write(*,*)
do k=1,sys_info%mol_types


   if ( adjustl(trim(sys_info%mol_names(k)))=='DMY' ) then
      write(*,*) '  AAP for specie '//sys_info%mol_names(k)//' are not needed!'
      aap(k,:,:)=0.0_rk
   else

   ! i_crp and n_crp
   icrp_fname=sys_info%mei_fn(k)

! 2clear
!   write(*,*) '  Insert the info file for specie '//sys_info%mol_names(k)
!   read(*,*) icrp_fname
!   write(*,*) ' >> ',trim(icrp_fname),' read'
!   write(*,*) ' '
   
   inquire(file=icrp_fname,exist=file_exists)
   if (file_exists) then
      open(file=icrp_fname,unit=20,iostat=stat,status="old")
!      sys_info%parfiles(k)=icrp_fname 2clear
   else
      write(*,*) "ERROR: file "//trim(icrp_fname)//" not found"
      call stop_exec
   endif

   read(20,*) n_crp(k)
   if (n_crp(k).gt.at_per_mol_max) then
      write(*,*) "ERROR: numer of atoms per molecule too large!"
      call stop_exec
   endif


   j=0
   do ii=1,sys_info%nat_type(k)
      read(20,*) is_crp,idx,q0
      if (is_crp.eq.1) then
         j=j+1
         i_crp(j,k)=idx
         qq0(j,k)=q0
      endif
   enddo


   q_net=sum(qq0(1:n_crp(k),k))
   q_case=nint(q_net) ! NINT(x) round to nearest integer, returns an integer
   q_diff=0.0_rk

   !!! rounding net charge !!!
   if (round_chg) then 
      select case (q_case)
      case(-1) 
         qq0(1:n_crp(k),k)=qq0(1:n_crp(k),k)-(q_net+1.0_rk)/n_crp(k)
         q_diff=abs(-1.0_rk-q_net)
      case(0) 
         qq0(1:n_crp(k),k)=qq0(1:n_crp(k),k)-(q_net)/n_crp(k)
         q_diff=abs(q_net)
      case(1)
         qq0(1:n_crp(k),k)=qq0(1:n_crp(k),k)-(q_net-1.0_rk)/n_crp(k)
         q_diff=abs(1.0_rk-q_net)
      case default
         write(*,*) "ERROR: Charge on specie "//sys_info%mol_names(k)//" is:",q_net 
         write(*,*) " Value not contemplated!!! "
         call stop_exec
         
      end select


      if (q_diff.gt.1.0e-3) then
         write(*,*) "ERROR: non integer net charge for specie "//sys_info%mol_names(k)," charge=",q_net
         call stop_exec
      elseif (q_diff.gt.q_tol) then
         write(*,*) "WARNING: non integer net charge for specie "//sys_info%mol_names(k)," charge=",q_net
         write(*,*) "         charges rounded to integer value"
      endif

   endif

   !last check
   if (j.ne.n_crp(k)) then
      write(*,*) "ERROR: number of atoms for SCCR not consistent!"
      call stop_exec
   endif
   close(20)


! 2clear
!   write(*,*) '  Insert the filename with AAP for specie '//sys_info%mol_names(k)
!   read(*,*) aap_fname 
!   write(*,*) ' >> ',trim(aap_fname),' read'
!   write(*,*) ' '
   aap_fname=sys_info%aap_fn(k)
   
   inquire(file=aap_fname,exist=file_exists)
   if (file_exists) then
      open(file=aap_fname,unit=10,iostat=stat,status="old")
   else
      write(*,*) "ERROR:  AAP file "//trim(aap_fname)//" not found"
      call stop_exec
   endif

   isok=.true.
   do ii=1,n_crp(k)**2 
      read(10,*) i,j,Pij
      aap(k,i,j)=Pij
      aap(k,j,i)=Pij
   enddo


   ! check that sum_i (P_ij) =0
   isok=.true.
   max_sum=0.0_rk
   do ii=1,n_crp(k)

      Pij_sum=0.0_rk
      do jj=1,n_crp(k)
         Pij_sum=Pij_sum + aap(k,ii,jj)
      enddo

      if (abs(Pij_sum).gt.0.1_rk) isok=.false.
      if (abs(Pij_sum).gt.abs(max_sum)) max_sum=Pij_sum
      aap(k,ii,ii)=aap(k,ii,ii)-Pij_sum
   enddo
   

   ! OUTPUT  msg
   if (isok) then
      write(*,*) '  AAP for specie '//sys_info%mol_names(k)//' read correctly'
      if (abs(max_sum).ge.1e-4) then
         write(*,*) '  WARNING: MAX|sum_i (P_ij)|= ',abs(max_sum) 
         write(*,*) '           The diagonal of the AAP matrix has been adjusted '
      endif
      
   else
      write(*,*) '  ERROR: AAP for specie '//sys_info%mol_names(k)//' are wrong!'
      write(*,*) '  MAX|sum_i (P_ij)|= ',abs(max_sum) 
      call stop_exec
   endif
   close(10)

endif 
enddo


! writing gas phase charges to sys
do i=1,sys_info%nmol_tot ! loop on all molecules 
   i_type=sys(i)%moltype_id
   sys_info%chg_type(i_type)=0.0_rk
   do j=1,n_crp(sys(i)%moltype_id) ! loop on the crp of each molecule
      sys(i)%q_0(j)=qq0(j,sys(i)%moltype_id)
      sys(i)%q_i(j)=qq0(j,sys(i)%moltype_id)
      sys_info%chg_type(i_type)=sys_info%chg_type(i_type)+sys(i)%q_0(j)
   enddo
enddo

write(*,*)
write(*,*) 'Atomic permanent charges:'

do k=1,sys_info%mol_types ! loop on chem spec
   write(*,"(a,i6,a,f12.6)") '   '//sys_info%mol_names(k)//' has ', &
        n_crp(k),' charge red. points and net charge ',sys_info%chg_type(k)
enddo

! total charge of the system
call total_charge(q_tot)
sys_info%total_chg=q_tot

write(*,*)
write(*,"(a,f16.8)") '  Total charge of the system is ',sys_info%total_chg
write(*,*)
write(*,*) 

end subroutine input_sccr


subroutine  input_chg_me(sys_info,n_crp,i_crp)
! This subroutine loads the charges for ME calculations
! It's a simplified version of input_sccr
  implicit none

  type(sys_summary)                     :: sys_info 
  integer,allocatable,dimension(:,:)    :: i_crp
  integer,allocatable,dimension(:)      :: n_crp
  real(rk),allocatable,dimension(:,:)   :: qq0
  integer         ::  i,j,k,ii,stat,is_crp,idx,i_type,q_case
  character(50)   ::  aap_fname,icrp_fname,label
  logical         ::  file_exists
  real(rk)         ::  q0,q_tot,q_net,q_tol,q_diff


allocate(i_crp(sys_info%at_per_mol_max,sys_info%mol_types))
allocate(qq0(sys_info%at_per_mol_max,sys_info%mol_types))
allocate(n_crp(sys_info%mol_types))
qq0=0.0_rk
q_tol=1e-5

i_crp=0
n_crp=0

write(*,*) 'Atomic permanent charges:'
do k=1,sys_info%mol_types
   ! i_crp and n_crp

   icrp_fname=sys_info%mei_fn(k)

! 2clear
!   write(*,*) '  Insert the info file for specie '//sys_info%mol_names(k)
!   read(*,*) icrp_fname
!   write(*,*) ' >> ',trim(icrp_fname),' read'
!   write(*,*) ' '
   

   inquire(file=icrp_fname,exist=file_exists)
   if (file_exists) then
      open(file=icrp_fname,unit=20,iostat=stat,status="old")
!      sys_info%parfiles(k)=icrp_fname 2clear
   else
      write(*,*) "ERROR: file "//trim(icrp_fname)//" not found"
      call stop_exec
   endif

   read(20,*) n_crp(k)
   if (n_crp(k).gt.at_per_mol_max) then
      write(*,*) "ERROR: numer of atoms per molecule too large!"
      call stop_exec
   endif


   j=0
   do ii=1,sys_info%nat_type(k)
      read(20,*) is_crp,idx,q0
      if (is_crp.eq.1) then
         j=j+1
         i_crp(j,k)=idx
         qq0(j,k)=q0
!         write(*,*)'    > ',k,j,qq0(j,k)
      endif
   enddo


   q_net=sum(qq0(1:n_crp(k),k))
   q_case=nint(q_net) ! NINT(x) round to nearest integer, return integer
   q_diff=0.0_rk

   !!! rounding net charge !!!
   if (round_chg) then 
      select case (q_case)
      case(-1) 
         qq0(1:n_crp(k),k)=qq0(1:n_crp(k),k)-(q_net+1.0_rk)/n_crp(k)
         q_diff=abs(-1.0_rk-q_net)
      case(0) 
         qq0(1:n_crp(k),k)=qq0(1:n_crp(k),k)-(q_net)/n_crp(k)
         q_diff=abs(q_net)
      case(1)
         qq0(1:n_crp(k),k)=qq0(1:n_crp(k),k)-(q_net-1.0_rk)/n_crp(k)
         q_diff=abs(1.0_rk-q_net)
      case default
         write(*,*) "ERROR: Charge on specie "//sys_info%mol_names(k)//" is:",q_net 
         write(*,*) " Value not contemplated!!! "
         call stop_exec
      end select


      if (q_diff.gt.1.0e-3) then
         write(*,*) "ERROR: non integer net charge for specie "//sys_info%mol_names(k)," charge=",q_net
         call stop_exec
      elseif (q_diff.gt.q_tol) then
         write(*,*) "WARNING: non integer net charge for specie "//sys_info%mol_names(k)," charge=",q_net
         write(*,*) "         charges rounded to integer value"
      endif

   endif

   ! last check
   if (j.ne.n_crp(k)) then
      write(*,*) "ERROR: number of atoms and point charges not consistent!"
      call stop_exec
   endif
   close(20)
enddo


! writing gas phase charges to sys
do i=1,sys_info%nmol_tot ! loop on all molecules 
   i_type=sys(i)%moltype_id

!   write(*,*) i,i_type,sys_info%mol_names(i_type)
   sys_info%chg_type(i_type)=0.0_rk
   do j=1,n_crp(i_type) ! loop on the crp of each molecule
      sys(i)%q_0(j)=qq0(j,i_type)
      sys(i)%q_i(j)=qq0(j,i_type)
      sys_info%chg_type(i_type)=sys_info%chg_type(i_type)+sys(i)%q_0(j)
   enddo
!      write(*,*) '    -- ',i,i_type,sys_info%chg_type(i_type)  ! 2clear
enddo

! 2clear
!write(*,*) 
!write(*,*) ' Gas phase charges read'
!write(*,*) 

do k=1,sys_info%mol_types ! loop on chem spec
   write(*,"(a,i6,a,f12.6)") '   '//sys_info%mol_names(k)//' has ',n_crp(k),' point charges and net charge ',sys_info%chg_type(k)
enddo

! total charge of the system
call total_charge(q_tot)
sys_info%total_chg=q_tot

write(*,*)
write(*,"(a,f16.8)") '  Total charge of the system is ',sys_info%total_chg
write(*,*)


end subroutine input_chg_me



subroutine load_specific_charges
! GD 27/6/16
  implicit none
  integer       :: i,j,i_type,foo(2),resid
  character(30) :: fn_chg
  character(6)  :: i_fmt
  character(4)  :: resname

write(*,*)
write(*,*) ' -- Loading atomic charges specific to each molecule! -- '
write(*,*) 
write(*,*) 'INFO: Charges will be read from files with the name: MOLnnnnnn.chg  '
write(*,*) '      where MOL is the residue name and nnnnnn in the residue number'
write(*,*) '      (6 digits, zero padded), as they appear in the pdb.           '
write(*,*)
write(*,*)

do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id
   resid=sys(i)%resid
   resname=sys_info%mol_names(i_type)

   if (resname /="DMY") then
   
      write(i_fmt,"(i0.6)") resid

      fn_chg=''
      fn_chg=resname(1:3)//i_fmt//'.chg'

      open(file=fn_chg,unit=22,status="old")
      
      do j=1,n_pp(i_type)  ! loop on pp 
         read(22,*) sys(i)%q_0(j)
         sys(i)%q_i(j)=sys(i)%q_0(j)
      enddo
      close(22)
      write(*,"(2x,i8,2x,a3,2x,i8,2x,a20)") i,resname(1:3),resid,fn_chg
   
   endif

enddo
write(*,*)
write(*,*)

end subroutine load_specific_charges


subroutine  input_chg_alt 
! GD 3/12/15:  loads alternative charges
  implicit none
  integer       :: i,j,k,found,ioerr,i_at,foo1,foo2,stat
  integer       :: i_mol,i_type
  character(4)  :: mol_name
  character(40) :: mei_name
  character(80) :: line
  character(50) :: key
  logical       :: found_input_mol
  integer,allocatable,dimension(:)    :: pnt
  real(rk),allocatable,dimension(:,:) :: q_alt

allocate(q_alt(sys_info%at_per_mol_max,sys_info%mol_types))
allocate(pnt(sys_info%mol_types))

pnt=0
q_alt=0.0_rk



found=0
found_input_mol=.false.
do i=1,nl_inp
   line=input_file(i)
   read(line,*) key
   

   if ( adjustl(trim(key))=="INPUT_CHG_ALT" ) then
      found_input_mol=.true.
      
      do k=1,sys_info%mol_types ! loop on lines of the INPUT_CHG_ALT block
         line=input_file(i+k)
         
         read(line,*,iostat=ioerr) mol_name,mei_name
                  
         if (ioerr.ne.0) then
            write(*,*) "ERROR: Something is wrong in the INPUT_CHG_ALT block!"
            call stop_exec  
         endif

         do j=1,sys_info%mol_types
            if (sys_info%mol_names(j).eq.adjustl(mol_name)) then
               found=found+1
               pnt(j)=k
               exit
            endif
         enddo


         open(file=mei_name,unit=20,iostat=stat,status="old")
!         read(20,*) foo1
         do i_at=1,n_pp(j)
            !            read(20,*) foo1,foo2,q_alt(i_at,k)
            read(20,*) q_alt(i_at,k)
            
         enddo
         close(20)
         
      enddo

      line=input_file(i+sys_info%mol_types+1)
      read(line,*,iostat=ioerr) key
      if ( (ioerr.eq.0).and.(adjustl(trim(key))=="END") ) then
         exit
      else 
         write(*,*) "ERROR: The INPUT_CHG_ALT block must be terminated with END!"
         call stop_exec  
      endif
   endif

enddo

if (.not.found_input_mol) then
   write(*,*) "ERROR: Mandatory INPUT_CHG_ALT input block not found!"
   call stop_exec 
elseif (found.ne.sys_info%mol_types) then
   write(*,*) "ERROR: INPUT_CHG_ALT block and pdb file are inconsistent!"
   call stop_exec   
endif



do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
    i_type=sys(i_mol)%moltype_id

    do i_at=1,n_pp(i_type)
       sys(i_mol)%dq_i(i_at)=q_alt(i_at,pnt(i_type)) 
    enddo

enddo


end subroutine input_chg_alt




subroutine total_charge(q_tot)
  implicit none
  real(rk)   :: q_tot,q_mol
  integer   ::i,j,i_type

q_tot=0.0_rk
do i=1,sys_info%nmol_tot ! loop on all molecules 
   i_type=sys(i)%moltype_id

   q_mol=0.0_rk
   do j=1,n_crp(sys(i)%moltype_id) ! loop on the crp of each molecule
      q_mol=q_mol + sys(i)%q_0(j)
   enddo

   sys(i)%chg_mol=q_mol
   q_tot=q_tot + q_mol

enddo

end subroutine total_charge




subroutine system_input(sys_info,pdbfile) 
! this subroutine reads the pdb file and extract the info about the system
! editing the sys_info structure
implicit none 

type(sys_summary),intent(out) :: sys_info 
character(50),intent(out)     :: pdbfile

integer :: i,strlen
!character(20)     :: idfmt  ="(21x,i5)",linefmt="(a80)",resnamefmt  ="(17x,a4)" 
character(80)   :: line
logical     :: pdbexists,isnew,isok,found
integer     :: stat,id,last_id,nat_mol,this_mol_type,nat_chk
character(4) ::resname
character(50) :: key


write(*,*) ' --  System structure input --'
write(*,*) 

pdbfile=''
key="PDB"
call parse_str(key,pdbfile,found)
if (found) then 
   write(*,"(1x,a20,a2,a40)") key,': ',pdbfile
   write(*,*)
else
   write(*,*) 'ERROR: The ',adjustl(trim(key)),' variable is required!'
   call stop_exec 
endif


! GD 28/6/16 moved to mescal main
!strlen=len(trim(pdbfile))
!root_fname=adjustl(pdbfile)
!root_fname=root_fname(1:strlen-4)


sys_info%mol_types=0
sys_info%mol_names=""
sys_info%nat_type=0
sys_info%nat_tot=0
sys_info%nmol_tot=0
sys_info%at_per_mol_max=0


inquire(file=pdbfile,exist=pdbexists)
last_id=0
nat_mol=0
this_mol_type=1
if (pdbexists) then
   open(file=pdbfile,unit=1,iostat=stat,status="old")
   line=""

   do while(stat==0 .and. line(1:3)/="TER" .and. line(1:3)/="END" )
      read(1,linefmt,iostat=stat) line
      if (stat==0 .and. ( line(1:3)=="ATO" .or. line(1:3)=="HET" )) then
         sys_info%nat_tot=sys_info%nat_tot+1
         nat_mol=nat_mol+1
         read(line,idfmt) id

         if (id.ne.last_id) then ! new molecule found
            !write(*,*) id

            last_id=id
            sys_info%nmol_tot=sys_info%nmol_tot+1
            sys_info%nat_type(this_mol_type)=nat_mol
            nat_mol=0

            ! is the molecule a new chemical specie?
            read(line,resnamefmt) resname
            isnew=.true. 
            do i=1,moltype_max
               if (adjustl(resname).eq.sys_info%mol_names(i)) then
                  isnew=.false.
                  this_mol_type=i
                  exit
               endif
            enddo

            if (isnew) then
               sys_info%mol_types=sys_info%mol_types+1
               sys_info%mol_names(sys_info%mol_types)=adjustl(resname)
               this_mol_type=sys_info%mol_types
            endif
            sys_info%nmol_type(this_mol_type)=sys_info%nmol_type(this_mol_type)+1

!            write(*,*) this_mol_type,sys_info%nmol_type(this_mol_type)
         endif
      else  ! if the last molecule is a new specie the number of atoms
            ! must be set explicitly
         sys_info%nat_type(this_mol_type)=nat_mol+1
      end if

   enddo

   close(1)     
else
   write(*,*) "ERROR:  pdbfile "//pdbfile//" not found"
   call stop_exec
endif
sys_info%at_per_mol_max=maxval(sys_info%nat_type(:))


! LAST CHECK
isok=.true.
if (sum(sys_info%nmol_type(1:sys_info%mol_types)).ne.sys_info%nmol_tot ) isok=.false.

nat_chk=0
do i=1,sys_info%mol_types
   nat_chk=nat_chk+sys_info%nmol_type(i)*sys_info%nat_type(i)
enddo
if (nat_chk.ne.sys_info%nat_tot)  isok=.false.

! OUTPUT 
if (.not.isok)  then
   write(*,*) ' ERROR in subroutine system_input:'
   write(*,"(3x,a28,i12)") 'Total number of atoms: ',sys_info%nat_tot
   write(*,"(3x,a28,i12)") 'Total number of molecules: ',sys_info%nmol_tot   
   write(*,"(a8,a8,2x,a8,2x,a14)") 'MolId','MolName','# mol.','atoms per mol.'
   do i=1,sys_info%mol_types
      write(*,"(i8,a8,2x,i8,2x,i14)") i,sys_info%mol_names(i),sys_info%nmol_type(i),sys_info%nat_type(i)
   enddo
   call stop_exec
else
   write(*,*) " System IO from  "//trim(pdbfile)//"  terminated correctly"
   write(*,*) ' '
   write(*,"(3x,a28,i12)") 'Total number of atoms: ',sys_info%nat_tot
   write(*,"(3x,a28,i12)") 'Total number of molecules: ',sys_info%nmol_tot   
   write(*,*) ' '
   write(*,"(a8,a8,2x,a8,2x,a14)") 'MolId','MolName','# mol.','atoms per mol.'
   do i=1,sys_info%mol_types
      write(*,"(i8,a8,2x,i8,2x,i14)") i,sys_info%mol_names(i),sys_info%nmol_type(i),sys_info%nat_type(i)
   enddo
   write(*,*) 
endif

end subroutine system_input



subroutine read_atomic_coord(pdbfile,sys)
implicit none 

character(50),intent(in)   :: pdbfile
type(molecule),intent(out),allocatable,dimension(:) :: sys 

integer       :: i
character(80) :: line,line2
logical       :: pdbexists
integer       :: stat,i_mol,i_at !,id,last_id,nat_mol,this_mol_type,nat_chk
character(4)  :: resname,atlab
real(rk)      :: com(3),m_at,m_mol

allocate(sys(sys_info%nmol_tot))

! setting variables to zero 
sys%moltype_id=0
do i=1,sys_info%nmol_tot
   sys(i)%r_at=0.0_rk
   sys(i)%cm=0.0_rk
enddo

i_mol=0
inquire(file=pdbfile,exist=pdbexists)
if (pdbexists) then
   open(file=pdbfile,unit=1,iostat=stat,status="old")
   line=""

   do while(stat==0 .and. line(1:3)/="TER" .and. line(1:3)/="END" )
      read(1,linefmt,iostat=stat) line
      if (stat==0 .and. ( line(1:3)=="ATO" .or. line(1:3)=="HET" )) then

         i_mol=i_mol+1
         com=0.0_rk
         m_mol=0.0_rk

         ! recognize the chemical specie of the molecule 
         read(line,resnamefmt) resname
         do i=1,sys_info%nmol_tot
            if (adjustl(resname).eq.sys_info%mol_names(i)) then
               sys(i_mol)%moltype_id=i
               exit
               endif
         enddo

         ! GD 27/6/16 read resid from pdb
         read(line,"(22x,i5)") sys(i_mol)%resid
         !write(*,*)  sys(i_mol)%resid ! 2clr

         ! read atomic coordinates and center of mass
         i_at=1
         ! GD 6/1/16 pdx (higher coordinate precision) file
         
         if (use_pdx) then
            line2=line(31:80)
            read(line2,*) sys(i_mol)%r_at(i_at,:) 
         else
            read(line,coorfmt) sys(i_mol)%r_at(i_at,:) 
         endif

         read(line,atlabfmt) atlab
         m_at=atomic_mass(atlab)
         m_mol=m_mol+m_at
         com=com+m_at*sys(i_mol)%r_at(i_at,:) 
!         write(*,*) line
!         write(*,*) atlab,m_at

         do i_at=2,sys_info%nat_type(sys(i_mol)%moltype_id)
            read(1,linefmt,iostat=stat) line

! GD 6/1/16 introducion of pdx (higher coordinate precision) file

            if (use_pdx) then
               line2=line(31:80)
               read(line2,*) sys(i_mol)%r_at(i_at,:) 
            else
               read(line,coorfmt) sys(i_mol)%r_at(i_at,:) 
            endif
               
            read(line,atlabfmt) atlab
            m_at=atomic_mass(atlab)
            m_mol=m_mol+m_at
            com=com+m_at*sys(i_mol)%r_at(i_at,:) 
!            write(*,*) atlab,m_at
         enddo        
         
         ! writes molecular center of mass
         com=com/m_mol
         sys(i_mol)%cm=com

      end if

   enddo

   close(1)     
else
   write(*,*) "ERROR:  pdbfile "//pdbfile//" not found"
endif

! checks:
!i_mol=5
!do i_at=1,sys_info%nat_type(sys(i_mol)%moltype_id)
!write(*,*) sys(i_mol)%r_at(i_at,:)
!enddo
!do i_mol=1,sys_info%nmol_tot 
!   write(*,*) 
!   write(*,*) 'molecule ',i_mol,' cm coords: ',sys(i_mol)%cm
!enddo

write(*,*) 'INFO: Atomic coordinates read correctly.'
write(*,*) 
write(*,*) 

end subroutine read_atomic_coord



function atomic_mass(lab)
implicit none

real(rk)       :: atomic_mass
character(4)  :: lab

lab=adjustl(lab) 

select case (lab(1:1))
case('H')
   atomic_mass=1.0079_rk
case('C')
   atomic_mass=12.011_rk
case('O')
   atomic_mass=15.9994_rk
case('N')
   atomic_mass=14.0067_rk
case('F')
   atomic_mass=18.998403_rk
case('S')
   atomic_mass=32.06_rk
case('X')
   atomic_mass=12.011_rk
case('Y')
   atomic_mass=12.011_rk
case('Z')
   atomic_mass=12.011_rk
case('D') ! dummy atom
   atomic_mass=12.011_rk
case('A') ! generic atom
   atomic_mass=12.011_rk
case default
   write(*,*) '  WARNING: atomic mass for atom ',lab,' not recognized! ' 
   write(*,*) '           Default value is assigned (C atom) '
   atomic_mass=12.011_rk
end select

return
end function atomic_mass


function atnu2atpol(n)
  ! returns the value of atomic polarizability
  ! Source: CRC Handbook of Chemistry and Physics.
  implicit none
  integer n
  real(rk)       :: atnu2atpol 

select case (n)
case(0) ! GD 03/03/21  non-polarizable atom
   atnu2atpol=0.0_rk 
case(1)
   atnu2atpol=0.666793_rk
case(6)
   atnu2atpol=1.67_rk  
case(7)
   atnu2atpol=1.10_rk   
case(8)
   atnu2atpol=0.802_rk
case(9)
   atnu2atpol=0.557_rk
case(15)
   atnu2atpol=3.63_rk
case(16)
   atnu2atpol=2.90_rk
case(17)
   atnu2atpol=2.18_rk 
case(35)
   atnu2atpol= 3.05_rk 
case default
! GD 11/10/19 ERROR --> WARNING
   write(*,*) '  WARNING: element Z=',n,'not recognized in mei/cri file! '
   write(*,*) '           Parameters for C atom were assigned instead.'
   atnu2atpol=1.67_rk       
!   write(*,*) '  ERROR in function atnu2atpol: element Z=',n,'not recognized! ' 
!   call stop_exec
end select

return
end function atnu2atpol


function atnu2valence(n)
  ! returns the number of valence electron for (as real number) 
  implicit none
  integer n
  real(rk)       :: atnu2valence

select case (n)
case(0) ! GD 03/03/21  non-polarizable atom
   atnu2valence=0.0_rk
case(1) ! H
   atnu2valence=1.0_rk
case(6) ! C
   atnu2valence=4.0_rk
case(7) ! N
   atnu2valence=5.0_rk   
case(8) ! O
   atnu2valence=6.0_rk
case(9) ! F
   atnu2valence=7.0_rk
case(15) ! P
   atnu2valence=5.0_rk
case(16) ! S
   atnu2valence=6.0_rk
case(17) ! Cl
   atnu2valence=7.0_rk 
case(35) ! Br
   atnu2valence=7.0_rk 
case default
   write(*,*) '  ERROR: in function atnu2valence: element not recognized! ' 
   write(*,*) '  Atomic no. ',n
   call stop_exec
end select

return
end function atnu2valence


subroutine opt_calc_aap
 implicit none

 character(80)    :: line,ofmt,msg(5)
 integer          :: Nat,i,j,l,chg,mult,j1,j2,n_el,n_cs
 character(3)     :: ext
 real(rk)         :: dphi,d,qp,qm
 real(rk)         :: alphaC(3,3)
 logical          :: exists,is_rohf,is_uhf,found,do_clean,use_ff
 real(rk),dimension(:,:),allocatable :: Pij,r
 real(rk),dimension(:),allocatable :: Vff 
 real(rk)         :: eval(3),evec(3,3)
 character(50)    :: key,aapxyz_file,fout_aap,ff_file 
!!!!!!!!!!!!!!!!!!!

msg(1)='ATOM-ATOM POLARIZABILITY CALCULATION'
call option_banner(msg,1)


dphi=0.0050_rk  ! Original Tsiper value


! 2clear
!write(*,*) ' Insert: number of atoms, charge, spin multiplicity: '
!write(*,*) '   Note: for open shell systems the sign of multiplicity defines'
!write(*,*) '         the SCF procedure: + for ROHF, - for UHF'
!read(*,*) Nat,chg,mult
!write(*,*) ' >> ',Nat,chg,mult,' read'
!write(*,*) ' Insert the xyz file:'
!read(*,*) xyzfile
!write(*,*) '>> ',xyzfile,' read'
!write(*,*)

aapxyz_file=''
key="XYZ_AAP"
call parse_str(key,aapxyz_file,found)
if (found) then 
   write(*,"(1x,a20,a2,a40)") key,': ',aapxyz_file
   write(*,*)
else
   write(*,*) 'ERROR: The ',adjustl(trim(key)),' variable is required in AAP calculations!'
   call stop_exec 
endif

inquire(file=aapxyz_file,exist=exists)
if (exists) then
     open(file=aapxyz_file,unit=3,status="old")
else
   write(*,*) 'ERROR: ',adjustl(trim(aapxyz_file)),' file not found!' 
   stop
endif


! GD 06/02/2020  
! AAP evaluated at finite potential
use_ff=.false.
ff_file=''
key="FF_AAP"
call parse_str(key,ff_file,found)
if (found) then 
   write(*,"(1x,a20,a2,a40)") key,': ',ff_file
   write(*,*)
   write(*,*) ' INFO: AAP matrix will be computed as derivatives at finite potential!'
   write(*,*)   
   use_ff=.true.
else
   write(*,*) ' INFO: AAP matrix will be computed as derivatives at zero potential (default) '
   write(*,*)
endif





read(3,*) Nat
read(3,*) chg,mult

if ( (chg.eq.0) .and. (mult.eq.1) ) then
   is_rohf=.false.
   is_uhf=.false.

elseif ( (abs(chg).eq.1).and. (mult.eq.2) ) then
   is_rohf=.true.
   is_uhf=.false.
   write(*,*) 'INFO: AAP from ROHF INDO/S calculations'

elseif ( (abs(chg).eq.1).and. (mult.eq.-2) ) then
   mult=-mult
   is_rohf=.false.
   is_uhf=.true.
   write(*,*) 'INFO: AAP from UHF INDO/S calculations'

else
   write(*,*) 'ERROR: check molecular charge and spin multiplicity!!!'
   call stop_exec 
endif
write(*,*)


allocate(Pij(Nat,Nat))
allocate(r(Nat,3))
allocate(Vff(Nat))

! GD 06/02/2020  
! AAP evaluated at finite potential
Vff=0.0_rk
if (use_ff) then
   open(file=ff_file,unit=23,status="old")

   do i=1,Nat
      read(23,*) Vff(i)     
   enddo
   close(23)

   write(*,*) ' INFO: The potential at atoms has been read from file (in Volt)'
   write(*,*) 
endif



n_el=0
do i = 1,Nat
   read(3,*) j
   n_el=n_el + atnu2vale(j) 
enddo

rewind (3)
read(3,*) 
read(3,*) 


Pij=0.0_rk
do i = 1,Nat

   write(*,"(a20,i4,a1,i4)") '  Processing atom ',i,'/',Nat

   ext = char(ichar('0')+i/10)//char(ichar('0')+mod(i,10))

   if(i < 10) then
      write(ext,'(a2,i1)') '00',i
   else if(i < 100) then
      write(ext,'(a1,i2)') '0',i
   else
      write(ext,'(i3)') i
   endif

   open (1,file='gzin_'//ext)
   open (2,file='gzin_m'//ext)


   ! Heading of ZINDO input files
   if (chg.eq.0) then

      call zindo_head(1,chg,mult) 
      call zindo_head(2,chg,mult)

   elseif (chg.eq.1) then

      if (is_rohf) then 
         n_cs=n_el-2
         call zindo_head_ROHF(1,chg,mult,n_cs) 
         call zindo_head_ROHF(2,chg,mult,n_cs) 
      elseif (is_uhf) then
         call zindo_head_UHF(1,chg,mult) 
         call zindo_head_UHF(2,chg,mult) 
      endif

   elseif (chg.eq.-1) then

      n_cs=n_el
      if (is_rohf) then
         call zindo_head_ROHF(1,chg,mult,n_cs) 
         call zindo_head_ROHF(2,chg,mult,n_cs) 
      elseif (is_uhf) then
         call zindo_head_UHF(1,chg,mult) 
         call zindo_head_UHF(2,chg,mult) 
      endif

   end if


   rewind(3)
   read(3,*) 
   read(3,*) 
   do j = 1,Nat
      read(3,"(a)") line
      do l=80,1,-1
         if (line(l:l).ne.' ') exit
      enddo
     
      ! GD 06/02/2020  
      ! AAP evaluated at finite potential
      
      if (j.eq.i) then
         write(1,"(a,f12.6)") line(1:l+3),Vff(j) + dphi
         write(2,"(a,f12.6)") line(1:l+3),Vff(j) - dphi
      else
         write(1,"(a,f12.6)") line(1:l+3),Vff(j)
         write(2,"(a,f12.6)") line(1:l+3),Vff(j)
      endif
      
   enddo
   write(1,"(a10)") '$END'
   close(1)
   write(2,"(a10)") '$END'
   close(2)

   call system ('rrzindo q < gzin_'//ext//' > zout_'//ext)
   call system ('mv charge.a chg_'//ext)

   call system ('rrzindo q < gzin_m'//ext//' > zout_m'//ext)
   call system ('mv charge.a chg_m'//ext)

   open(unit=1,file='chg_'//ext)
   open(unit=2,file='chg_m'//ext)


   do j=1,Nat
      read(1,*) j1,qp
      read(2,*) j2,qm
      d=-(qp-qm)/(2*dphi)
      Pij(i,j)=d
   enddo
   close (1)
   close (2)

enddo


! Imposing symmetry to AAP matrix
do i=1,Nat
   do j=i,Nat
!      write(*,"(a12,i4,i4,1x,f10.6,1x,f10.6)") '-flag-  ',i,j,Pij(i,j),Pij(j,i)
      Pij(i,j)=.50_rk*(Pij(i,j)+Pij(j,i))
      Pij(j,i)=Pij(i,j)
   enddo
enddo


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                AAP OUTPUT 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

write(*,*) '>>>>>>>>>>>>>>>>>',root_fname

fout_aap=trim(root_fname)//".aap"
write(*,*) '>>>>>>>>>>>>>>>>>',fout_aap
open(unit=4,file=fout_aap)
open(unit=5,file='aap_mat.dat')
rewind (3)
read(3,*) 
read(3,*) 
r=0.0_rk
Pij=Pij*0.52917721_rk*27.21138506_rk  ! conversion: a.u --> internal_units!
do i=1,Nat
   read(3,*) j2,r(i,1),r(i,2),r(i,3)
   write(5,"(500(1x,e15.6))") (.50_rk*(Pij(i,j)+Pij(j,i)),j=1,Nat)
   do j=1,Nat
      write(4,"(i5,i5,f18.12)") i,j,.50_rk*(Pij(i,j)+Pij(j,i))
   enddo
enddo
close(3)
close(4)
close(5)


write(*,*)
write(*,*) '  Atom-atom polarizability (internal units) saved to files:'
write(*,*) '   -aap.dat     :  i j Pij format  '
write(*,*) '   -aap_mat.dat :  matrix format  '
write(*,*) '  '
write(*,*) '  '

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   CHARGE-ONLY POLARIZABILITY CALCULATION   
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

alphaC=0.0_rk
do j1=1,3
   do j2=1,3

      do i=1,Nat
         do j=1,Nat
            alphaC(j1,j2)=alphaC(j1,j2) + Pij(i,j)*r(i,j1)*r(j,j2)
         enddo
      enddo
      
   enddo
enddo

ofmt="(a15,f15.6)" 

write(*,*) ' Charge-only polarizability tensor (Angstrom^3):'
write(*,*) 
do i=1,3
   write(*,"(3(f10.3))") alphaC(i,:)
enddo
write(*,*) 

!write(*,ofmt) '   xx=',alphaC(1,1)
!write(*,ofmt) '   yy=',alphaC(2,2)
!write(*,ofmt) '   zz=',alphaC(3,3)
!write(*,ofmt) '   xy=',alphaC(1,2)!,alphaC(2,1),' diff:',alphaC(1,2)-alphaC(2,1)
!write(*,ofmt) '   xz=',alphaC(1,3)!,alphaC(3,1),' diff:',alphaC(1,3)-alphaC(3,1)
!write(*,ofmt) '   yz=',alphaC(2,3)!,alphaC(3,2),' diff:',alphaC(2,3)-alphaC(3,2)
!write(*,ofmt) '  iso=',(alphaC(1,1)+alphaC(2,2)+alphaC(3,3))/3.0_rk
!write(*,*) '  '

call Diagonalize3(alphaC,eval,evec)
write(*,*) ' Charge-only polarizability eigenvalues (Angstrom^3):'
write(*,*) 
write(*,ofmt) '   e1=',eval(1)
write(*,ofmt) '   e2=',eval(2)
write(*,ofmt) '   e3=',eval(3)
write(*,*) '  '
write(*,*) '  '


! removing rrzindo useless files
key="CLEAN_AAP" 
call parse_log(key,do_clean,found) 
if (found) then 
   write(*,"(1x,a20,a2,l1)") key,': ',do_clean
else
   do_clean=.true.
   write(*,"(1x,a20,a2,l1,a10)") key,': ',do_clean,' (default)'
endif

if (do_clean) then
   call system('rm -rf gzin*')
   call system('rm -rf zout*')
   call system('rm -rf chg_*')
endif



contains 

subroutine zindo_head(fid,q,m)
implicit none

integer  :: fid,q,m

write(fid,*) '$TITLEI'       
write(fid,*) '   TITLE'       
write(fid,*) '$END'          
write(fid,*) '$CONTRL'       
write(fid,*) '   RUNTYP = ENERGY  INTTYP = 1  INTFA(1) = 1.0 1.267 0.585 1.0 1.0'
write(fid,*) '   SCFTYP = RHF'
write(fid,*) '   CHARGE = ',q
write(fid,*) '   MULT = ',m
write(fid,*) '   ITMAX = 999'
write(fid,*) '   SCFTOL = 0'
write(fid,*) '   ONAME = res'
write(fid,*) '$END'
write(fid,*) '$DATAIN'

end subroutine zindo_head


subroutine zindo_head_ROHF(fid,q,m,n_cs)
implicit none

integer  :: fid,q,m,n_cs

write(fid,*) '$TITLEI'       
write(fid,*) '   TITLE'       
write(fid,*) '$END'          
write(fid,*) '$CONTRL'       
write(fid,*) '   RUNTYP = ENERGY  INTTYP = 1  INTFA(1) = 1.0 1.267 0.585 1.0 1.0'
write(fid,*) '   SCFTYP = ROHF'
write(fid,*) '   CHARGE = ',q
write(fid,*) '   MULT = ',m
write(fid,*) '   ITMAX = 999'
write(fid,*) '   NDT = 1'	  
write(fid,*) '   SCFTOL = 0'
write(fid,*) '   FOP(1) =',n_cs,1 
write(fid,*) '   NOP = 1'
write(fid,*) '   ONAME = res'
write(fid,*) '$END'
write(fid,*) '$DATAIN'

end subroutine zindo_head_ROHF


subroutine zindo_head_UHF(fid,q,m)
implicit none

integer  :: fid,q,m

write(fid,*) '$TITLEI'       
write(fid,*) '   TITLE'       
write(fid,*) '$END'          
write(fid,*) '$CONTRL'       
write(fid,*) '   RUNTYP = ENERGY  INTTYP = 1  INTFA(1) = 1.0 1.267 0.585 1.0 1.0'
write(fid,*) '   SCFTYP = UHF'
write(fid,*) '   CHARGE = ',q
write(fid,*) '   MULT = ',m
write(fid,*) '   ITMAX = 999'
write(fid,*) '   NDT = 1'	  
write(fid,*) '   SCFTOL = 0'
write(fid,*) '   ONAME = res'
write(fid,*) '$END'
write(fid,*) '$DATAIN'

end subroutine zindo_head_UHF


function  atnu2vale(atnu) result(val_e)
  implicit none
  integer   :: atnu,val_e

  select case(atnu)
 
  case(1) ! H
     val_e=1
  case(6) ! C
     val_e=4
  case(7) ! N
     val_e=5
  case(8) ! O
     val_e=6
  case(9) ! F
     val_e=7
  case(15) ! P
     val_e=5
  case(16) ! S
     val_e=6
  case(17) ! Cl
     val_e=7
  case(35) ! Br
     val_e=7
  case default 
     write(*,*) '  ERROR in atnu2vale function: atomic number not recognized!!! '
     
  end select

end function atnu2vale

end subroutine opt_calc_aap

subroutine success_msg(n_it,ene_final)
  implicit none 
  integer   :: n_it
  real(rk)   :: ene_final

call  cpu_time( time_end )
!call smile
write(*,"(a,i5,a)")  '   Convergence reached in ',n_it,' steps!!! '
write(*,*) 
call print_elaps_time
write(*,*) 
write(*,*) 
write(*,"(a,f16.4,a)") '            Final Energy (eV): ',energy_total,     '  g_ENERGY'
write(*,*) 
write(*,"(a,f16.4,a)") '   Electrostatic  Energy (eV): ',energy_elstat,   '  g_ELECTROSTATIC'
write(*,"(a,f16.4,a)") '        Induction Energy (eV): ',energy_induction,'  g_INDUCTION'

write(*,*) 
write(*,*) 

end subroutine success_msg


subroutine print_elaps_time
  implicit none
  integer   :: days,hours,mins
  real(rk)  :: secs,tt

tt=(time_end - time_begin)
days=floor(tt/86400) 
hours=floor((tt-days*86400)/3600)
mins=floor((tt-days*86400-3600*hours)/60)
secs=(tt-days*86400-3600*hours-60*mins)

write(*,"(a)") '   Elapsed time: '
write(*,"(a5,i8,a5,i8,a5,i8,a5,f8.1)") 'd:',days,'h: ',hours,'m:',mins,'s:',secs
write(*,*)

end subroutine print_elaps_time


subroutine failure_msg(it_max)
  implicit none
  integer   :: it_max

call  cpu_time( time_end )
write(*,*) ' '
write(*,"(i5,a)") it_max,' iterative steps done without reaching convergence :( '
write(*,"(a,f14.1)")     '   Wasted time (sec)', time_end - time_begin
write(*,*) ' ' 
write(*,*) ' ' 
end subroutine failure_msg


subroutine sc_input
! GD 26/10/15 new version for flexible input
  implicit none
  character(50) :: key
  logical       :: found
  real(rk)      :: damp_dyn_pars(4)

write(*,*) ' --  Inputs for the self consistent iterative solution -- '
write(*,*) 

key="TOL_ENE"
call parse_real(key,tolE,found) 
if (found) then 
   write(*,"(1x,a20,a2,e10.3E2)") key,': ',tolE
else
   tolE=1e-6
   write(*,"(1x,a20,a2,e10.3E2,a10)") key,': ',tolE, ' (default)'
endif


key="MAX_ITER"
call parse_int(key,max_it,found) 
if (found) then 
   write(*,"(1x,a20,a2,i5)") key,': ',max_it
else
   max_it=200
   write(*,"(1x,a20,a2,i5,a10)") key,': ',max_it, ' (default)'
endif


key="ENE_MAX"
call parse_real(key,Emax,found) 
if (found) then 
   write(*,"(1x,a20,a2,f10.5)") key,': ',Emax
else
   Emax=1.0
   write(*,"(1x,a20,a2,f10.5,a10)") key,': ',Emax, ' (default)'
endif
Emax=abs(Emax*sys_info%nmol_tot)
write(*,"(a50,e11.2,a3)") ' INFO: The program will be stopped if abs(Energy)>',Emax,' eV'
write(*,*)


! GD 09/04/2021 introduction of dynamic damping 
use_damp_dyn=.false.
key="DAMP"
call parse_real(key,damp_read,found) 
if (found) then 
   write(*,"(1x,a20,a2,f10.5)") key,': ',damp_read
   call check_damp(damp_read)
   
else
   key="DAMP_DYN"
   call parse_real4(key, damp_dyn_pars ,found) 
    
   if (found) then 

      write(*,"(1x,a20,a2,4(f12.8))") key,': ',damp_dyn_pars

      call check_damp_dyn(damp_dyn_pars)

      use_damp_dyn=.true.
      damp_1=damp_dyn_pars(1)
      dE_1=damp_dyn_pars(2)
      damp_2=damp_dyn_pars(3)
      dE_2=damp_dyn_pars(4)
     
   else ! default
      key="DAMP"   
      damp_read=0.40_rk
      write(*,"(1x,a20,a2,f10.5,a10)") key,': ',damp_read, ' (default)'
   endif
   
endif

! damp will be reassigned during the iterative solution if dynamic damping is used
damp=abs(damp_read)


! 2clear
!use_smart_damp=.true.
!if (abs(damp_read).ge.1.0_rk) then
!   write(*,*) '  ERROR: damping factor must be lower than 1!'
!   call stop_exec
!elseif (damp_read.lt.0) then
!   use_smart_damp=.false.
!   write(*,"(a,f6.3,a)") ' INFO: Damping factor will be applied at every step (DAMP<0)'
!endif
write(*,*)
write(*,*)


contains

  subroutine check_damp(damp_r)
    real(rk)  :: damp_r

   if (abs(damp_r).ge.1.0_rk) then
      write(*,*) '  ERROR: damping factor must be lower than 1!'
      call stop_exec
   elseif (damp_r.lt.0) then
      write(*,"(a,f6.3,a)") ' INFO: Damping factor must be positive. The absolute value is retained.'
   endif

 end subroutine check_damp


 subroutine check_damp_dyn(ddp)
   real(rk)  :: ddp(4)
   logical   :: is_ok
! 1 damp_1   2 dE_1   3 damp_2   4 dE_2  

   is_ok=.true.

   if ( minval(ddp).lt.0.0_rk) then ! all positive!
      is_ok=.false.

   elseif ( ddp(4).gt.ddp(2) ) then
      is_ok=.false.

   elseif ( ddp(3).gt.ddp(1) ) then
      is_ok=.false.

   elseif ( (ddp(1).ge.1.0_rk) .or. (ddp(3).ge.1.0_rk) ) then
      is_ok=.false.

   endif

   if (.not.is_ok) then
      write(*,*) '  ERROR: the input values are incorrect!'
      call stop_exec
   endif

 end subroutine check_damp_dyn




 
end subroutine sc_input


! 2clear
!subroutine sc_input
!  implicit none
!
!write(*,*) ' -- Input for self consistency options -- '
!write(*,*)
!write(*,*) ' Please insert: '(*,*) '   Energy tolerance (in eV)' 
!write(*,*) '   Maximum number of iterations' 
!write(*,*) '   Damping factor for solution update (-1=<damp<=1 *)'
!write(*,*) '   Maximum energy per molecule allowed (eV, absolute value) '
!write(*,*)
!write(*,*) '   *) Damping in solution update is applied in the first 4    '
!write(*,*) '      iterations and when energy oscillations are detected.   '
!write(*,*) '      Extreme cases: 0=no damping, 1=no update.               '
!write(*,*) '      If input value is negative the damping factor (absolute '
!write(*,*) '      value) is applied  at every iteration.                  '
!read(*,*)  tolE,max_it,damp_read,Emax
!
!write(*,*)
!write(*,*) ' Values read: '
!write(*,*) '   Energy tolerance (eV)              :',tolE
!write(*,*) '   Maximum number of iterations       :',max_it 
!write(*,*) '   Damping factor for solution update :',damp_read
!write(*,*) '   Maximum energy per molecule (eV)   :',Emax
!write(*,*)
!
!use_smart_damp=.true.
!damp=abs(damp_read)
!if (abs(damp_read).ge.1.0_rk) then
!   write(*,*) '  ERROR: damping factor must be lower than 1!'
!   call stop_exec
!elseif (damp_read.lt.0) then
!   use_smart_damp=.false.
!   write(*,"(a,f6.3,a)") ' INFO: Damping factor ',damp,' applied at every step'
!endif
!
!Emax=abs(Emax*sys_info%nmol_tot)
!write(*,"(a50,e11.2,a3)") 'INFO: The program will be stopped if abs(Energy)>',Emax,' eV'
!write(*,*)
!write(*,*)
!
!end subroutine sc_input




subroutine IOfinished_banner

call  cpu_time(time_io)

write(*,*) 
write(*,"(a)")           '  Input-Output terminated!                          '
write(*,"(a,f14.1)")     '  Elapsed time (sec)', time_io - time_begin
write(*,*) 
write(*,"(a)") ' ======================================================================= '
!write(*,"(a)") ' ======================================================================= '
write(*,*) 
write(*,*) 

end subroutine IOfinished_banner

    

subroutine smile

write(*,*) ' '
write(*,"(a)") '                           oooo$$$$$$$$$$$$oooo                               '
write(*,"(a)") '                       oo$$$$$$$$$$$$$$$$$$$$$$$$o                            '
write(*,"(a)") '                    oo$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o         o$   $$ o$      '
write(*,"(a)") '    o $ oo        o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o       $$ $$ $$o$     '
write(*,"(a)") ' oo $ $ "$      o$$$$$$$$$    $$$$$$$$$$$$$    $$$$$$$$$o       $$$o$$o$      '
write(*,"(a)") ' "$$$$$$o$     o$$$$$$$$$      $$$$$$$$$$$      $$$$$$$$$$o    $$$$$$$$       '
write(*,"(a)") '   $$$$$$$    $$$$$$$$$$$      $$$$$$$$$$$      $$$$$$$$$$$$$$$$$$$$$$$       '
write(*,"(a)") '   $$$$$$$$$$$$$$$$$$$$$$$    $$$$$$$$$$$$$    $$$$$$$$$$$$$$  """$$$         '
write(*,"(a)") '    "$$$""""$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     "$$$        '
write(*,"(a)") '     $$$   o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     "$$$o      '
write(*,"(a)") '    o$$"   $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$       $$$o     '
write(*,"(a)") '    $$$    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" "$$$$$$ooooo$$$$o   '
write(*,"(a)") '   o$$$oooo$$$$$  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$   o$$$$$$$$$$$$$$$$$  '
write(*,"(a)") '   $$$$$$$$"$$$$   $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     $$$$""""""""        '
write(*,"(a)") '  """"       $$$$    "$$$$$$$$$$$$$$$$$$$$$$$$$$$$"      o$$$                 '
write(*,"(a)") '             "$$$o     """$$$$$$$$$$$$$$$$$$"$$"         $$$                  '
write(*,"(a)") '               $$$o          "$$""$$$$$$""""           o$$$                   '
write(*,"(a)") '                $$$$o                                o$$$"                    '
write(*,"(a)") '                 "$$$$o      o$$$$$$o"$$$$o        o$$$$                      '
write(*,"(a)") '                   "$$$$$oo     ""$$$$o$$$$$o   o$$$$""                       '
write(*,"(a)") '                      ""$$$$$oooo  "$$$o$$$$$$$$$"""                          '
write(*,"(a)") '                         ""$$$$$$$oo $$$$$$$$$$                               '
write(*,"(a)") '                                 """"$$$$$$$$$$$                              '
write(*,"(a)") '                                     $$$$$$$$$$$$                             '
write(*,"(a)") '                                      $$$$$$$$$$"                             '
write(*,"(a)") '                                       "$$$""                                 '
write(*,*) ' '
end subroutine smile


subroutine stop_banner

write(*,"(80a)") ' '
write(*,"(80a)") '   ==> Execution finished, bye ;)       g_OK'
write(*,"(80a)") ' '
write(*,"(80a)") ' '

end  subroutine stop_banner


subroutine start_banner
write(*,"(80a)") ' '
write(*,"(80a)") ' '
write(*,"(80a)") '   Welcome to'
write(*,"(80a)") '  '
write(*,"(80a)") '     /  \     /  |/        | /      \  /      \           /  | '
write(*,"(80a)") '     $$  \   /$$ |$$$$$$$$/ /$$$$$$  |/$$$$$$  |  ______  $$ | '
write(*,"(80a)") '     $$$  \ /$$$ |$$ |__    $$ \__$$/ $$ |  $$/  /      \ $$ | '
write(*,"(80a)") '     $$$$  /$$$$ |$$    |   $$      \ $$ |       $$$$$$  |$$ | '
write(*,"(80a)") '     $$ $$ $$/$$ |$$$$$/     $$$$$$  |$$ |   __  /    $$ |$$ | '
write(*,"(80a)") '     $$ |$$$/ $$ |$$ |_____ /  \__$$ |$$ \__/  |/$$$$$$$ |$$ | '
write(*,"(80a)") '     $$ | $/  $$ |$$       |$$    $$/ $$    $$/ $$    $$ |$$ | '
write(*,"(80a)") '     $$/      $$/ $$$$$$$$/  $$$$$$/   $$$$$$/   $$$$$$$/ $$/  '
write(*,"(80a)") ' '
write(*,"(80a)") ' '
write(*,"(80a)") '  MESCal is a suite of routines for MicroElectroStatic Calculations     '
write(*,"(80a)") '  in organic solids.                                                    '
write(*,"(80a)") '                                                                        '
write(*,"(80a)") '  If you are looking for a distilled alcoholic beverage made from       '
write(*,"(80a)") '  agave americana you are in the wrong place, sorry!                    '
write(*,"(80a)") '  http://en.wikipedia.org/wiki/Mezcal                                   '
write(*,"(80a)") '                                                                        '
write(*,"(80a)") '                                                                        '
write(*,"(80a)") '  Copyright by Gabriele D'//achar(39)//'Avino (2013)                    ' 
write(*,"(80a)") '  gabriele.davino@gmail.com                                             '
write(*,"(80a)") '                                                                        '
write(*,"(80a)") '  MESCal is free software: you can redistribute it and/or modify        '       
write(*,"(80a)") '  it under the terms of the GNU General Public License as published by  '
write(*,"(80a)") '  the Free Software Foundation, either version 3 of the License.        '
write(*,"(80a)") '                                                                        '
write(*,"(80a)") '  MESCal is distributed in the hope that it will be useful,             '
write(*,"(80a)") '  but WITHOUT ANY WARRANTY; without even the implied warranty of        '
write(*,"(80a)") '  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         '
write(*,"(80a)") '  GNU General Public License for more details.                          '
write(*,"(80a)") '                                                                        '
write(*,"(80a)") '  You should have received a copy of the GNU General Public License     '
write(*,"(80a)") '  along with MESCal.  If not, see <http://www.gnu.org/licenses/>.       '    
write(*,"(80a)") '                                                                        '
write(*,"(80a)") '  Publications of results obtained with MESCal should cite              '
write(*,"(80a)") '  G. D'//achar(39)//'Avino et al., J. Chem. Theory Comput. 10, 4959 (2014)'
write(*,"(80a)") ' '
write(*,"(80a)") ' '
write(*,"(80a)") '  List of contributors: '
write(*,"(80a)") '       Jing Li          : MPI parallelization                            '
write(*,"(80a)") ' '
write(*,"(80a)") ' ======================================================================= '
write(*,"(80a)") ' ======================================================================= '
write(*,"(80a)") ' '

end subroutine start_banner

subroutine stop_exec
  write(*,*) 
  write(*,*) " ...execution stopped!  "
  write(*,*)
#ifdef MPI
  call MPI_ABORT(MPI_COMM_WORLD, 0, ierr)
#endif
  stop
end subroutine stop_exec


end module io

